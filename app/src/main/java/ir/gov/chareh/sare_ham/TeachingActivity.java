package ir.gov.chareh.sare_ham;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class TeachingActivity extends AppCompatActivity
//        implements NavigationView.OnNavigationItemSelectedListener
{

    public static boolean isExpanded = false ;
    public static boolean isExpanded2 = false ;
    public static boolean isExpanded3 = false ;
    public static boolean isExpanded4 = false ;
    public static boolean isExpanded5 = false ;
    public static float firstY  ;
    public static float CfirstY  ;
    public static boolean is_firstY_changed = false;
    public static float secondY = 0 ;
    public static float PimaAlpha = 1 ;
    public static boolean isLastItemClicked = false ;
    public static void MyAnim(final View v,final View v2,final int ch,final ScrollView scrollView){
        final int initialHeight = v.getMeasuredHeight();
        final int chHi =((v.getHeight()*ch * 48 )/88);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = initialHeight + ((int) (chHi* (interpolatedTime * interpolatedTime)));
                v.requestLayout();
                super.applyTransformation(interpolatedTime, t);
            }
        };
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                v.setClickable(false);
                v2.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setClickable(true);
                v2.setClickable(true);
//                if (v.getId() == R.id.item5){
//                    if ((isLastItemClicked == true)&&(isExpanded5 == true)){
//                        scrollView.smoothScrollBy(0,chHi);
//                        isLastItemClicked = false ;
//                    }
//                }else
                if(v.getId() == R.id.item4){
                    if ((isLastItemClicked == true)&&(isExpanded4 == true)){
                        scrollView.smoothScrollBy(0,chHi);
                        isLastItemClicked = false ;
                    }
                }
                else if(v.getId() == R.id.item3){
                    if ((isLastItemClicked == true)&&(isExpanded3 == true)){
                        scrollView.smoothScrollBy(0,(chHi));
                        isLastItemClicked = false ;
                    }
                }
                else if(v.getId() == R.id.item2){
                    if ((isLastItemClicked == true)&&(isExpanded2 == true)){
                        scrollView.smoothScrollBy(0,(chHi));
                        isLastItemClicked = false ;
                    }
                }
                else if(v.getId() == R.id.item1){
                    if ((isLastItemClicked == true)&&(isExpanded == true)){
                        scrollView.smoothScrollBy(0,(chHi));
                        isLastItemClicked = false ;
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation.setDuration(220);
        v.startAnimation(animation);
    }
    public static void MyAnim2(final View v,final View v2,final View cont){
        final int initialHeight = v.getMeasuredHeight();
        final int chHi = cont.getHeight() ;
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = initialHeight - ((int) (chHi* (interpolatedTime * interpolatedTime)));
                v.requestLayout();
                super.applyTransformation(interpolatedTime, t);
            }
        };
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                v.setClickable(false);
                v2.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setClickable(true);
                v2.setClickable(true);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        animation.setDuration(225);
        v.startAnimation(animation);
    }
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
        TextView tv = (TextView) findViewById(R.id.toolbar_text);
        if (tv != null)
            tv.setText(title);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teaching);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.teaching_toolbar);
//        setSupportActionBar(toolbar);
        setTitle("");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupStatusBarColor();
        }
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.teaching_drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.teaching_nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
        {
//            View app_bar = findViewById(R.id.app_bar_layout);
            View data = findViewById(R.id.included_teaching);
//            Animation move_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.appbar_in);
//            AlphaAnimation move_down = new AlphaAnimation(0 , 1);
//            move_down.setFillAfter(true);
//            move_down.setDuration(getResources().getInteger(R.integer.animation_in));
            AlphaAnimation fade_in = new AlphaAnimation(0, 1);
            fade_in.setDuration(getResources().getInteger(R.integer.animation_in));
//            app_bar.startAnimation(move_down);
            data.startAnimation(fade_in);
        }
        isExpanded = false ;
        isExpanded2 = false ;
        isExpanded3 = false ;
        isExpanded4 = false ;
        isExpanded5 = false ;
        firstY = 0f ;
        is_firstY_changed = false;
        secondY = 0 ;
        PimaAlpha = 1 ;
        isLastItemClicked = false ;

        final ScrollView scrollView = (ScrollView) findViewById(R.id.scView);
        final RelativeLayout mainR = (RelativeLayout) findViewById(R.id.mainRe);
        mainR.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == event.ACTION_DOWN){
                    firstY = event.getY() ;
                    RelativeLayout H = (RelativeLayout) findViewById(R.id.item1_click);
                    if (H.getHeight() > mainR.getHeight() - event.getY()){
                        isLastItemClicked = true ;
                    }
                }
                return false;
            }
        });
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == event.ACTION_DOWN){
                    firstY = event.getY() ;
                    return false ;
                }
                if (event.getAction() == event.ACTION_MOVE) {
                    isLastItemClicked =false ;
                    ImageView imageView = (ImageView) findViewById(R.id.imageVin);
                    if (scrollView.canScrollVertically(-1) == true){
                        CfirstY = event.getY() ;
                        is_firstY_changed = true ;
                    }
                    if (scrollView.canScrollVertically(1) == false){
                        firstY = event.getY() ;
                    }
                    if ((scrollView.canScrollVertically(-1) == false) && (event.getY() - firstY > 0)) {
                        if (is_firstY_changed){
                            firstY = CfirstY ;
                            is_firstY_changed = false ;
                        }
                        if (event.getY() < firstY ){
                            firstY = event.getY() ;
                        }
                        if (imageView.getAlpha() > 0.5 ) {
                            imageView.setAlpha(1 - ((event.getY() - firstY) / 700));
                            PimaAlpha = imageView.getAlpha() ;
                        }
                        secondY = event.getY() ;
                    }
                    if ((imageView.getAlpha() < 1) && (event.getY() - secondY < 0)) {
                        imageView.setAlpha(PimaAlpha - ((event.getY() - secondY) / 700));
                    }
                    return false;
                }
                if (event.getAction() == event.ACTION_UP){
                    ImageView imageView = (ImageView) findViewById(R.id.imageVin);
                    ObjectAnimator objectAnimator = new ObjectAnimator().ofFloat(imageView,"alpha",imageView.getAlpha(),1);
                    objectAnimator.setDuration(250);
                    objectAnimator.start();
                    return false;
                }

                return false;
            }
        });

    }

    private void setupStatusBarColor() {
        Window window = this.getWindow();

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.mainPage_first_item_color));
        }

    }

    public void itemOneClickHandler(View view) {
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scView);
        RelativeLayout relativeLayout1 = (RelativeLayout) findViewById(R.id.item1);
        RelativeLayout relativeLayout11 = (RelativeLayout) findViewById(R.id.item1_click);
        Animation rotateAnimation = AnimationUtils.loadAnimation(this,R.anim.rot) ;
        rotateAnimation.setDuration(225);
        rotateAnimation.setFillAfter(true);
        Animation rotateAnimation2 = AnimationUtils.loadAnimation(this,R.anim.rot2) ;
        rotateAnimation2.setDuration(225);
        rotateAnimation2.setFillAfter(true);
        relativeLayout1.bringToFront();
        RelativeLayout cont = (RelativeLayout) findViewById(R.id.childCont1);
        ImageView imageView = (ImageView) findViewById(R.id.item1arrow);
        if (isExpanded){
            MyAnim2(relativeLayout1,relativeLayout11,cont);
            if (isLastItemClicked){
                isLastItemClicked = false ;
            }
            imageView.startAnimation(rotateAnimation2);
            imageView.setImageResource(R.drawable.down_arrow);
            isExpanded = false ;
        }else {
            MyAnim(relativeLayout1,relativeLayout11,4,scrollView);
            imageView.startAnimation(rotateAnimation);
            imageView.setImageResource(R.drawable.up_arrow);
            isExpanded = true;
        }
    }

    public void itemTwoclickhandler(View view) {
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scView);
        RelativeLayout relativeLayout1 = (RelativeLayout) findViewById(R.id.item2);
        RelativeLayout relativeLayout11 = (RelativeLayout) findViewById(R.id.item2_click);
        Animation rotateAnimation = AnimationUtils.loadAnimation(this,R.anim.rot) ;
        rotateAnimation.setDuration(225);
        rotateAnimation.setFillAfter(true);
        Animation rotateAnimation2 = AnimationUtils.loadAnimation(this,R.anim.rot2) ;
        rotateAnimation2.setDuration(225);
        rotateAnimation2.setFillAfter(true);
        relativeLayout1.bringToFront();
        RelativeLayout cont = (RelativeLayout) findViewById(R.id.childCont2);
        ImageView imageView = (ImageView) findViewById(R.id.item2arrow);
        if (isExpanded2){
            MyAnim2(relativeLayout1,relativeLayout11,cont);
            if (isLastItemClicked){
                isLastItemClicked = false ;
            }
            imageView.startAnimation(rotateAnimation2);
            imageView.setImageResource(R.drawable.down_arrow);
            isExpanded2 = false ;
        }else {
            MyAnim(relativeLayout1,relativeLayout11,3,scrollView);
            imageView.startAnimation(rotateAnimation);
            imageView.setImageResource(R.drawable.up_arrow);
            isExpanded2 = true;
        }
    }

    public void itemThreeClickHandler(View view) {
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scView);
        RelativeLayout relativeLayout1 = (RelativeLayout) findViewById(R.id.item3);
        RelativeLayout relativeLayout11 = (RelativeLayout) findViewById(R.id.item3_click);
        Animation rotateAnimation = AnimationUtils.loadAnimation(this,R.anim.rot) ;
        rotateAnimation.setDuration(225);
        rotateAnimation.setFillAfter(true);
        Animation rotateAnimation2 = AnimationUtils.loadAnimation(this,R.anim.rot2) ;
        rotateAnimation2.setDuration(225);
        rotateAnimation2.setFillAfter(true);
        relativeLayout1.bringToFront();
        RelativeLayout cont = (RelativeLayout) findViewById(R.id.childCont3);
        ImageView imageView = (ImageView) findViewById(R.id.item3arrow);
        if (isExpanded3){
            MyAnim2(relativeLayout1,relativeLayout11,cont);
            if (isLastItemClicked){
                isLastItemClicked = false ;
            }
            imageView.startAnimation(rotateAnimation2);
            imageView.setImageResource(R.drawable.down_arrow);
            isExpanded3 = false ;
        }else {
            MyAnim(relativeLayout1,relativeLayout11,6,scrollView);
            imageView.startAnimation(rotateAnimation);
            imageView.setImageResource(R.drawable.up_arrow);
            isExpanded3 = true;
        }
    }

    public void itemFourClickHandler(View view) {
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scView);
        RelativeLayout relativeLayout1 = (RelativeLayout) findViewById(R.id.item4);
        RelativeLayout relativeLayout11 = (RelativeLayout) findViewById(R.id.item4_click);
        Animation rotateAnimation = AnimationUtils.loadAnimation(this,R.anim.rot) ;
        rotateAnimation.setDuration(225);
        rotateAnimation.setFillAfter(true);
        Animation rotateAnimation2 = AnimationUtils.loadAnimation(this,R.anim.rot2) ;
        rotateAnimation2.setDuration(225);
        rotateAnimation2.setFillAfter(true);
        relativeLayout1.bringToFront();
        RelativeLayout cont = (RelativeLayout) findViewById(R.id.childCont4);
        ImageView imageView = (ImageView) findViewById(R.id.item4arrow);
        if (isExpanded4){
            MyAnim2(relativeLayout1,relativeLayout11,cont);
            if (isLastItemClicked){
                isLastItemClicked = false ;
            }
            imageView.startAnimation(rotateAnimation2);
            imageView.setImageResource(R.drawable.down_arrow);
            isExpanded4 = false ;
        }else {
            MyAnim(relativeLayout1,relativeLayout11,3,scrollView);
            imageView.startAnimation(rotateAnimation);
            imageView.setImageResource(R.drawable.up_arrow);
            isExpanded4 = true;
        }

    }

//    public void itemFiveClickHandler(View view) {
//        final ScrollView scrollView = (ScrollView) findViewById(R.id.scView);
//        RelativeLayout relativeLayout1 = (RelativeLayout) findViewById(R.id.item5);
//        RelativeLayout relativeLayout11 = (RelativeLayout) findViewById(R.id.item5_click);
//        Animation rotateAnimation = AnimationUtils.loadAnimation(this,R.anim.rot) ;
//        rotateAnimation.setDuration(225);
//        rotateAnimation.setFillAfter(true);
//        Animation rotateAnimation2 = AnimationUtils.loadAnimation(this,R.anim.rot2) ;
//        rotateAnimation2.setDuration(225);
//        rotateAnimation2.setFillAfter(true);
//        relativeLayout1.bringToFront();
//        RelativeLayout cont = (RelativeLayout) findViewById(R.id.childCont5);
//        ImageView imageView = (ImageView) findViewById(R.id.item5arrow);
//        if (isExpanded5){
//            MyAnim2(relativeLayout1,relativeLayout11,cont);
//            if (isLastItemClicked){
//                isLastItemClicked = false ;
//            }
//            imageView.startAnimation(rotateAnimation2);
//            imageView.setImageResource(R.drawable.down_arrow);
//            isExpanded5 = false ;
//        }else {
//            MyAnim(relativeLayout1,relativeLayout11,2,scrollView);
//            imageView.startAnimation(rotateAnimation);
//            imageView.setImageResource(R.drawable.up_arrow);
//            isExpanded5 = true;
//        }
//
//    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.teaching_drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
            Intent prev_page = new Intent(this, First_page.class);
            startActivity(prev_page);
//        }
        finish();
    }

//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.teaching_drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    public void item1child1clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne_fortasviri.class);
        next_page.putExtra("tag", "00");
        startActivity(next_page);
    }

    public void item1child2clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "01");
        startActivity(next_page);
    }
    public void item1child3clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "02");
        startActivity(next_page);
    }
    public void item1child4clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "03");
        startActivity(next_page);
    }
    public void item2child1clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "10");
        startActivity(next_page);
    }
    public void item2child2clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "11");
        startActivity(next_page);
    }
    public void item2child3clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "12");
        startActivity(next_page);
    }
    public void item3child1clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "20");
        startActivity(next_page);
    }
    public void item3child2clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "21");
        startActivity(next_page);
    }
    public void item3child3clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "22");
        startActivity(next_page);
    }
    public void item3child4clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "23");
        startActivity(next_page);
    }
    public void item3child5clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "24");
        startActivity(next_page);
    }
    public void item3child6clicked(View view) {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "25");
        startActivity(next_page);
    }
    public void item4child1clicked(View view)
    {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "30");
        startActivity(next_page);
    }
    public void item4child2clicked(View view)
    {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "31");
        startActivity(next_page);

    }
    public void item4child3clicked(View view)
    {
        Intent next_page = new Intent(TeachingActivity.this, teachingOne.class);
        next_page.putExtra("tag", "32");
        startActivity(next_page);

    }
}
