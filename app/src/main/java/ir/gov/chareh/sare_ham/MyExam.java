package ir.gov.chareh.sare_ham;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MyExam extends AppCompatActivity
     /*   implements NavigationView.OnNavigationItemSelectedListener*/ {


    private ExamCaller examCaller;
    private QuestionCaller questionCaller;
    private Exam exam;
    private ArrayList<Question> questions = new ArrayList<>();
    private ArrayList<Customized_radiobtn> radios = new ArrayList<>();

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MyDatabase";
    private static final String QUESTION_TABLE_NAME = "Questions";
    private static final String IMAGES_TABLE_NAME = "Images";
    private static final String EXAMS_TABLE_NAME = "Exams";
    private static final String RECORDS_TABLE_NAME = "Records";
    private static final String CHAPTERS_TABLE_NAME = "Chapters";
    private static final String PARAGRAPHS_TABLE_NAME = "Paragraphs";

    private static final String PAGE_TYPE = "PAGE_TYPE";
    private static final String EXAM_QUESTIONS = "EXAM_QUESTIONS";
    private static final String TAG_QUESTIONS = "TAG_QUESTIONS";
    private static final String RANDOM_EXAM_QUESTIONS = "RANDOM_EXAM_QUESTIONS";
    private static final String CHAPTER_TAG = "CHAPTER_TAG";
    private static final String EXAM_ID = "EXAM_ID";
    private static final String REVIEW_MODE = "REVIEW_MODE";
    private static final String RECORD_ID = "RECORD_ID";
    private static final String THIS_RECORD_ID = "THIS_RECORD_ID";

    private ValueAnimator timer;
    String pageType;
    int examId;
    String chapterTag;
    AssetDatabaseHelper dbHelper;

    public static int EXAM_TIME = 8;
    public static int SECOND_PER_MIN = 60;

    private static ProgressDialog progressDialog;
    int wrongBackGroundColor = Color.rgb(255,200,200);
    int correctBackGroundColor = Color.rgb(200,255,200);

    Drawable submitQuestionDrawable;
    Drawable resetQuestionDrawable;
    Drawable unavailableDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        InitialPageTask task = new InitialPageTask();
        task.execute();

    }

    private class InitialPageTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MyExam.this,null,getString(R.string.message_progressDialog_loadExam),true);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            initial();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            initial2();
            if (progressDialog != null)
              progressDialog.dismiss();
        }
    }

    private void initial() {


        submitQuestionDrawable = getResources().getDrawable(R.drawable.check);
        resetQuestionDrawable = getResources().getDrawable(R.drawable.reset);
        unavailableDrawable = null;
//        unavailableDrawable = getResources().getDrawable(R.drawable.);

        dbHelper = new AssetDatabaseHelper(
                getBaseContext(), DATABASE_NAME, DATABASE_VERSION);

        ImageView endImageView = (ImageView) findViewById(R.id.exit_exam_top);
        if (endImageView != null)
            endImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (pageType.equals(EXAM_QUESTIONS) || pageType.equals(RANDOM_EXAM_QUESTIONS))
                        decisionMakingExitExam();
                    else if (pageType.equals(REVIEW_MODE) || pageType.equals(TAG_QUESTIONS))
                        onBackPressed();
                    else
                        throw new RuntimeException("unhandled end_exam_top image view");
                }
            });


        // DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        //        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);
        //toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        final TextView timerText = (TextView) findViewById(R.id.timer_text);
        pageType = getIntent().getExtras().getString(PAGE_TYPE);

        Button finishButton = (Button) findViewById(R.id.finish_button);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pageType.equals(EXAM_QUESTIONS) || pageType.equals(RANDOM_EXAM_QUESTIONS))
                    decisionMakingEndExam();
                else if (pageType.equals(REVIEW_MODE) || pageType.equals(TAG_QUESTIONS))
                    onBackPressed();
            }
        });
        if (pageType.equals(REVIEW_MODE) || pageType.equals(TAG_QUESTIONS))
            finishButton.setVisibility(View.GONE);


            if (pageType.equals(EXAM_QUESTIONS) || pageType.equals(RANDOM_EXAM_QUESTIONS)) {
//            Timerclass(timerText, 30);

            int secondsToRun = EXAM_TIME * SECOND_PER_MIN - 1;

            timer = ValueAnimator.ofInt(secondsToRun);
            timer.setDuration(secondsToRun * 1000).setInterpolator(new LinearInterpolator());
            timer.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    int elapsedSeconds = (int) animation.getAnimatedValue();
                    int minutes = (EXAM_TIME - 1) - elapsedSeconds / SECOND_PER_MIN;
                    int seconds = SECOND_PER_MIN-1 - elapsedSeconds % SECOND_PER_MIN;

                    timerText.setText(String.format("%d:%02d", minutes, seconds));
                    if ((minutes    == 0) && (seconds == 0)) {
                        timer.cancel();
                        decisionMakingEndExam();
                    }
                }
            });

        }

        if (pageType.equals(EXAM_QUESTIONS) || pageType.equals(REVIEW_MODE)) {
            examId = getIntent().getExtras().getInt(EXAM_ID);
            examCaller = new ExamCaller(getBaseContext(), dbHelper, DATABASE_NAME, DATABASE_VERSION, EXAMS_TABLE_NAME, QUESTION_TABLE_NAME, IMAGES_TABLE_NAME);
            exam = examCaller.callExamById(examId);
            questions = exam.getQuestions();

        } else if (pageType.equals(RANDOM_EXAM_QUESTIONS)) {
            examCaller = new ExamCaller(getBaseContext(), dbHelper, DATABASE_NAME, DATABASE_VERSION, EXAMS_TABLE_NAME, QUESTION_TABLE_NAME, IMAGES_TABLE_NAME);
            exam = examCaller.callRandomExam();
            questions = exam.getQuestions();
            examId = -1;
        } else if (pageType.equals(TAG_QUESTIONS)) {
            chapterTag = getIntent().getExtras().getString(CHAPTER_TAG);
//            Toast.makeText(getApplicationContext(), pageType + " : " + chapterTag, Toast.LENGTH_LONG).show();
            questionCaller = new QuestionCaller(getBaseContext(), dbHelper, DATABASE_NAME, DATABASE_VERSION, QUESTION_TABLE_NAME, IMAGES_TABLE_NAME);
            questions = questionCaller.callQuestionsByChapterTag(chapterTag, -1, false);
        }
//        LinearLayout questions_list_layout = (LinearLayout) findViewById(R.id.questions_list_layout);
//        setup_questions(questions_list_layout);
//        View end_exam_layout = getLayoutInflater().inflate(R.layout.endofexasm_buttons, null);
////        ImageView end_exam_btn = (ImageView) end_exam_layout.findViewById(R.id.end_exam_top);
////        end_exam_btn.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                endExam();
//
////                Intent i = new Intent(MyExam.this, RecordActivity.class);
////                i.putExtra(EXAM_ID,examId);
////                startActivity(i);
////            }
////        });
//        if (pageType.equals(TAG_QUESTIONS)) {
//            questions_list_layout.addView(end_exam_layout);
//            Button nextQuestions = (Button) findViewById(R.id.next_questions);
//            nextQuestions.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                    Toast.makeText(getApplicationContext(), "در حال آماده سازی ....", Toast.LENGTH_SHORT).show();
//                    //                int father_no = Integer.parseInt(chapterTag.substring(0, 1));
//                    //                int son_no = Integer.parseInt(chapterTag.substring(1,2));
//                    //                int max_son = chapterHandler.getCountOfTag(Integer.toString(father_no) + "?");
//                    //                if(son_no + 1 >= max_son){
//                    //                    if(!(father_no + 1 >= max_fathers)){
//                    //                        next_tag = Integer.toString((father_no + 1) * 10);
//                    //                        scrollView.fullScroll(1);
//                    //                        outro_scroll = true;
//                    //                    }
//                    //                }
//                    //                else{
//                    //                    next_tag = Integer.toString(father_no * 10) + Integer.toString(son_no + 1);
//                    //                    scrollView.fullScroll(1);
//                    //                    outro_scroll = true;
//                    //                }
//                }
//            });
//
//        }
    }
    private void initial2()
    {
        LinearLayout questions_list_layout = (LinearLayout) findViewById(R.id.questions_list_layout);
        setup_questions(questions_list_layout);
//        View end_exam_layout = getLayoutInflater().inflate(R.layout.endofexasm_buttons, null);
//        ImageView end_exam_btn = (ImageView) end_exam_layout.findViewById(R.id.end_exam_top);
//        end_exam_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                endExam();

//                Intent i = new Intent(MyExam.this, RecordActivity.class);
//                i.putExtra(EXAM_ID,examId);
//                startActivity(i);
//            }
//        });
        if (pageType.equals(TAG_QUESTIONS)) {

//            questions_list_layout.addView(end_exam_layout);
//            Button nextQuestions = (Button) findViewById(R.id.next_questions);
//            if (nextQuestions == null)
//                throw new RuntimeException("null next question button");
//            nextQuestions.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int father_no = Integer.parseInt(chapterTag.substring(0, 1));
//                    int son_no = Integer.parseInt(chapterTag.substring(1,2));
//                    int max_son = chapterHandler.getCountOfTag(Integer.toString(father_no) + "?");
//                    if(son_no + 1 >= max_son){
//                        if(!(father_no + 1 >= max_fathers)){
//                            String next_tag = Integer.toString((father_no + 1) * 10);
//                            scrollView.fullScroll(1);
//                            outro_scroll = true;
//                        }
//                    }
//                    else{
//                        String next_tag = Integer.toString(father_no * 10) + Integer.toString(son_no + 1);
//                        scrollView.fullScroll(1);
//                        outro_scroll = true;
//                    }
//}
//                }
//            });

        }
        if (pageType.equals(EXAM_QUESTIONS) || pageType.equals(RANDOM_EXAM_QUESTIONS))
            timer.start();
    }

    public ArrayList<Integer> get_user_answers() {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < radios.size(); i++) {
            result.add(radios.get(i).get_chosen_option());
        }
        return result;
    }

    public void setup_questions(LinearLayout layout) {
        for (int position = 0; position < questions.size(); position++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final LinearLayout question = (LinearLayout) inflater.inflate(R.layout.options, null);
//            final Button check_question = new Button(getApplicationContext());
            final Button check_question = (Button) question.findViewById(R.id.refresh_button);
            ArrayList<Integer> userAnswers = new ArrayList<>();

            View touch1 = question.findViewById(R.id.option1).findViewById(R.id.touch_area1);
            View touch2 = question.findViewById(R.id.option2).findViewById(R.id.touch_area2);
            View touch3 = question.findViewById(R.id.option3).findViewById(R.id.touch_area3);
            View touch4 = question.findViewById(R.id.option4).findViewById(R.id.touch_area4);
            final Customized_radiobtn radiobtn = new Customized_radiobtn(question);
            touch1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("touch","prev touch : " + radiobtn.get_chosen_option() + "|" + "touch1");
                    if (radiobtn.get_chosen_option() != 0) {
                        radiobtn.set_option(1);
                        if (pageType.equals(TAG_QUESTIONS)) {
                            check_question.setBackground(submitQuestionDrawable);
                            check_question.setClickable(true);
                            check_question.setEnabled(true);
                        }
                    }
                    else
                    {
                        radiobtn.set_option(1);
                        check_question.setBackground(unavailableDrawable);
                        check_question.setClickable(false);
                        check_question.setClickable(false);
                    }
                }
            });
            touch2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (radiobtn.get_chosen_option() != 1) {
                        radiobtn.set_option(2);
                        if (pageType.equals(TAG_QUESTIONS)) {
                            check_question.setBackground(submitQuestionDrawable);
                            check_question.setClickable(true);
                            check_question.setEnabled(true);
                        }
                    }
                    else
                    {
                        radiobtn.set_option(2);
                        check_question.setBackground(unavailableDrawable);
                        check_question.setClickable(false);
                        check_question.setClickable(false);
                    }
                }
            });
            touch3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (radiobtn.get_chosen_option() != 2) {
                        radiobtn.set_option(3);
                        if (pageType.equals(TAG_QUESTIONS)) {
                            check_question.setBackground(submitQuestionDrawable);
                            check_question.setClickable(true);
                            check_question.setEnabled(true);
                        }
                    }
                    else
                    {
                        radiobtn.set_option(3);
                        check_question.setBackground(unavailableDrawable);
                        check_question.setClickable(false);
                        check_question.setClickable(false);
                    }
                }
            });
            touch4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (radiobtn.get_chosen_option() != 3) {
                        radiobtn.set_option(4);
                        if (pageType.equals(TAG_QUESTIONS)) {
                            check_question.setBackground(submitQuestionDrawable);
                            check_question.setClickable(true);
                            check_question.setEnabled(true);
                        }
                    }
                    else
                    {
                        radiobtn.set_option(4);
                        check_question.setBackground(unavailableDrawable);
                        check_question.setClickable(false);
                        check_question.setClickable(false);
                    }
                }
            });
            if (pageType.equals(TAG_QUESTIONS)) {
//                check_question = new Button(getApplicationContext());
                check_question.setVisibility(View.VISIBLE);
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(80, 80);
//                params.setMargins(15, 5, 0 , 0);
//                check_question.setLayoutParams(params);
//                question.addView(check_question);
                updateCheckButton(question,position, check_question);
                Question thisQuestion = questions.get(position);
                if (thisQuestion.getSelectedOP() != -1) {
                    if (questions.get(position).getAnswerOP() == questions.get(position).getSelectedOP()) {
                        question.setBackgroundColor(correctBackGroundColor);
                    } else if (questions.get(position).getAnswerOP() != questions.get(position).getSelectedOP()) {
                        question.setBackgroundColor(wrongBackGroundColor);
                    }
                }

            } else if (pageType.equals(EXAM_QUESTIONS) || pageType.equals(RANDOM_EXAM_QUESTIONS)) {

            } else if (pageType.equals(REVIEW_MODE)) {
                int recordId = getIntent().getExtras().getInt(RECORD_ID);
                RecordHandler rh = new RecordHandler(getApplicationContext(), dbHelper, DATABASE_NAME, DATABASE_VERSION, RECORDS_TABLE_NAME);
                Record record = rh.getRecordById(recordId);
                userAnswers = record.getUserAnswers();
            }

            TextView question_number = (TextView) question.findViewById(R.id.question_number);
            TextView question_txt = (TextView) question.findViewById(R.id.question_txt);
            ImageView question_img = (ImageView) question.findViewById(R.id.question_img);
            TextView option1_txt = (TextView) question.findViewById(R.id.option1_text);
            ImageView option1_img = (ImageView) question.findViewById(R.id.option1_img);
            TextView option2_txt = (TextView) question.findViewById(R.id.option2_text);
            ImageView option2_img = (ImageView) question.findViewById(R.id.option2_img);
            TextView option3_txt = (TextView) question.findViewById(R.id.option3_text);
            ImageView option3_img = (ImageView) question.findViewById(R.id.option3_img);
            TextView option4_txt = (TextView) question.findViewById(R.id.option4_text);
            ImageView option4_img = (ImageView) question.findViewById(R.id.option4_img);




            question_number.setText("-" + Integer.toString(position + 1));

            Question thisquestion = questions.get(position);

            if (thisquestion.HasHeadImage())
                question_img.setImageBitmap(thisquestion.getHeadImage().getBitmap());
            question_txt.setText(thisquestion.getQuestionText());
            if (thisquestion.HasOpImage()) {
                option1_img.setImageBitmap(thisquestion.getIthOpImage(0).getBitmap());
                option2_img.setImageBitmap(thisquestion.getIthOpImage(1).getBitmap());
                option3_img.setImageBitmap(thisquestion.getIthOpImage(2).getBitmap());
                option4_img.setImageBitmap(thisquestion.getIthOpImage(3).getBitmap());

            } else {
                option1_txt.setText(thisquestion.getIthOP(0));
                option2_txt.setText(thisquestion.getIthOP(1).substring(1));
                option3_txt.setText(thisquestion.getIthOP(2).substring(1));
                option4_txt.setText(thisquestion.getIthOP(3).substring(1));
            }
            if (pageType.equals(REVIEW_MODE)) {
                touch1.setClickable(false);
                touch2.setClickable(false);
                touch3.setClickable(false);
                touch4.setClickable(false);
                if (userAnswers.get(position) != -1 && userAnswers.get(position) != thisquestion.getAnswerOP()) {
                    if (userAnswers.get(position) == 0) {
                        question.findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                    } else if (userAnswers.get(position) == 1) {
                        question.findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                    } else if (userAnswers.get(position) == 2) {
                        question.findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                    } else if (userAnswers.get(position) == 3) {
                        question.findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                    }

                }
                if (thisquestion.getAnswerOP() == 0) {
                    question.findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.gold));
                } else if (thisquestion.getAnswerOP() == 1) {
                    question.findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.gold));
                } else if (thisquestion.getAnswerOP() == 2) {
                    question.findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.gold));
                } else if (thisquestion.getAnswerOP() == 3) {
                    question.findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.gold));
                }

            } else if (pageType.equals(TAG_QUESTIONS)) {
                if (thisquestion.getAnswerOP() == thisquestion.getSelectedOP()) {
                    if (thisquestion.getAnswerOP() == 0) {
                        question.findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.gold));
                    } else if (thisquestion.getAnswerOP() == 1) {
                        question.findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.gold));
                    } else if (thisquestion.getAnswerOP() == 2) {
                        question.findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.gold));
                    } else if (thisquestion.getAnswerOP() == 3) {
                        question.findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.gold));
                    }

                } else if (thisquestion.getSelectedOP() != -1 && thisquestion.getSelectedOP() != thisquestion.getAnswerOP()) {
                    if (thisquestion.getSelectedOP() == 0) {
                        question.findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                    } else if (thisquestion.getSelectedOP() == 1) {
                        question.findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                    } else if (thisquestion.getSelectedOP() == 2) {
                        question.findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                    } else if (thisquestion.getSelectedOP() == 3) {
                        question.findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                    }
                    if (thisquestion.getAnswerOP() == 0) {
                        question.findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.gold));
                    } else if (thisquestion.getAnswerOP() == 1) {
                        question.findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.gold));
                    } else if (thisquestion.getAnswerOP() == 2) {
                        question.findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.gold));
                    } else if (thisquestion.getAnswerOP() == 3) {
                        question.findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.gold));
                    }
                }

            }
            radios.add(radiobtn);
            layout.addView(question);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        //    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //    if (drawer.isDrawerOpen(GravityCompat.START)) {
        //        drawer.closeDrawer(GravityCompat.START);
        //    } else {
//        if (pageType.equals(RANDOM_EXAM_QUESTIONS) || pageType.equals(EXAM_QUESTIONS))
//            endExam();
        if (pageType.equals(RANDOM_EXAM_QUESTIONS) || pageType.equals(EXAM_QUESTIONS)) {
            decisionMakingExitExam();
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle(getString(R.string.title_alertDialog_cancelExam));
//            builder.setMessage(getString(R.string.message_alertDialog_cancelExam));
//            builder.setPositiveButton(getString(R.string.okButton_alertDialog),new DialogInterface.OnClickListener(){
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    timer.cancel();
//                    finish();
//                }
//            });
//            builder.setNegativeButton(getString(R.string.cancelButton_alertDialog), null);
//
//            AlertDialog alertDialog = builder.create();
//            alertDialog.show();
        }
        else
            super.onBackPressed();

        //    }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /*@SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/

    //    public void end_exam(View view) {
//        RecordHandler recordHandler = new RecordHandler(getBaseContext(),dbHelper,DATABASE_NAME,DATABASE_VERSION,RECORDS_TABLE_NAME);
//        Record r = recordHandler.makeRecord(exam, get_user_answers());
//        recordHandler.commitRecord(r);
//
//                Intent i = new Intent(MyExam.this, RecordActivity.class);
//                i.putExtra(EXAM_ID,examId);
//                startActivity(i);
//    }

    public void decisionMakingExitExam()
    {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
//        alertDialog.setTitle(getString(R.string.title_alertDialog_exitExam));

        alertDialog.setMessage("از خروج از آزمون اطمینان دارید؟");


        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.exit_exam), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                timer.cancel();
                finish();
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancelButton_alertDialog), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.cancel();
            }
        });
        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, getString(R.string.end_exam), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                endExam();
            }
        });
        if (!timer.isRunning()){
            alertDialog.setMessage(getString(R.string.message_alertDialog_forceEndExam));
            alertDialog.cancel();
        }
        alertDialog.show();

    }
    public void decisionMakingEndExam()
    {

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

//        alertDialog.setTitle(getString(R.string.title_alertDialog_endExam));

        alertDialog.setMessage("از پایان آزمون اطمینان دارید؟");

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancelButton_alertDialog), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.cancel();
            }
        });
        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, getString(R.string.end_exam), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                endExam();
            }
        });
        if (!timer.isRunning()){
            alertDialog.setMessage(getString(R.string.message_alertDialog_forceEndExam));
            alertDialog.cancel();
        }
        alertDialog.show();
    }
    public void endExam() {

        progressDialog = ProgressDialog.show(this,null,this.getString(R.string.message_progressDialog_endExam),true);
        timer.cancel();
        RecordHandler recordHandler = new RecordHandler(getBaseContext(), dbHelper, DATABASE_NAME, DATABASE_VERSION, RECORDS_TABLE_NAME);
        Record r = recordHandler.makeRecord(exam, get_user_answers());
        recordHandler.commitRecord(r);

        ///fucking shit for update id
        ArrayList<Record> records = recordHandler.makeListByExamId(r.getExamId());
        r = records.get(records.size() - 1);
        records = null;
        //fucking shit fo update id !!!
        //it is ok now...
        Intent i = new Intent(MyExam.this, RecordActivity.class);
        i.putExtra(EXAM_ID, examId);
        i.putExtra(THIS_RECORD_ID,r.getId());
        startActivity(i);
    }

    public void Timerclass(final TextView textView, final int time_in_min) {
//
//        int secondsToRun = time_in_min * SECOND_PER_MIN - 1;
//
//        final ValueAnimator timer = ValueAnimator.ofInt(secondsToRun);
//        timer.setDuration(secondsToRun * 1000).setInterpolator(new LinearInterpolator());
//        timer.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                int elapsedSeconds = (int) animation.getAnimatedValue();
//                int minutes = (time_in_min - 1) - elapsedSeconds / SECOND_PER_MIN;
//                int seconds = 59 - elapsedSeconds % SECOND_PER_MIN
// ;
//
//                textView.setText(String.format("%d:%02d", minutes, seconds));
//                if ((minutes == 0) && (seconds == 0)) {
//                    endExam();
//                }
//            }
//        });
//        timer.start();
    }
//
//    public void force_end_exam(View view) {
//        if (pageType.equals(EXAM_QUESTIONS) || pageType.equals(RANDOM_EXAM_QUESTIONS))
//            endExam();
//        else if (pageType.equals(TAG_QUESTIONS))
//            onBackPressed();
//    }

    public void updateCheckButton(final LinearLayout questionView, final int position, final Button check_question) {
//        final Button check_question = (Button) questionView.getChildAt(questionView.getChildCount() - 1);
//        final Question[] thisQuestion = {questions.get(position)};
//        final boolean checked = (thisQuestion[0].getSelectedOP() != -1);
//        Log.d("checkButtonTag","updating check button " + position + " : " + checked);
        final Question[] thisQuestion = {questions.get(position)};
        final boolean checked = (thisQuestion[0].getSelectedOP() != -1);
        Log.d("checkButtonTag","updating first time");

        if (!checked)
        {

            check_question.setClickable(false);
            check_question.setEnabled(false);
            check_question.setBackground(unavailableDrawable);
            questionView.findViewById(R.id.option1).findViewById(R.id.touch_area1).setClickable(true);
            questionView.findViewById(R.id.option2).findViewById(R.id.touch_area2).setClickable(true);
            questionView.findViewById(R.id.option3).findViewById(R.id.touch_area3).setClickable(true);
            questionView.findViewById(R.id.option4).findViewById(R.id.touch_area4).setClickable(true);

        }
        else {
            check_question.setClickable(true);
            check_question.setEnabled(true);
            check_question.setBackground(resetQuestionDrawable);
            questionView.findViewById(R.id.option1).findViewById(R.id.touch_area1).setClickable(false);
            questionView.findViewById(R.id.option2).findViewById(R.id.touch_area2).setClickable(false);
            questionView.findViewById(R.id.option3).findViewById(R.id.touch_area3).setClickable(false);
            questionView.findViewById(R.id.option4).findViewById(R.id.touch_area4).setClickable(false);


        }

        check_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                        Toast.makeText(getApplicationContext(),"چک شد",Toast.LENGTH_LONG).show();

                final Question[] thisQuestion = {questions.get(position)};
                final boolean checked = (thisQuestion[0].getSelectedOP() != -1);

                ////////
                if (!checked) {
                    ArrayList<Integer> user_touches = get_user_answers();
                    questionCaller.changeQuestionSituation(thisQuestion[0].getId(), user_touches.get(position));
                    questions.set(position, questionCaller.callQuestionById(thisQuestion[0].getId()));
//                    if (thisQuestion[0].getAnswerOP() == user_touches.get(position)) {
//                        Toast.makeText(getApplicationContext(), "درسته" + thisQuestion[0].getAnswerOP(), Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(getApplicationContext(), "غلطه" + thisQuestion[0].getAnswerOP() + ":" + user_touches.get(position), Toast.LENGTH_SHORT).show();
//                    }
                    Log.d("checkButtonTag","updating check_question set on click listener");
                    if (questions.get(position).getAnswerOP() == questions.get(position).getSelectedOP()) {
                        questionView.setBackgroundColor(correctBackGroundColor);
                    } else if (questions.get(position).getAnswerOP() != questions.get(position).getSelectedOP()) {
                        questionView.setBackgroundColor(wrongBackGroundColor);
                    }
                    ArrayList<Integer> g = get_user_answers();
                    questionCaller.changeQuestionSituation(thisQuestion[0].getId(),g.get(position));
                    questions.set(position, questionCaller.callQuestionById(thisQuestion[0].getId()));
                    thisQuestion[0] = questionCaller.callQuestionById(thisQuestion[0].getId());
                    if (thisQuestion[0].getAnswerOP() == thisQuestion[0].getSelectedOP()) {
                        if (thisQuestion[0].getAnswerOP() == 0) {
                            questionView.findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.gold));
                            questionView.findViewById(R.id.option1).findViewById(R.id.touch_area1).callOnClick();

                        } else if (thisQuestion[0].getAnswerOP() == 1) {
                            questionView.findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.gold));
                            questionView.findViewById(R.id.option2).findViewById(R.id.touch_area2).callOnClick();

                        } else if (thisQuestion[0].getAnswerOP() == 2) {
                            questionView.findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.gold));
                            questionView.findViewById(R.id.option3).findViewById(R.id.touch_area3).callOnClick();

                        } else if (thisQuestion[0].getAnswerOP() == 3) {
                            questionView.findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.gold));
                            questionView.findViewById(R.id.option4).findViewById(R.id.touch_area4).callOnClick();

                        }

                    } else if (thisQuestion[0].getSelectedOP() != -1 && thisQuestion[0].getSelectedOP() != thisQuestion[0].getAnswerOP()) {
                        if (thisQuestion[0].getSelectedOP() == 0) {
                            questionView.findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                            questionView.findViewById(R.id.option1).findViewById(R.id.touch_area1).callOnClick();
                        } else if (thisQuestion[0].getSelectedOP() == 1) {
                            questionView.findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                            questionView.findViewById(R.id.option2).findViewById(R.id.touch_area2).callOnClick();

                        } else if (thisQuestion[0].getSelectedOP() == 2) {
                            questionView.findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                            questionView.findViewById(R.id.option3).findViewById(R.id.touch_area3).callOnClick();

                        } else if (thisQuestion[0].getSelectedOP() == 3) {
                            questionView.findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
                            questionView.findViewById(R.id.option4).findViewById(R.id.touch_area4).callOnClick();

                        }
                        if (thisQuestion[0].getAnswerOP() == 0) {
                            questionView.findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.gold));
                        } else if (thisQuestion[0].getAnswerOP() == 1) {
                            questionView.findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.gold));
                        } else if (thisQuestion[0].getAnswerOP() == 2) {
                            questionView.findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.gold));
                        } else if (thisQuestion[0].getAnswerOP() == 3) {
                            questionView.findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.gold));
                        }
                    }

                    questionView.findViewById(R.id.option1).findViewById(R.id.touch_area1).setClickable(false);
                    questionView.findViewById(R.id.option2).findViewById(R.id.touch_area2).setClickable(false);
                    questionView.findViewById(R.id.option3).findViewById(R.id.touch_area3).setClickable(false);
                    questionView.findViewById(R.id.option4).findViewById(R.id.touch_area4).setClickable(false);

                    check_question.setBackground(resetQuestionDrawable);
                    check_question.setClickable(true);
                    check_question.setEnabled(true);


                }
                else{
                    ArrayList<Integer> user_touches = get_user_answers();
                    questionView.setBackgroundColor(Color.TRANSPARENT);
                    questionView.findViewById(R.id.option1).setBackgroundColor(Color.TRANSPARENT);
                    questionView.findViewById(R.id.option2).setBackgroundColor(Color.TRANSPARENT);
                    questionView.findViewById(R.id.option3).setBackgroundColor(Color.TRANSPARENT);
                    questionView.findViewById(R.id.option4).setBackgroundColor(Color.TRANSPARENT);
                    questionView.findViewById(R.id.option1).findViewById(R.id.touch_area1).setClickable(true);
                    questionView.findViewById(R.id.option2).findViewById(R.id.touch_area2).setClickable(true);
                    questionView.findViewById(R.id.option3).findViewById(R.id.touch_area3).setClickable(true);
                    questionView.findViewById(R.id.option4).findViewById(R.id.touch_area4).setClickable(true);
//                    if (thisQuestion[0].getSelectedOP() == 0) {
////                        questionView.findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
//                        questionView.findViewById(R.id.option1).findViewById(R.id.touch_area).callOnClick();
//                    } else if (thisQuestion[0].getSelectedOP() == 1) {
////                        questionView.findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
//                        questionView.findViewById(R.id.option2).findViewById(R.id.touch_area).callOnClick();
//
//                    } else if (thisQuestion[0].getSelectedOP() == 2) {
////                        questionView.findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
//                        questionView.findViewById(R.id.option3).findViewById(R.id.touch_area).callOnClick();
//
//                    } else if (thisQuestion[0].getSelectedOP() == 3) {
////                        questionView.findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.examPage_appbar));
//                        questionView.findViewById(R.id.option4).findViewById(R.id.touch_area).callOnClick();
//
//                    }

                    questionCaller.changeQuestionSituation(thisQuestion[0].getId(), -1);
                    check_question.setBackground(unavailableDrawable);
                    check_question.setClickable(false);
                    check_question.setEnabled(false);


                }
                questions.set(position,questionCaller.callQuestionById(thisQuestion[0].getId()));
            }
        });
    }
}
