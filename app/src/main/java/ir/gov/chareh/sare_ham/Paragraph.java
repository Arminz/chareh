package ir.gov.chareh.sare_ham;

import android.database.Cursor;

/**
 * Created by user on 9/8/16.
 */
public class Paragraph {

    private static String KEY_ID = "ID";
    private static String KEY_TEXT = "Text";
    private static String KEY_CHAPTER_TAG = "ChapterTag";
    private static String KEY_HTML_TAG = "HtmlTag";
    private static String KEY_STYLE_TAG = "StyleTag";

    private int id;
    private String  text;
    private String chapterTag;
    private String htmlTag;
    private String styleTag;

    public Paragraph(Cursor cursor){
        int iId = cursor.getColumnIndex(KEY_ID);
        int iText = cursor.getColumnIndex(KEY_TEXT);
        int iChapterTag = cursor.getColumnIndex(KEY_CHAPTER_TAG);
        int iHtmlTag = cursor.getColumnIndex(KEY_HTML_TAG);
        int iStyleTag = cursor.getColumnIndex(KEY_STYLE_TAG);

        id = cursor.getInt(iId);
        text = cursor.getString(iText);
        chapterTag = cursor.getString(iChapterTag);
        if (cursor.getString(iHtmlTag) != null)
            htmlTag = cursor.getString(iHtmlTag);
        if (cursor.getString(iStyleTag) != null)
            styleTag = cursor.getString(iStyleTag);

    }
    public int getId(){return id;}
    public String getText(){return text;}
    public String getHtmlTag(){return htmlTag;}
    public boolean hasStyleTag(){return (styleTag != null);}
    public String getStyleTag(){return styleTag;}

}
