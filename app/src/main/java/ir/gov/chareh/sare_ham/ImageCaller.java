package ir.gov.chareh.sare_ham;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by user on 7/28/16.
 */
public class ImageCaller {

    public static String KEY_ID = "ID";
    public static String KEY_IMAGE_DATA = "ImageData";
    public static String KEY_NAME = "Name";
    public static String KEY_CHAPTER_TAG = "ChapterTag";

    private int HEADER_QUESTION_WIDTH = 250;
    private int HEADER_QUESTION_HEIGHT = 250;
    private int OP_QUESTION_WIDTH = 70;
    private int OP_QUESTION_HEIGHT = 70;
    private int DEFAULT_WIDTH = 300;
    private int DEFAULT_HEIGHT = 300;
    private int THUMBNAIL_WIDTH = 150;
    private int THUMBNAIL_HEIGHT = 150;

    private String HEADER_QUESTION = "HEADER_QUESTIONS";
    private String OP_QUESTION = "OP_QUESTION";
    private String REAL_SIZE = "REAL_SIZE";
    private String THUMBNAIL_SIZE = "THUMBNAIL_SIZE";


    private Context context;
    private String IMAGES_TABLE_NAME;
    private String DATABASE_NAME;
    private int DATABASE_VERSION;
    private AssetDatabaseHelper dbHelper;
    private SQLiteDatabase db;
    public ImageCaller(Context baseContext, AssetDatabaseHelper _dbHelper,String _DATABASE_NAME , int _DATABASE_VERSION , String _IMAGES_TABLE_NAME){
        context = baseContext;
        DATABASE_VERSION = _DATABASE_VERSION;
        DATABASE_NAME = _DATABASE_NAME;
        IMAGES_TABLE_NAME = _IMAGES_TABLE_NAME;
        dbHelper = _dbHelper;
//        dbHelper = new AssetDatabaseHelper(context,_DATABASE_NAME , DATABASE_VERSION);
        db = dbHelper.getReadableDatabase();
    }
    public Image callImageById(int id,int width, int height)
    {
        Log.d("ImageCaller" , "calling image with width : " + width + " height : " + height);
        String SQLiteCommand = "SELECT * FROM " + IMAGES_TABLE_NAME +" WHERE " + KEY_ID + " = " + id;
        Cursor cursor = db.rawQuery(SQLiteCommand , null );
        Log.d("SQLiteCommand" , SQLiteCommand );
        cursor.moveToFirst();//very very important!!!
        Image this_image = new Image(cursor,height,width);
        return this_image;

    }
    public Image callImageById(int id , String usage)
    {
        Log.d("ImageCaller" , "calling image with usage : " + usage);
        if (usage . equals(HEADER_QUESTION))
            return callImageById(id , HEADER_QUESTION_WIDTH , HEADER_QUESTION_HEIGHT);
        if (usage . equals(OP_QUESTION))
            return callImageById(id , OP_QUESTION_WIDTH , OP_QUESTION_HEIGHT);
        if (usage . equals(REAL_SIZE))
            return callImageById(id , -1 , -1);
        return callImageById(id);
    }
    public Image callImageById(int id)
    {
        Log.d("ImageCaller" , "calling image with default setting");
        String SQLiteCommand = "SELECT * FROM " + IMAGES_TABLE_NAME +" WHERE " + KEY_ID + " = " + id;
        Cursor cursor = db.rawQuery(SQLiteCommand , null );
        Log.d("SQLiteCommand" , SQLiteCommand );
        cursor.moveToFirst();//very very important!!!
        Image this_image = new Image(cursor,DEFAULT_WIDTH,DEFAULT_HEIGHT );
        return this_image;

    }
    public ArrayList<Image> getThumbnailImages(String chapterTag)
    {
        ArrayList <Image> thumbnailImages = new ArrayList<>();
        String SQLiteCommand = "SELECT * FROM " + IMAGES_TABLE_NAME + " WHERE " + KEY_CHAPTER_TAG + " GLOB \"" + chapterTag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = db.rawQuery(SQLiteCommand,null);
        for (cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
        {
            Image thisImage = new Image(cursor,THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT);
            thumbnailImages.add(thisImage);
        }
        return thumbnailImages;
    }
    public ArrayList<Integer> getListIdByTag(String chapterTag)
    {
        ArrayList<Integer> ids = new ArrayList<>();
        String SQLiteCommand = "SELECT " + KEY_ID + " FROM " + IMAGES_TABLE_NAME + " WHERE " + KEY_CHAPTER_TAG + " GLOB \"" + chapterTag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = db.rawQuery(SQLiteCommand,null);
        int iId = cursor.getColumnIndex(KEY_ID);
        for (cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
        {
            ids.add(cursor.getInt(iId));
        }
        return ids;
    }

}
