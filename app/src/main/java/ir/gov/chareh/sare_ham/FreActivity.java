package ir.gov.chareh.sare_ham;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

public class FreActivity extends AppCompatActivity {
    private static boolean is_first_time = true;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    public static ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fre);
        setUpAnalytics();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.fre_colorPrimaryDark));
        }
        final ImageView iv = (ImageView) findViewById(R.id.img);
        final ImageView iv2 = (ImageView) findViewById(R.id.img2);
        final ImageView iv3 = (ImageView) findViewById(R.id.img3);
        final Button prev = (Button) findViewById(R.id.Button1);
        final Button next = (Button) findViewById(R.id.Button2);
        Typeface noto_arabic = Typeface.createFromAsset(getAssets(), "fonts/NotoSansArabic-Bold.ttf");
        prev.setTypeface(noto_arabic);
        next.setTypeface(noto_arabic);

        prev.setTextColor(getResources().getColor(R.color.fre_colorPrimaryDark));
        next.setTextColor(getResources().getColor(R.color.fre_colorPrimaryDark));
        iv.setScaleX((float)(1));
        iv.setScaleY((float)(1));
        iv2.setScaleX((float)(0.5));
        iv2.setScaleY((float)(0.5));
        iv2.setAlpha(0.38f);
        iv3.setScaleX((float)(0.5));
        iv3.setScaleY((float)(0.5));
        iv3.setAlpha(0.38f);
        prev.setClickable(false);
        prev.setAlpha(0.26f);
        mViewPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                float Offset = (float) (positionOffset * 0.5);
                if(position == 0 && positionOffset != 0){
                    iv.setScaleX((float) (1.0 - Offset));
                    iv.setScaleY((float) (1.0 - Offset));
                    iv2.setScaleX((float) (0.5 + Offset));
                    iv2.setScaleY((float) (0.5 + Offset));
                    iv.setAlpha(1 - positionOffset * 0.62f);
                    iv2.setAlpha(0.38f + positionOffset * 0.62f);
                    prev.setAlpha(0.26f + positionOffset * 0.74f);
                }
                else if(position == 1 && positionOffset != 0){
                    iv2.setScaleX((float) (1.0 - Offset));
                    iv2.setScaleY((float) (1.0 - Offset));
                    iv3.setScaleX((float) (0.5 + Offset));
                    iv3.setScaleY((float) (0.5 + Offset));
                    iv2.setAlpha(1 - positionOffset * 0.62f);
                    iv3.setAlpha(0.38f + positionOffset * 0.62f);
                    if(positionOffset <= 0.5) {
                        next.setText("");
                        next.setAlpha(1);
//                        next.setAlpha(1 - 2 * positionOffset);
                    }
                    else{
                        next.setText("شروع");
                        next.setAlpha((float) ((positionOffset - 0.5) * 2));
                    }
                }
            }
            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    prev.setAlpha(1);
                    prev.setClickable(false);
                }
                else{
                    prev.setClickable(true);
                }
                if(position == 1){
                }
                if(position == 2){
                    next.setText("شروع");
                }
                else{
                    next.setText("");
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }

    private void setUpAnalytics() {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString("test1", "fre");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT, bundle);
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
    }

    private void prep_fre_func(){
        final ImageView iv = (ImageView) findViewById(R.id.img);
        final ImageView iv2 = (ImageView) findViewById(R.id.img2);
        final ImageView iv3 = (ImageView) findViewById(R.id.img3);
        final Button prev = (Button) findViewById(R.id.Button1);
        final Button next = (Button) findViewById(R.id.Button2);
        Typeface noto_arabic = Typeface.createFromAsset(getAssets(), "fonts/NotoSansArabic-Bold.ttf");
        prev.setTypeface(noto_arabic);
        next.setTypeface(noto_arabic);

        prev.setTextColor(getResources().getColor(R.color.fre_colorPrimaryDark));
        next.setTextColor(getResources().getColor(R.color.fre_colorPrimaryDark));
        iv.setScaleX((float)(1));
        iv.setScaleY((float)(1));
        iv2.setScaleX((float)(0.5));
        iv2.setScaleY((float)(0.5));
        iv2.setAlpha(0.38f);
        iv3.setScaleX((float)(0.5));
        iv3.setScaleY((float)(0.5));
        iv3.setAlpha(0.38f);
        prev.setClickable(false);
        prev.setAlpha(0.26f);
        mViewPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                float Offset = (float) (positionOffset * 0.5);
                if(position == 0 && positionOffset != 0){
                    iv.setScaleX((float) (1.0 - Offset));
                    iv.setScaleY((float) (1.0 - Offset));
                    iv2.setScaleX((float) (0.5 + Offset));
                    iv2.setScaleY((float) (0.5 + Offset));
                    iv.setAlpha(1 - positionOffset * 0.62f);
                    iv2.setAlpha(0.38f + positionOffset * 0.62f);
                    prev.setAlpha(0.26f + positionOffset * 0.74f);
                }
                else if(position == 1 && positionOffset != 0){
                    iv2.setScaleX((float) (1.0 - Offset));
                    iv2.setScaleY((float) (1.0 - Offset));
                    iv3.setScaleX((float) (0.5 + Offset));
                    iv3.setScaleY((float) (0.5 + Offset));
                    iv2.setAlpha(1 - positionOffset * 0.62f);
                    iv3.setAlpha(0.38f + positionOffset * 0.62f);
                    if(positionOffset <= 0.5) {
                        next.setText("");
                        next.setAlpha(1);
//                        next.setAlpha(1 - 2 * positionOffset);
                    }
                    else{
                        next.setText("شروع");
                        next.setAlpha((float) ((positionOffset - 0.5) * 2));
                    }
                }
            }
            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    prev.setAlpha(1);
                    prev.setClickable(false);
                }
                else{
                    prev.setClickable(true);
                }
                if(position == 1){
                }
                if(position == 2){
                    next.setText("شروع");
                }
                else{
                    next.setText("");
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }
    private class prep_fre extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... strings) {
            prep_fre_func();
            return null;
        }
    }

    public void prev_clicked(View view) {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }
        private Activity mActivity;
        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof Activity)
                mActivity = (Activity) context;
            setUserVisibleHint(true);
        }

        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {
            super.setUserVisibleHint(isVisibleToUser);
            if (isVisibleToUser) {
                mViewPager.setOnTouchListener(new View.OnTouchListener() {
                private static final float OVERSCROLL_THRESHOLD_IN_PIXELS = 100;
                private float downX;
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
                    float scrWidth = displayMetrics.widthPixels;
                    boolean atEnd = mViewPager.getCurrentItem() == 2;
                    boolean atStart = mViewPager.getCurrentItem() == 0;

                    if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        downX = motionEvent.getX();
                    }
                    else if(motionEvent.getAction() == MotionEvent.ACTION_MOVE){
                        float deltaX = motionEvent.getX() - downX;
                        float deltaX_with_threshold = Math.abs(deltaX) - OVERSCROLL_THRESHOLD_IN_PIXELS;
                        float alpha_thresh = (float) (Math.abs(deltaX) / scrWidth);
                        if(atEnd && deltaX < 0 && deltaX_with_threshold > 0){
                            getView().setAlpha(1 - alpha_thresh);
                        }
                        else if(atStart && deltaX > 0 && deltaX_with_threshold > 0){
                            getView().setAlpha(1 - alpha_thresh);
                        }
                        if((atStart && deltaX < 0) || (atEnd && deltaX > 0)){
                            getView().setAlpha(1);
                        }
                    }
                    else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                        ObjectAnimator fade_back_to_life = ObjectAnimator.ofFloat(getView(), "alpha", getView().getAlpha(), 1);
                        fade_back_to_life.setDuration(200);
                        fade_back_to_life.start();
                    }
                    return false;
                }
                });
                if (getTofActivity() != null) {
                    final Button next = (Button) getTofActivity().findViewById(R.id.Button2);
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mViewPager.getCurrentItem() == 2) {
                                next.setClickable(false);
                                final Intent next_page = new Intent((getActivity() == null ? mActivity : getActivity()), First_page.class);
                                //                            RelativeLayout down_bar = (RelativeLayout) getActivity().findViewById(R.id.down_bar);
                                //                            Animation down_bar_exit = AnimationUtils.loadAnimation(getContext(), R.anim.fre_down_bar_exit);
                                //                            Animation move_red_background = AnimationUtils.loadAnimation(getContext(), R.anim.fre_anim1_move);
                                //                            Animation main_img_exit = AnimationUtils.loadAnimation(getContext(), R.anim.fre_main_image_exit);
                                //                            View red_background = getActivity().findViewById(R.id.red_background);
                                //                            AlphaAnimation fade_red_in = new AlphaAnimation(0, 1);
                                //                            fade_red_in.setDuration(200);
                                //                            fade_red_in.setFillAfter(true);
                                //                            down_bar_exit.setFillAfter(true);
                                //                            main_img_exit.setFillAfter(true);
                                //                            getView().startAnimation(main_img_exit);
                                //                            mViewPager.scrollBy(-1, 0);
                                //                            down_bar.setAnimation(down_bar_exit);
                                //                            red_background.startAnimation(fade_red_in);
                                startActivity(next_page);
                                (getActivity() == null ? mActivity : getActivity()).finish();
                            }
                            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
                        }
                    });
                }
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_fre, container, false);
            final RelativeLayout down_bar = (RelativeLayout) getTofActivity().findViewById(R.id.down_bar);
            ImageView main_img = (ImageView) rootView.findViewById(R.id.main_image);
            TextView main_text = (TextView) rootView.findViewById(R.id.main_text);
            Typeface noto_arabic = Typeface.createFromAsset(getContext().getAssets(), "fonts/NotoSansArabic-Regular.ttf");
            main_text.setTypeface(noto_arabic);
            if(getArguments().getInt(ARG_SECTION_NUMBER) == 1){
                main_img.setImageResource(R.drawable.fre_1);
                main_text.setText("آنچه برای گرفتن گواهینامه‌ی رانندگی\nنیاز دارید");
            }
            else if(getArguments().getInt(ARG_SECTION_NUMBER) == 2){
                main_img.setImageResource(R.drawable.fre_2);
                main_text.setText("آموزش ببینید، آزمون بدهید،\nاز پیشرفت خود مطمئن باشید");
            }
            else if(getArguments().getInt(ARG_SECTION_NUMBER) == 3){
                main_img.setImageResource(R.drawable.fre_3);
                main_text.setText("در صبح آخرین آزمون تنها نیستید\nبا نکات مهم آزمون عملی");
            }
            if(getArguments().getInt(ARG_SECTION_NUMBER) == 1 && is_first_time){
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Animation anim_move = AnimationUtils.loadAnimation(getContext(), R.anim.fre_anim1_move);
                        anim_move.setFillAfter(true);
                        Animation down_bar_entrance_anim = AnimationUtils.loadAnimation(getContext(), R.anim.fre_down_bar_entrance);
                        down_bar_entrance_anim.setFillAfter(true);
                        rootView.setAnimation(anim_move);
                        rootView.setVisibility(View.VISIBLE);
                        is_first_time = false;
                        down_bar.setAnimation(down_bar_entrance_anim);
                        mViewPager.scrollBy(1, 0);
                        mViewPager.setVisibility(View.VISIBLE);
                    }
                }, 100);
            }
            return rootView;
        }

        public Activity getTofActivity() {
            return (getActivity()  == null  ?  mActivity : getActivity());
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 3;
        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
