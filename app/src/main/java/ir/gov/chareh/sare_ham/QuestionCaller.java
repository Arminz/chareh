package ir.gov.chareh.sare_ham;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.CheckedOutputStream;

/**
 * Created by user on 7/24/16.
 */
public class QuestionCaller {

    //SQLite Keys
    private static String KEY_ID = "ID";
    private static String KEY_QUESTION_TEXT = "QuestionText";
    private static String KEY_OPS = "OP's";
    private static String KEY_ANSWER_OP = "AnswerOP";
    private static String KEY_LESSON_ID = "LessonID";
    private static String KEY_CORRECT_ANSWERS_COUNT = "CorrectAnswersCount";
    private static String KEY_WRONG_ANSWERS_COUNT = "WrongAnswersCount";
    private static String KEY_IMAGE_ID = "ImageID";
    private static String KEY_DIFFICULTY_TAG = "DifficultyTag";
    private static String KEY_CHAPTER_TAG = "ChapterTag";

    //Situation Keys
    private static String KEY_SELECTED_OP = "SelectedOP";
//    private static String KEY_DEFAULT = "Default";
//    private static String KEY_CORRECT_ANSWERED = "CorrectAnswered";
//    private static String KEY_WRONG_ANSWERED = "WrongAnswered";


    private String HEADER_QUESTION = "HEADER_QUESTIONS";
    private String OP_QUESTION = "OP_QUESTION";


    Context context;

    private String DATABASE_NAME;
    private String QUESTION_TABLE_NAME;
    String IMAGES_TABLE_NAME;
    int DATABASE_VERSION;
    AssetDatabaseHelper dbHelper;
    SQLiteDatabase rdb;
    SQLiteDatabase wdb;

    ImageCaller ic;
    public QuestionCaller(Context baseContext,AssetDatabaseHelper _dbHelper , String DATABASE_NAME ,int DATABASE_VERSION , String QUESTION_TABLE_NAME,String IMAGES_TABLE_NAME){
        context = baseContext;
        this.DATABASE_VERSION = DATABASE_VERSION;
        this.DATABASE_NAME = DATABASE_NAME;
        this.QUESTION_TABLE_NAME = QUESTION_TABLE_NAME;
        this.IMAGES_TABLE_NAME = IMAGES_TABLE_NAME;
        dbHelper = _dbHelper;
//        dbHelper = new AssetDatabaseHelper(context,DATABASE_NAME , DATABASE_VERSION);
        rdb = dbHelper.getReadableDatabase();
        wdb = dbHelper.getWritableDatabase();
        ic = new ImageCaller(context,dbHelper,DATABASE_NAME,DATABASE_VERSION,IMAGES_TABLE_NAME);
    }

    public void changeQuestionSituation(int questionId ,int newSelectedOp)
    {
        String SQLiteCommand = "UPDATE " + QUESTION_TABLE_NAME + " SET " + KEY_SELECTED_OP + " = " + newSelectedOp + " WHERE " + KEY_ID + " = " + questionId;
        Log.d("SQLiteCommand" , SQLiteCommand);
        wdb.execSQL(SQLiteCommand);
    }
    public Question callQuestionById(int id)
    {

        String SQLiteCommand = "SELECT * FROM " + QUESTION_TABLE_NAME +" WHERE " + KEY_ID + " = " + id;
        Cursor cursor = rdb.rawQuery(SQLiteCommand , null );
        Log.d("SQLiteCommand" , SQLiteCommand );
        cursor.moveToFirst();//very very important!!!
        Question thisQuestion = new Question(cursor);
        if (thisQuestion.HasHeadImage())
            thisQuestion.setHeadImage(ic.callImageById(thisQuestion.getImageID(),HEADER_QUESTION));
        if (thisQuestion.HasOpImage())
            for (int i = 0 ; i < 4 ; i ++)
                thisQuestion.setIthOpImage(ic.callImageById(Integer.parseInt(thisQuestion.getIthOP(i)),OP_QUESTION),i);
        return thisQuestion;
    }
    public ArrayList<Question> callQuestionsByChapterTag(String chapterTag , int limit , boolean isRandomArranged)//limit:-1 = all of questions
    {
        ArrayList <Question> questions = new ArrayList<>();
        String SQLiteCommand = "SELECT * FROM " + QUESTION_TABLE_NAME + " WHERE " + KEY_CHAPTER_TAG + " GLOB \"" + chapterTag + "\"";
        if (isRandomArranged)
            SQLiteCommand += " ORDER BY RANDOM()";
        if (limit != -1)
            SQLiteCommand += " LIMIT " + limit;
        Cursor cursor = rdb.rawQuery(SQLiteCommand , null );
        Log.d(" SQLiteCommand" , SQLiteCommand);
        for (cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
        {
            Question thisQuestion = new Question(cursor);
            if (thisQuestion.HasHeadImage())
                thisQuestion.setHeadImage(ic.callImageById(thisQuestion.getImageID(),HEADER_QUESTION));
            if (thisQuestion.HasOpImage())
                for (int i = 0 ; i < 4 ; i ++)
                    thisQuestion.setIthOpImage(ic.callImageById(Integer.parseInt(thisQuestion.getIthOP(i)),OP_QUESTION),i);
            questions.add(thisQuestion);
        }
        return questions;
    }

    public int getCountOfCheckedTestsByChapterTag(String chapterTag)
    {
        String SQLiteCommand = "SELECT COUNT(*) FROM " + QUESTION_TABLE_NAME + " WHERE " + KEY_CHAPTER_TAG + " GLOB \"" + chapterTag + "\"" + " AND " + KEY_SELECTED_OP + " != " + -1;
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = rdb.rawQuery(SQLiteCommand , null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        return count;
    }
    public int getCountOfTestsByChapterTag(String chapterTag)
    {
        String SQLiteCommand = "SELECT COUNT(*) FROM " + QUESTION_TABLE_NAME + " WHERE " + KEY_CHAPTER_TAG + " GLOB \"" + chapterTag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = rdb.rawQuery(SQLiteCommand , null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        return count;

    }




}
