package ir.gov.chareh.sare_ham.iab;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.android.trivialdrivesample.util.IabHelper;
import com.example.android.trivialdrivesample.util.IabResult;
import com.example.android.trivialdrivesample.util.Inventory;
import com.example.android.trivialdrivesample.util.Purchase;

import ir.gov.chareh.sare_ham.PurchaseHandler;

/**
 * Created by armin on 3/21/17.
 */
public class InAppBillingHandler {
    static final String TAG = "InAppBillingTag";

    // SKUs for our products: the premium upgrade (non-consumable)
    static final String SKU_PREMIUM = "EsfanduneCoin";

    // Does the user have the premium upgrade?
    boolean mIsPremium = false;

    String base64EncodedPublicKey = "";

    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 1372;

    // The helper object
    IabHelper mHelper;
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener;

    Activity activity;

    private boolean isSetup = false;
    private boolean isProgressing = false;

    public InAppBillingHandler(final Context context, @Nullable final SetupListeners setupListeners, @Nullable final GetInventoryListeners getInventoryListeners){

        this.activity = (Activity)context;

        mHelper = new IabHelper(context, base64EncodedPublicKey);


        mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                Log.d(TAG, "Query inventory finished.");
                if (result.isFailure()) {
                    Log.d(TAG, "Failed to query inventory: " + result);
                    if (getInventoryListeners != null)
                        getInventoryListeners.onError(result.getMessage());
                }
                else {
                    Log.d(TAG, "Query inventory was successful.");
                    mIsPremium = inventory.hasPurchase(SKU_PREMIUM);
                    if (mIsPremium)
                        PurchaseHandler.commitPurchase(context);
                    else
                        PurchaseHandler.removePurchase(context);
                    Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));
                    if (getInventoryListeners != null)
                        getInventoryListeners.onSuccessful(mIsPremium);
                }
                getInventoryListeners.onFinish();
                Log.d(TAG, "Initial inventory query finished; enabling main UI.");
                isProgressing = false;
            }
        };



        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                    if (setupListeners != null)
                        setupListeners.onError(result.getMessage());
                }
                else {
                    // Hooray, IAB is fully set up!
                    Log.d(TAG, "IAB is fully set up!");
                    isProgressing = true;
                    isSetup = true;
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                    if (setupListeners != null)
                        setupListeners.onSuccessful();

                }
            }
        });



//



    }

    public void purchase(@Nullable final OnPurchaseFinished onPurchaseFinished)
    {
        if (isProgressing) {
            if (onPurchaseFinished != null)
                onPurchaseFinished.onErrorOtherProgressIsRunning();
            return;
        }
        if (!isSetup) {
            if (onPurchaseFinished != null)
                onPurchaseFinished.onErrorNotFullySetup();
            return;
        }

        IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                if (result.isFailure()) {
                    Log.d(TAG, "Error purchasing: " + result);
                    if (onPurchaseFinished != null)
                        onPurchaseFinished.onError(result.getMessage());
                    return;
                }
                else if (purchase.getSku().equals(SKU_PREMIUM)) {
                    // give user access to premium content and update the UI
                    Log.d(TAG, "Successful purchasing: " + result);
                    PurchaseHandler.commitPurchase(activity);
                    if (onPurchaseFinished != null)
                        onPurchaseFinished.onSuccessful();



                }
            }
        };
        mHelper.launchPurchaseFlow(activity, SKU_PREMIUM, RC_REQUEST, mPurchaseFinishedListener, "payload_string");


    }
    public void dispose()
    {
        if (mHelper != null)
            mHelper.dispose();
        mHelper = null;
    }
}
