package ir.gov.chareh.sare_ham;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 8/10/16.
 */
public class
RecordHandler {
    private static String KEY_ID = "ID";
    private static String KEY_EXAM_ID = "ExamID";
    private static String KEY_USER_ANSWERS = "UserAnswers";
    private static String KEY_DATE = "Date";
    private static String KEY_RESULT = "Result";
    private static String KEY_RECORD_ID = "ID";
    private static String KEY_WEAKNESS_TAGS = "WeaknessTags";
    private String DATABASE_NAME;
    private int DATABASE_VERSION;
    private String RECORDS_TABLE_NAME;


    private AssetDatabaseHelper dbHelper;
    private SQLiteDatabase rdb;
    private SQLiteDatabase wdb;

    private QuestionCaller qc;
    private Context context;
    public RecordHandler(Context baseContext,AssetDatabaseHelper dbHelper, String DATABASE_NAME , int DATABASE_VERSION , String RECORDS_TABLE_NAME)
    {
        this.context = baseContext;
        this.dbHelper = dbHelper;
        this.DATABASE_NAME = DATABASE_NAME;
        this.DATABASE_VERSION = DATABASE_VERSION;
        this.RECORDS_TABLE_NAME = RECORDS_TABLE_NAME;


        rdb=dbHelper.getReadableDatabase();
        wdb=dbHelper.getWritableDatabase();

//
//        qc = new QuestionCaller(context,dbHelper,DATABASE_NAME,DATABASE_VERSION,QUESTIONS_TABLE_NAME,IMAGES_TABLE_NAME);

    }
    public Record makeRecord(Exam exam , ArrayList<Integer> userAnswers){
        Record record = new Record(exam,userAnswers);

        return record;
    }
    public void commitRecord(Record record)
    {
        int examId = record.getExamId();
        ArrayList <Integer> results = record.getResult();
        String sResult = "";
        for (int i = 0 ; i < results.size() ; i ++)
        {
            sResult += results.get(i);
            sResult += '|';
        }
        String sUserAnswers ="";
        String date=record.getDate();
        ArrayList<Integer> userAnswers;
        userAnswers = record.getUserAnswers();
        String sWeaknessTags = record.getStringOfWrongSonTags();
        for (int i = 0 ; i < 30 ; i ++)
            sUserAnswers += userAnswers.get(i) + "|";
        //ID is autoIncrement, primaryKey and unique.
        String SQLiteCommand = "INSERT INTO " + RECORDS_TABLE_NAME + " (" + KEY_EXAM_ID + ", " + KEY_RESULT + ", " + KEY_USER_ANSWERS + ", " + KEY_DATE + ", " + KEY_WEAKNESS_TAGS +
                ") VALUES (" + examId + ", \"" + sResult + "\", \"" + sUserAnswers + "\", \"" + date + "\" , \"" + sWeaknessTags+ "\")";
        Log.d("SQLiteCommand" , SQLiteCommand);
        wdb.execSQL(SQLiteCommand);
    }
    public ArrayList<Record> makeListByExamId(int examId){
        String SQLiteCommand = "SELECT * FROM " + RECORDS_TABLE_NAME + " WHERE " + KEY_EXAM_ID + "=" + examId;//sort folan shavad
        Log.d("SQLiteCommand",SQLiteCommand);
        ArrayList <Record> records = new ArrayList<>();
        Cursor cursor=rdb.rawQuery(SQLiteCommand,null);
        for (cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
        {
            Record r = new Record(cursor);
            records.add(r);
        }
        return records;
    }
    public Record getRecordById(int recordId)
    {
        String SQLiteCommand = "SELECT * FROM " + RECORDS_TABLE_NAME + " WHERE " + KEY_RECORD_ID + " = " + recordId;
        Log.d("SQLiteCommand" , SQLiteCommand);
        Record record;
        Cursor cursor = rdb.rawQuery(SQLiteCommand,null);
        cursor.moveToFirst();
        record = new Record(cursor);
        return record;
    }
    public static ArrayList<String> getWeaknessSonTags(AssetDatabaseHelper dbHelper,Record thisRecord,Context context)
    {
        Map<String ,Integer> wrongsMap = thisRecord.getWrongSonTags();
        Map<String , Integer> minWrong = new HashMap<>();
        ArrayList<String> weaknesSonTags = new ArrayList<>();
        for (Map.Entry<String,Integer> entry : wrongsMap.entrySet())
        {
            String key = entry.getKey();
            minWrong.put(key,1);
            Integer value = entry.getValue();
            if (value > minWrong.get(key))
                weaknesSonTags.add(key);
        }
        return weaknesSonTags;
    }
    public void deleteRecordById(int recordId)
    {
        String SQLiteCommand = "DELETE FROM " + RECORDS_TABLE_NAME + " WHERE " + KEY_RECORD_ID + " = " + recordId;
        Log.d("SQLiteCommand" , SQLiteCommand);
        wdb.execSQL(SQLiteCommand);
    }
    public void deleteRecordsByExamId(int examId)
    {
        String SQLiteCommand = "DELETE FROM " + RECORDS_TABLE_NAME + " WHERE " + KEY_EXAM_ID + " = " + examId;
        Log.d("SQLiteCommand" , SQLiteCommand);
        wdb.execSQL(SQLiteCommand);

    }




}
