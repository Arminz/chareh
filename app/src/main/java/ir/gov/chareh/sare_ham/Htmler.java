package ir.gov.chareh.sare_ham;

import android.util.Log;

/**
 * Created by user on 9/12/16.
 */
public class Htmler {

    public static String makeHtml(String s,String htmlTag)
    {
        if (s == null)
            s = " ";
        s = removeLeadingSpace(s);
//        s = removeLastPoint(s);
        if (htmlTag == null)
            return s;
        if (htmlTag .equals("x:y"))
           s = boldBeforeColumn(s);
        return s;


    }
    private static String boldBeforeColumn(String s)
    {
        String hs = "<b>";
        boolean flag = true;
        for (int i = 0 ; i < s.length() ; i ++) {
            hs += s.charAt(i);
            if (s.charAt(i) == ':' && flag) {
                hs += "</b>";
                flag = false;
            }
        }
        return hs;
    }
    private static String removeLastPoint(String s)
    {
        if (s.length() > 0 && (s.charAt(s.length()-1) == '.' || s.charAt(s.length()-1) == '.'))
            s=s.substring(0,s.length() - 1);
        return s;
    }
    private static String removeLeadingSpace(String s)
    {
        while (s.length() > 0 && s.charAt(s.length()-1) == ' ')
            s=s.substring(0,s.length()-1);
        return s;
    }

}
