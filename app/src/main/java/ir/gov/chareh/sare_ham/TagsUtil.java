package ir.gov.chareh.sare_ham;

/**
 * Created by armin on 1/7/18.
 * util class for tag calculations
 */

public class TagsUtil {

    public static int fatherNumber(String tag)
    {
        return Integer.parseInt(tag.substring(0, 1));
    }

    public static int maxSon(ChapterHandler chapterHandler, String tag)
    {
        return chapterHandler.getCountOfTag(Integer.toString(fatherNumber(tag)) + "?");
    }

    public static String nextTag(ChapterHandler chapterHandler, String tag)
    {
        String nextTag = "00";
        int sonNumber = Integer.parseInt(tag.substring(1,2));
        if(sonNumber + 1 >= maxSon(chapterHandler, tag)){
            int max_fathers = 4;
            if(!(fatherNumber(tag)+ 1 >= max_fathers)){
                nextTag = Integer.toString((fatherNumber(tag) + 1)) + "0";
            }
        }
        else{
            nextTag = Integer.toString(fatherNumber(tag))  + Integer.toString(sonNumber + 1);
        }
        return nextTag;

    }
}
