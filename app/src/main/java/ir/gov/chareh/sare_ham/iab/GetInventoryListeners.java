package ir.gov.chareh.sare_ham.iab;

/**
 * Created by armin on 3/21/17.
 */
public interface GetInventoryListeners {
    void onSuccessful(Boolean isPremium);
    void onError(String error);
    void onFinish();
}
