package ir.gov.chareh.sare_ham;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import static ir.gov.chareh.sare_ham.TagsUtil.nextTag;

public class teachingOne extends AppCompatActivity implements
//        NavigationView.OnNavigationItemSelectedListener,
        reached100 {

//    private bottom_sheet_pager_adapter bottom_sheet_adapter;
//    private ViewPager bottom_sheet_viewpager;
//    public View bottomSheet ;
//    public RelativeLayout disable ;

    private static ProgressDialog progressDialog;


    private static final String PAGE_TYPE = "PAGE_TYPE";
    private static final String TAG_QUESTIONS = "TAG_QUESTIONS";
    private static final String CHAPTER_TAG = "CHAPTER_TAG";

    final int max_fathers = 3;
    final int arc_duration = 150;
    final int arc_duration2 = 200;
    final int big_bang_duration = 400;
    final int fab_fade_duration = 150;
    final int hidden_scale_duration = 300;
    boolean is_fullscr_dlg_open = false;
    boolean bottom_dlg_open = false;
    boolean during_animation = false;
    private boolean outro_scroll = false;
    int rel_layout_height = 160;
    private ChapterHandler chapterHandler;
    private expandableScrollView ex_scrollview;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MyDatabase";
    private static final String QUESTION_TABLE_NAME = "Questions";
    private static final String IMAGES_TABLE_NAME = "Images";
    private static final String EXAMS_TABLE_NAME = "Exams";
    private static final String RECORDS_TABLE_NAME = "Records";
    private static final String CHAPTERS_TABLE_NAME = "Chapters";
    private static final String PARAGRAPHS_TABLE_NAME = "Paragraphs";

    private int full_progresses_count = 0;
    private boolean already_shown_100percent = false;

    final DisplayMetrics metrics = new DisplayMetrics();
    AssetDatabaseHelper dbHelper;
    private String tag = "23";
    private String next_tag;
    private boolean isSobheEmtehan;

    private void set_big_bang(ObjectAnimator animator){
        animator.setDuration(big_bang_duration);
        animator.setInterpolator(new AccelerateInterpolator());
        animator.setStartDelay(arc_duration + fab_fade_duration - 100);
    }
    private void set_hiddenScale(ObjectAnimator animator){
        animator.setDuration(hidden_scale_duration);
        animator.setInterpolator(new DecelerateInterpolator(1));
    }
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
        TextView tv = (TextView) findViewById(R.id.toolbar_text);
        tv.setText(title);
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (!isSobheEmtehan)
            update_is_read();
    }

    private void update_is_read(){
        ArrayList<Boolean> is_read = new ArrayList<>();
        is_read = ex_scrollview.getIsread();
        for(int i = 0; i < is_read.size(); i++){
            int int_read = 0;
            if(is_read.get(i))
                int_read = 1;
            chapterHandler.setChapterIsRead(tag + Integer.toString(i), int_read);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teaching_one);
        ColorStateList csl = new ColorStateList(new int[][]{new int[0]}, new int[]{getResources().getColor(R.color.colorAccent)});
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.fab).setBackgroundTintList(csl);
        }

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.tag = getIntent().getExtras().getString("tag", "23");
        this.next_tag = this.tag;
        this.isSobheEmtehan = (tag.equals("50"));


        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        //to set status bar color
        final Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        LinearLayout goto_tasviri_btns = (LinearLayout) getLayoutInflater().inflate(R.layout.goto_tasviri_buttons, null);
        final Button goto_tasviri_btn = (Button) goto_tasviri_btns.findViewById(R.id.goto_tasviri);
        goto_tasviri_btn.setText(getResources().getString(R.string.khatkeshies));
        goto_tasviri_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InitialTasvirisTask task = new InitialTasvirisTask();
                task.execute();
            }
        });
        goto_tasviri_btns.setGravity(Gravity.RIGHT);
        goto_tasviri_btns.setPadding((int) (24 * metrics.density), 0, (int) (24 * metrics.density),
                (int) (16 * metrics.density));

        final ImageView header = (ImageView) findViewById(R.id.my_im);
        int thisColor = R.color.colorPrimary;
        if (tag.equals("00")) {
            header.setBackground(getResources().getDrawable(R.drawable.header1_tablo));
            thisColor = R.color.mycyan;
        }else if (tag.equals("01")) {
            header.setBackground(getResources().getDrawable(R.drawable.header2_khatkeshi));
            thisColor = R.color.blue_grey_800;
        }else if (tag.equals("02")) {
            header.setBackground(getResources().getDrawable(R.drawable.header3_alayemehedayatkonandeh));
            thisColor = R.color.yellow_700;
        }else if (tag.equals("03")) {
            header.setBackground(getResources().getDrawable(R.drawable.header4_haghetaghadom));
            thisColor = R.color.pink_900;
        }else if (tag.equals("10")) {
            header.setBackground(getResources().getDrawable(R.drawable.header5_maadehayinnameh));
            thisColor = R.color.green_600;
        }else if (tag.equals("11")) {
            header.setBackground(getResources().getDrawable(R.drawable.header6_taarif));
            thisColor = R.color.green_800;
        }else if (tag.equals("12")) {
            header.setBackground(getResources().getDrawable(R.drawable.header7_mogharrarat));
            thisColor = R.color.deep_orange_400;
        }else if (tag.equals("20")) {
            header.setBackground(getResources().getDrawable(R.drawable.header8_imenidarbarabardigarkhodroha));
            thisColor = R.color.light_blue_800;
        }else if (tag.equals("21")) {
            header.setBackground(getResources().getDrawable(R.drawable.header9_sharayetegheireaadi));
            thisColor = R.color.cyan_800;
        }else if (tag.equals("22")) {
            header.setBackground(getResources().getDrawable(R.drawable.header10_bazdidekhodro));
            thisColor = R.color.teal_A700;
        }else if (tag.equals("23")) {
            header.setBackground(getResources().getDrawable(R.drawable.header11_sayerekarbaranejaddeh));
            thisColor = R.color.teal_700;
        }else if (tag.equals("24")) {
            header.setBackground(getResources().getDrawable(R.drawable.header12_vaziyatruhieranandeh));
            thisColor = R.color.deep_purple_600;
        }else if (tag.equals("25")) {
            header.setBackground(getResources().getDrawable(R.drawable.header13_motefarreghe));
            thisColor = R.color.indigo_A200;
        }else if (tag.equals("30")) {
            header.setBackground(getResources().getDrawable(R.drawable.header14_systemekhodro));
            thisColor = R.color.deep_purple_A200;
        }else if (tag.equals("31")) {
            header.setBackground(getResources().getDrawable(R.drawable.header15_servicevanegahdari));
            thisColor = R.color.red_A400;
        }else if (tag.equals("32")) {
            header.setBackground(getResources().getDrawable(R.drawable.header16_rahandaziekhodro));
            thisColor = R.color.teal_600;
        }else if (tag.equals("4")) {
            header.setBackground(getResources().getDrawable(R.drawable.header1_tablo));
            thisColor = R.color.mycyan;
        }
        else if (tag.equals("50")){
            header.setBackground(getResources().getDrawable(R.drawable.header16_rahandaziekhodro));
            thisColor = R.color.teal_600;
        }
        toolbar.setBackgroundColor(getResources().getColor(thisColor));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(thisColor));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            goto_tasviri_btn.setBackgroundTintList(getResources().getColorStateList(thisColor));
            goto_tasviri_btn.setTextColor(getResources().getColor(R.color.white));
        }

        //definitions
        final View behind_fab = findViewById(R.id.behind_fab);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        final RelativeLayout bottom_rel = (RelativeLayout) findViewById(R.id.bottom_relLayout);
        final ScrollView scrollView = (ScrollView) findViewById(R.id.main_scroll);

        //get screen size
        int width = metrics.widthPixels;
        final int height = metrics.heightPixels;
        //define arcs
        final Path path = new Path();
        RectF rectF = new RectF((44 - 28) * metrics.density, (float) (height - (56 + 16 + 16) * metrics.density), width - (44 + 28) * metrics.density, height - (56) * metrics.density);
        path.addArc(rectF, 0, 90);
        final Path path2 = new Path();
        RectF rectF2 = new RectF((44 - 28) * metrics.density, (float) (height - (233.5) * metrics.density), width - (44 + 28) * metrics.density, height - (56) * metrics.density);
        path2.addArc(rectF2, 0, 90);

        //load from database
        final LinearLayout great_mother = (LinearLayout) findViewById(R.id.mother);

        dbHelper = new AssetDatabaseHelper(
                getBaseContext(), DATABASE_NAME, DATABASE_VERSION);
        try {
            dbHelper.importIfNotExist();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("MTH", "Database import err:" + e.getMessage());

        }
        chapterHandler = new ChapterHandler(getBaseContext(), dbHelper, DATABASE_NAME, DATABASE_VERSION,
                CHAPTERS_TABLE_NAME, PARAGRAPHS_TABLE_NAME, IMAGES_TABLE_NAME);
        final ArrayList<View> list = new ArrayList<>();
        ArrayList<Boolean> is_read = new ArrayList<>();
        ArrayList<String> nave_names = new ArrayList<>();
        TextView child_title = (TextView) findViewById(R.id.child_title);
        child_title.setText(chapterHandler.getTagName(tag));
        if (!tag.equals("50"))
            setTitle(chapterHandler.getTagName(tag.substring(0, 1)));
        else
            setTitle("");

        {
            StyleAdapter adapter = new StyleAdapter(this);
            ArrayList<Paragraph> paragraphs = chapterHandler.CallParagraphsByChapterTag(tag + "9");
            for(int i = 0; i < paragraphs.size(); i++) {
                String text = Html.fromHtml(Htmler.makeHtml(paragraphs.get(i).getText(), paragraphs.get(i).getHtmlTag())).toString();
                great_mother.addView(adapter.get_content(text, StyleAdapter.TAG_default));
                View paddingBottom = new View(this);
                paddingBottom.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (48 * metrics.density)));
                great_mother.addView(paddingBottom);
            }
        }

        int nave_count = chapterHandler.getCountOfTag(tag    + "?");
        for(int j = 0; j < nave_count; j++) {
            ArrayList<Paragraph> paragraphs = chapterHandler.CallParagraphsByChapterTag(tag + Integer.toString(j));
            Boolean this_is_read;
            if(chapterHandler.getChapterIsRead(tag + Integer.toString(j)) == 0)
                this_is_read = false;
            else
                this_is_read = true;
            is_read.add(this_is_read);
            nave_names.add(chapterHandler.getTagName(tag + Integer.toString(j)));
            final LinearLayout mother = new LinearLayout(this);
            mother.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mother.setOrientation(LinearLayout.VERTICAL);
            StyleAdapter adapter = new StyleAdapter(this);
            View paddingTop = new View(this);
            paddingTop.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (24 * metrics.density)));
            mother.addView(paddingTop);
            for (int i = 0; i < paragraphs.size(); i++) {
                String text = Html.fromHtml(Htmler.makeHtml(paragraphs.get(i).getText(), paragraphs.get(i).getHtmlTag())).toString();
                mother.addView(adapter.get_content(text, StyleAdapter.TAG_default));
            }
            list.add(mother);
        }
        ex_scrollview = new expandableScrollView();
        ex_scrollview.addItems(great_mother, list, this, is_read, nave_names,isSobheEmtehan);
        if (!isSobheEmtehan) {
            LinearLayout end_buttons = (LinearLayout) getLayoutInflater().inflate(R.layout.endofteaching_buttons, null);
            end_buttons.setGravity(Gravity.RIGHT);
            end_buttons.setPadding((int) (24 * metrics.density), 0, (int) (24 * metrics.density), (int) (32 * metrics.density));
            final Button next_chapter = (Button) end_buttons.findViewById(R.id.goto_next_chapter);
            next_chapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    next_tag = nextTag(chapterHandler, tag);
                    scrollView.fullScroll(1);
                        outro_scroll = true;
                }
            });
            final Button questions_button = (Button) end_buttons.findViewById(R.id.goto_questions);
            questions_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(teachingOne.this, MyExam.class);
                    i.putExtra(PAGE_TYPE, TAG_QUESTIONS);
                    i.putExtra(CHAPTER_TAG, tag + "*");
                    startActivity(i);
                }
            });
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                questions_button.setBackgroundTintList(getResources().getColorStateList(thisColor));
                next_chapter.setBackgroundTintList(getResources().getColorStateList(thisColor));
            }
            if (tag.equals("01") || tag.equals("00"))
                great_mother.addView(goto_tasviri_btns);
            great_mother.addView(end_buttons);
        }
        final ValueAnimator pathAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
        pathAnimator.setDuration(arc_duration);
        pathAnimator.setStartDelay(fab_fade_duration);
        pathAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            float[] point = new float[2];
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float val = animation.getAnimatedFraction();
                PathMeasure pathMeasure = new PathMeasure(path, false);
                pathMeasure.getPosTan(pathMeasure.getLength() * val, point, null);
                if (!isSobheEmtehan && behind_fab != null) {
                    behind_fab.setX(point[0]);
                    behind_fab.setY(point[1]);
                }
            }
        });

//        float scale_th2 = (float) ((Math.sqrt(Math.pow(width / 2, 2) + Math.pow(72 - 28, 2))) / 56);
//        final ObjectAnimator ripple_scaleX = ObjectAnimator.ofFloat(behind_fab2, "scaleX", 1, scale_th2);
//        final ObjectAnimator ripple_scaleY = ObjectAnimator.ofFloat(behind_fab2, "scaleY", 1, scale_th2);
//        set_hiddenScale(ripple_scaleX);
//        set_hiddenScale(ripple_scaleY);
        /*final ValueAnimator pathAnimator2 = ValueAnimator.ofFloat(0.0f, 1.0f);
        pathAnimator2.setDuration(arc_duration2);
        pathAnimator2.setStartDelay(fab_fade_duration);
        pathAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            float[] point = new float[2];
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float val = animation.getAnimatedFraction();
                PathMeasure pathMeasure = new PathMeasure(path2, false);
                pathMeasure.getPosTan(pathMeasure.getLength() * val, point, null);
                if(behind_fab2.getY() >= (160 - 72) * metrics.density) {
                    rel_layout_height = 72;
                    CoordinatorLayout.LayoutParams newparams = (CoordinatorLayout.LayoutParams) bottom_rel.getLayoutParams();
                    newparams.height = (int) (72 * metrics.density);
                    newparams.anchorGravity = Gravity.BOTTOM | GravityCompat.END;
                    bottom_rel.setLayoutParams(newparams);
                    ripple_scaleX.start();
                    ripple_scaleY.start();
                }
                behind_fab2.setX(point[0]);
                behind_fab2.setY(point[1] - (height - metrics.density * rel_layout_height));
            }
        });*/

        float scale_th = (float) ((Math.sqrt(Math.pow(width / 2, 2) + Math.pow(height - 28, 2))) / 56);
        final ObjectAnimator big_bangX = ObjectAnimator.ofFloat(behind_fab, "scaleX", 1, scale_th);
        big_bangX.setInterpolator(new DecelerateInterpolator());
        final ObjectAnimator big_bangY = ObjectAnimator.ofFloat(behind_fab, "scaleY", 1, scale_th);
        big_bangY.setInterpolator(new DecelerateInterpolator());
        set_big_bang(big_bangX);
        set_big_bang(big_bangY);

        final AlphaAnimation fab_fade = new AlphaAnimation(1, 0);
        fab_fade.setFillAfter(true);
        fab_fade.setDuration(fab_fade_duration);

        if (fab != null && !isSobheEmtehan) {
            final int finalThisColor = thisColor;
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Button pr_cn_btn = (Button) findViewById(R.id.progress_confirm_button);
//                    pr_cn_btn.setBackgroundColor(finalThisColor);
//                    View toolbar1 = findViewById(R.id.progress_toolbar);
//                    toolbar1.setBackgroundColor(finalThisColor);
                    if(!during_animation) {
                        during_animation = true;
                        fab.setClickable(false);
//                        fab2.setClickable(false);
                        behind_fab.setVisibility(View.VISIBLE);
                        pathAnimator.start();
                        big_bangX.start();
                        big_bangY.start();
                        fab.startAnimation(fab_fade);
//                        fab2.startAnimation(fab_fade);
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                open_fullscr_dlg();
                            }
                        }, big_bangX.getStartDelay() + 200);
                    }
                }
            });
        }
        /*if (fab2 != null) {
            fab2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!during_animation) {
                        during_animation = true;
                        if (bottomSheet.getVisibility() == View.INVISIBLE) {
                            bottomSheet.setVisibility(View.VISIBLE);
                        }
                        if (disable.getVisibility() == View.INVISIBLE) {
                            fab.setClickable(false);
                            fab2.setClickable(false);
                            behind_fab2.setVisibility(View.VISIBLE);
                            pathAnimator2.start();
                            fab.startAnimation(fab_fade);
                            fab2.startAnimation(fab_fade);
                            SlideUpBottomSheet();
                        }
                        disable.setVisibility(View.VISIBLE);
                        bottom_dlg_open = true;
                    }
                }
            });
        }*/

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
        final TextView header_title = (TextView) findViewById(R.id.child_title);
        header_title.getLayoutParams().width = metrics.widthPixels/2 ;
        header_title.setY((metrics.widthPixels *(1)) / (64));
        final ImageView imageView = (ImageView) findViewById(R.id.my_im);
        imageView.setLayoutParams(new RelativeLayout.LayoutParams(metrics.widthPixels, (metrics.widthPixels * 9) / 16));
        scrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            int current_loc = scrollView.getScrollY();
            @Override
            public void onScrollChanged() {
                if(outro_scroll && scrollView.getScrollY() == 0){
                    Intent next_page;
                    if (next_tag.equals("00"))
                        next_page = new Intent(teachingOne.this, teachingOne_fortasviri.class);
                    else
                        next_page = new Intent(teachingOne.this, teachingOne.class);
                    next_page.putExtra("tag", next_tag);
                    next_page.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(next_page);
                }
                float loc;
                if (current_loc - scrollView.getScrollY() > 70) {
                    current_loc = scrollView.getScrollY();
                    if (!isSobheEmtehan && fab != null) {
                        fab.show();
                        fab.setClickable(true);
                    }
//                    fab2.show();
//                    fab2.setClickable(true);
                }else if (current_loc - scrollView.getScrollY() < -70){
                    current_loc = scrollView.getScrollY();
                    if (!isSobheEmtehan && fab != null) {
                        fab.hide();
                        fab.setClickable(false);
                    }
//                    fab2.hide();
//                    fab2.setClickable(false);
                }
                loc = scrollView.getScrollY() * -1;
                imageView.setY(loc/2);
                imageView.setAlpha(1f+loc/(imageView.getHeight()));
                header_title.setAlpha(1f+loc/(imageView.getHeight() * 0.25f));
            }
        });
        /*disable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (during_animation == false) {
                    if (bottom_dlg_open) {
                        SlideDownBottomSheet();
                        reset_bottom_dlg();
                        disable.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });*/
        checkSobheAzmmon();
    }
    public void checkSobheAzmmon(){
        if (!isSobheEmtehan)
            return;
        View fab = findViewById(R.id.fab);
        if (fab != null) {
            fab.setClickable(false);
            fab.setVisibility(View.INVISIBLE);
            fab.setEnabled(false);
        }


    }
    public void open_fullscr_dlg(){
        if (!isSobheEmtehan)
            update_is_read();
        final View included_dlg = findViewById(R.id.included_fullscrn_dlg);
        View app_bar_layout = findViewById(R.id.appbar_layout);
        View second_toolbar = findViewById(R.id.second_toolbar);
        View stable_backgrnd = findViewById(R.id.stable_background);
        stable_backgrnd.setClickable(true);

        AlphaAnimation fade_out = new AlphaAnimation(1, 0);
        fade_out.setDuration(300);
        fade_out.setFillAfter(true);
        AlphaAnimation fade_in = new AlphaAnimation(0, 1);
        fade_in.setDuration(300);
        fade_in.setFillAfter(true);
        Animation animation_bottom_sheet = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_up_dlg);
        int bottom_sheet_anim_duration = 400;
        animation_bottom_sheet.setDuration(bottom_sheet_anim_duration);
        animation_bottom_sheet.setInterpolator(new DecelerateInterpolator(1.5f));
        animation_bottom_sheet.setFillAfter(true);
        ObjectAnimator move_up_dlg = ObjectAnimator.ofFloat(included_dlg, "y", metrics.heightPixels, 0);
        move_up_dlg.setDuration(400);
        ObjectAnimator fade_backgrnd = ObjectAnimator.ofFloat(stable_backgrnd, "alpha", 0, 1);
        fade_backgrnd.setDuration(400);
        app_bar_layout.startAnimation(fade_out);
        second_toolbar.startAnimation(fade_in);
//        View toolbar_back_btn = findViewById(R.id.toolbar_back_btn);
//        toolbar_back_btn.setClickable(true);
        View toolbar = findViewById(R.id.appbar_layout);
        toolbar.setClickable(false);
        included_dlg.startAnimation(animation_bottom_sheet);
        animation_bottom_sheet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                included_dlg.setY(0);
            }
            @Override
            public void onAnimationEnd(Animation animation) {during_animation = false;set_up_fullscr_dlg();}
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
//        move_up_dlg.start();
        stable_backgrnd.setY(24 * metrics.density);
        fade_backgrnd.start();
        is_fullscr_dlg_open = true;
        if(already_shown_100percent){
            findViewById(R.id.first_progress_tv).setVisibility(View.INVISIBLE);
            findViewById(R.id.second_progress_tv).setVisibility(View.INVISIBLE);
            findViewById(R.id.progress1).setVisibility(View.GONE);
            findViewById(R.id.progress2).setVisibility(View.GONE);
            View golden_100 = findViewById(R.id.reached100_circle);
            ObjectAnimator golden_bigX = ObjectAnimator.ofFloat(golden_100, "ScaleX", 1.2f, (float) 1.5);
            golden_bigX.setStartDelay(400);
            golden_bigX.setDuration(200);
            ObjectAnimator golden_bigY = ObjectAnimator.ofFloat(golden_100, "ScaleY", 1.2f, (float) 1.5);
            golden_bigY.setStartDelay(400);
            golden_bigY.setDuration(200);
            ObjectAnimator golden_smallX = ObjectAnimator.ofFloat(golden_100, "ScaleX", (float) 1.5, 1.2f);
            golden_smallX.setStartDelay(400 + 200);
            golden_smallX.setDuration(200);
            ObjectAnimator golden_smallY = ObjectAnimator.ofFloat(golden_100, "ScaleY", (float) 1.5, 1.2f);
            golden_smallY.setStartDelay(400 + 200);
            golden_smallY.setDuration(200);
            golden_bigX.start();
            golden_bigY.start();
            golden_smallX.start();
            golden_smallY.start();
        }
    }
    public void set_up_fullscr_dlg(){
        QuestionCaller questionCaller = new QuestionCaller(getBaseContext(), dbHelper, DATABASE_NAME, DATABASE_VERSION, QUESTION_TABLE_NAME, IMAGES_TABLE_NAME);
        View progress1_V = findViewById(R.id.progress1);
        View progress2_V = findViewById(R.id.progress2);
        progress1_V.setVisibility(View.VISIBLE);
        progress2_V.setVisibility(View.VISIBLE);
        int progress1_out_of100 = 0;
        for(int i = 0; i < chapterHandler.getCountOfTag(tag + "?"); i++){
            if(chapterHandler.getChapterIsRead(tag + Integer.toString(i)) == 1)
                progress1_out_of100++;
        }
        progress1_out_of100 *= 100;
        progress1_out_of100 /= chapterHandler.getCountOfTag(tag + "?");
        CircleProgressBar progress1 = new CircleProgressBar(progress1_V, progress1_out_of100, getApplicationContext(), this);

        int progress2_out_of100 = (100 * questionCaller.getCountOfCheckedTestsByChapterTag(tag + "?"))/questionCaller.getCountOfTestsByChapterTag(tag + "?");
        CircleProgressBar progress2 = new CircleProgressBar(progress2_V, progress2_out_of100, getApplicationContext(), this);

        if (!already_shown_100percent) {
            progress1.start();
            progress2.start();
        }
    }
    public void reset_positions(){
        View background_view = findViewById(R.id.stable_background);
        View fab = findViewById(R.id.fab);
        View progress1_V = findViewById(R.id.progress1);
        View progress2_V = findViewById(R.id.progress2);

        if (!isSobheEmtehan && fab != null) {
            fab.setClickable(true);

            progress1_V.setVisibility(View.INVISIBLE);
            progress2_V.setVisibility(View.INVISIBLE);
            background_view.setAlpha(0);
        }
    }
    public void close_fullscr_dlg(){
        final int out_duration = 300;
        full_progresses_count = 0;
        View background_view = findViewById(R.id.stable_background);
        background_view.setClickable(false);
        final View included_dlg = findViewById(R.id.included_fullscrn_dlg);
        View app_bar_layout = findViewById(R.id.appbar_layout);
        View second_toolbar = findViewById(R.id.second_toolbar);
        View fab = findViewById(R.id.fab);
        ObjectAnimator move_out_dlg = ObjectAnimator.ofFloat(included_dlg, "y", 0, metrics.heightPixels);
        move_out_dlg.setDuration(out_duration);
        ObjectAnimator move_out_background = ObjectAnimator.ofFloat(background_view, "y", background_view.getY(), metrics.heightPixels);
        Animation move_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_down_outofpage);
        move_down.setFillAfter(true);
        move_down.setDuration(out_duration);
        move_out_background.setDuration(out_duration);
        AlphaAnimation fade_out = new AlphaAnimation(1, 0);
        fade_out.setDuration(out_duration);
        fade_out.setFillAfter(true);
        AlphaAnimation fade_in = new AlphaAnimation(0, 1);
        fade_in.setDuration(out_duration);
        fade_in.setFillAfter(true);

        View behind_fab = findViewById(R.id.behind_fab);
        if (behind_fab != null && !isSobheEmtehan) {
            behind_fab.setX(fab.getX());
            behind_fab.setY(fab.getY());
            behind_fab.setScaleX(1);
            behind_fab.setScaleY(1);
            behind_fab.setVisibility(View.INVISIBLE);
        }
        app_bar_layout.startAnimation(fade_in);
        second_toolbar.startAnimation(fade_out);
//        View toolbar_back_btn = findViewById(R.id.toolbar_back_btn);
//        toolbar_back_btn.setClickable(false);
        View toolbar = findViewById(R.id.appbar_layout);
        toolbar.setClickable(true);
        fab.startAnimation(fade_in);
        move_out_background.start();
//        move_out_dlg.start();
        included_dlg.startAnimation(move_down);
        move_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                included_dlg.setY(metrics.heightPixels);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
//        background_view.startAnimation(move_down);
        is_fullscr_dlg_open = false;
        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                reset_positions();
            }
        }, out_duration);
    }
    public void toolbar_back_clicked(View view){
        close_fullscr_dlg();
    }
    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (!during_animation) {
//            if ((drawer.isDrawerOpen(GravityCompat.START))) {
//                drawer.closeDrawer(GravityCompat.START);
            if ((is_fullscr_dlg_open)) {
                close_fullscr_dlg();
            } else {
                Intent teachingActivity = new Intent(this, TeachingActivity.class);
                if (isSobheEmtehan)
                    teachingActivity = new Intent(this,First_page.class);
                teachingActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(teachingActivity);
//                super.onBackPressed();
            }
        }
    }


//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    @Override
    public void callBack(String result) {
        full_progresses_count++;
        if(full_progresses_count > 1 && !already_shown_100percent){
            final AlphaAnimation fade_out = new AlphaAnimation(1, 0);
            fade_out.setDuration(fab_fade_duration);
            fade_out.setFillAfter(true);
            if (findViewById(R.id.first_progress_tv).getVisibility() == View.VISIBLE)
                findViewById(R.id.first_progress_tv).startAnimation(fade_out);
            if (findViewById(R.id.second_progress_tv).getVisibility() == View.VISIBLE)
                findViewById(R.id.second_progress_tv).startAnimation(fade_out);
            Typeface noto = Typeface.createFromAsset(getAssets(), "fonts/IRANSans(FaNum)_Bold.ttf");
            {
                TextView tv = (TextView) findViewById(R.id.text_view_for_100precent);
                tv.setTypeface(noto);
            }
            already_shown_100percent = true;
            View mother_of_progress = findViewById(R.id.mother_of_progress);
            View progress1 = findViewById(R.id.progress1);
            View progress2 = findViewById(R.id.progress2);
            View golden_100 = findViewById(R.id.reached100_circle);
            TranslateAnimation go_down = new TranslateAnimation(0, 0, 0, mother_of_progress.getHeight() / 2 - 135 - progress1.getY());
            TranslateAnimation go_up = new TranslateAnimation(0, 0, 0, mother_of_progress.getHeight()/2 - 135 - progress2.getY());
            go_down.setDuration(300);
            go_up.setDuration(300);

            AlphaAnimation fade_half = new AlphaAnimation(1, 0.5f);
            fade_half.setDuration(300);

            AnimationSet set1 = new AnimationSet(true);
            AnimationSet set2 = new AnimationSet(true);
            set1.addAnimation(go_down);
            set1.addAnimation(fade_half);
            set2.addAnimation(go_up);
            set2.addAnimation(fade_half);
            set1.setFillAfter(true);
            set2.setFillAfter(true);
            /*set1.setStartOffset(200);
            set2.setStartOffset(200);*/

            ObjectAnimator golden_fade = ObjectAnimator.ofFloat(golden_100, "alpha", 0, 1);
            golden_fade.setStartDelay(300);
            golden_fade.setDuration(200);
            ObjectAnimator golden_bigX = ObjectAnimator.ofFloat(golden_100, "ScaleX", 1, (float) 1.5);
            golden_bigX.setStartDelay(400);
            golden_bigX.setDuration(200);
            ObjectAnimator golden_bigY = ObjectAnimator.ofFloat(golden_100, "ScaleY", 1, (float) 1.5);
            golden_bigY.setStartDelay(400);
            golden_bigY.setDuration(200);
            ObjectAnimator golden_smallX = ObjectAnimator.ofFloat(golden_100, "ScaleX", (float) 1.5, 1.2f);
            golden_smallX.setStartDelay(400 + 200);
            golden_smallX.setDuration(200);
            ObjectAnimator golden_smallY = ObjectAnimator.ofFloat(golden_100, "ScaleY", (float) 1.5, 1.2f);
            golden_smallY.setStartDelay(400 + 200);
            golden_smallY.setDuration(200);
            golden_fade.start();
            golden_bigX.start();
            golden_bigY.start();
            golden_smallX.start();
            golden_smallY.start();

            progress1.startAnimation(set1);
            progress2.startAnimation(set2);
        }
    }

    public void progressConfirm(View view) {
        onBackPressed();
    }

    /*public static class bottomsheet_padapter extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public bottomsheet_padapter() {
        }

        public static bottomsheet_padapter newInstance(int sectionNumber) {
            bottomsheet_padapter fragment = new bottomsheet_padapter();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.bottom_sheet_fragment, container, false);
            ViewPager innerViewPager = (ViewPager) rootView.findViewById(R.id.inner_container);
            innerSectionsPagerAdapter inneradapter = new innerSectionsPagerAdapter(this.getChildFragmentManager());
            innerViewPager.setAdapter(inneradapter);
            return rootView;
        }
    }
    public static class innerSectionsPagerAdapter extends FragmentPagerAdapter{
        public innerSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return innerPlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 3;
        }

    }
    public static class innerPlaceholderFragment extends Fragment{
        public innerPlaceholderFragment(){}
        private static final String ARG_SECTION_NUMBER = "section_number";
        public static innerPlaceholderFragment newInstance(int sectionNumber) {
            innerPlaceholderFragment fragment = new innerPlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.bottomsheet_inner_fragment_main, container, false);

            return rootView;
        }
    }
    public class bottom_sheet_pager_adapter extends FragmentPagerAdapter {

        public bottom_sheet_pager_adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return bottomsheet_padapter.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }

    public void SlideUpBottomSheet(){
        ObjectAnimator slide_up = new ObjectAnimator().ofFloat(bottomSheet,"translationY",0,-(bottomSheet.getHeight()));
        slide_up.setDuration(300);
        slide_up.setStartDelay(fab_fade_duration + arc_duration2);
        slide_up.setInterpolator(new DecelerateInterpolator());
        slide_up.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}
            @Override
            public void onAnimationEnd(Animator animation) {
                during_animation = false;
            }
            @Override
            public void onAnimationCancel(Animator animation) {}
            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        slide_up.start();
    }

    public void SlideDownBottomSheet(){
        ObjectAnimator slide_down = new ObjectAnimator().ofFloat(bottomSheet,"translationY",-bottomSheet.getHeight(),0);
        slide_down.setDuration(200);
        slide_down.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                during_animation = true;
            }
            @Override
            public void onAnimationEnd(Animator animation) {
                during_animation = false;
            }
            @Override
            public void onAnimationCancel(Animator animation) {}
            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        slide_down.start();
    }
    void reset_bottom_dlg(){

        AlphaAnimation fade_in = new AlphaAnimation(0, 1);
        fade_in.setDuration(200);
        fade_in.setFillAfter(true);

        View fab = findViewById(R.id.fab);
        View fab2 = findViewById(R.id.fab2);
        View behind_fab2 = findViewById(R.id.behind_fab2);
        RelativeLayout bottom_rel = (RelativeLayout) findViewById(R.id.bottom_relLayout);
        CoordinatorLayout.LayoutParams newparams = (CoordinatorLayout.LayoutParams) bottom_rel.getLayoutParams();
        newparams.height = (int) (160 * metrics.density);
        newparams.anchorGravity = Gravity.RIGHT | Gravity.TOP;
        bottom_rel.setLayoutParams(newparams);
        fab.setClickable(true);
        fab2.setClickable(true);

        fab.startAnimation(fade_in);
        fab2.startAnimation(fade_in);

        behind_fab2.setScaleX(1);
        behind_fab2.setScaleY(1);
        behind_fab2.setX(fab2.getX());
        behind_fab2.setY(fab2.getY() - (metrics.heightPixels - metrics.density * 160));
        behind_fab2.setVisibility(View.INVISIBLE);
        bottom_dlg_open = false;
        rel_layout_height = 160;
    }*/

    private class InitialTasvirisTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(teachingOne.this, null, getString(R.string.message_progressDialog_loadExam), true);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            Intent next_page = new Intent(teachingOne.this, teachingAzmoon.class);
            next_page.putExtra("tag", tag);
            next_page.putExtra(teachingAzmoon.TAG_TITLE, getResources().getString(R.string.khatkeshies)/*خط کشی*/);
            startActivity(next_page);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (progressDialog != null)
                progressDialog.dismiss();
        }
    }
}
