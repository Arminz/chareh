package ir.gov.chareh.sare_ham;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by user on 8/8/16.
 */
public class ExamCaller {
    private static String KEY_ID = "ID";
    private static String KEY_QUESTIONS_ID = "QuestionsID";
    private static String KEY_RECORDS_ID = "RecordsID";



    private Context context;

    private String DATABASE_NAME;
    private String EXAMS_TABLE_NAME;
    private String QUESTIONS_TABLE_NAME;
    private String IMAGES_TABLE_NAME;
    int DATABASE_VERSION;
    private AssetDatabaseHelper dbHelper;
    private  SQLiteDatabase rdb;
    private SQLiteDatabase wdb;

    private QuestionCaller qc;

    public ExamCaller(Context baseContext,AssetDatabaseHelper dbHelper, String DATABASE_NAME , int DATABASE_VERSION , String EXAMS_TABLE_NAME,String QUESTIONS_TABLE_NAME,String IMAGES_TABLE_NAME){
        context = baseContext;
        this.DATABASE_NAME = DATABASE_NAME;
        this.DATABASE_VERSION = DATABASE_VERSION;
        this.EXAMS_TABLE_NAME = EXAMS_TABLE_NAME;
        this.QUESTIONS_TABLE_NAME = QUESTIONS_TABLE_NAME;
        this.IMAGES_TABLE_NAME = IMAGES_TABLE_NAME;
        this.dbHelper = dbHelper;
//        dbHelper = new AssetDatabaseHelper(context,DATABASE_NAME , DATABASE_VERSION);
        rdb = dbHelper.getReadableDatabase();
        wdb = dbHelper.getWritableDatabase();
        qc = new QuestionCaller(baseContext,dbHelper,DATABASE_NAME,DATABASE_VERSION,QUESTIONS_TABLE_NAME,IMAGES_TABLE_NAME);
    }


    public Exam callExamById(int id)
    {
        String SQLiteCommand = "SELECT * FROM " + EXAMS_TABLE_NAME +" WHERE " + KEY_ID + " = " + id;
        Cursor cursor = rdb.rawQuery(SQLiteCommand , null );
        Log.d("SQLiteCommand" , SQLiteCommand );
        cursor.moveToFirst();//very very important!!!
        Exam thisExam = new Exam(cursor);
        for (int i = 0 ; i < 30 ; i ++) {
            Log.d("fk", "is going to set " + i);
            thisExam.setIthQuestion(qc.callQuestionById(thisExam.getIthQuestionID(i)), i);
            Log.d("fk", i + "setted");
        }
        return thisExam;
    }
    public Exam callRandomExam(ArrayList<String> chapterTags, ArrayList <Integer> counts)
    {
        Exam thisExam = new Exam(null);
        ArrayList<Question> questions = new ArrayList<>();
        for (int i = 0 ; i < chapterTags.size() ; i ++)
        {
            questions = qc.callQuestionsByChapterTag(chapterTags.get(i),counts.get(i),true);
            thisExam.addQuestions(questions);
        }
        return thisExam;
    }
    public Exam callRandomExam()
    {
        ArrayList<String> chapterTags = new ArrayList<String>();
        ArrayList<Integer> counts = new ArrayList<Integer>();
//        chapterTags.add("00*");
//        counts.add(10);
//
//        chapterTags.add("007");
//        counts.add(1);
//
//        chapterTags.add("01*");
//        counts.add(1);
//
//        chapterTags.add("02*");
//        counts.add(1);
//
//        chapterTags.add("03*");
//        counts.add(2);
//
//        chapterTags.add("10*");
//        counts.add(2);
//
//        chapterTags.add("11*");
//        counts.add(1);
//
//        chapterTags.add("120");
//        counts.add(1);
//        chapterTags.add("121");
//        counts.add(1);
//        chapterTags.add("122");
//        counts.add(1);
//        chapterTags.add("12*");
//        counts.add(1);
//
//        chapterTags.add("20*");
//        counts.add(1);
//
//        chapterTags.add("21*");
//        counts.add(1);
//
//        Random rn = new Random();
//        int x = rn.nextInt() % 2 + 2;
//        String s = "2" + x + "*";
//        chapterTags.add(s);//random 22 ya 23
//        counts.add(1);
//
//        chapterTags.add("24");
//        counts.add(1);
//
//        chapterTags.add("25*");
//        counts.add(2);
//
//
//        chapterTags.add("3*");
//        counts.add(1);



        chapterTags.add("10*");
        counts.add(2);

        chapterTags.add("11*");
        counts.add(1);

        chapterTags.add("120");
        counts.add(1);
        chapterTags.add("121");
        counts.add(1);
        chapterTags.add("122");
        counts.add(1);
        chapterTags.add("12*");
        counts.add(1);


        chapterTags.add("01*");
        counts.add(1);




        chapterTags.add("20*");
        counts.add(1);

        chapterTags.add("21*");
        counts.add(1);

        chapterTags.add("02*");
        counts.add(1);

        Random rn = new Random();
        int x = rn.nextInt() % 2 + 2;
        String s = "2" + x + "*";
        chapterTags.add(s);//random 22 ya 23
        counts.add(1);

        chapterTags.add("*");
        counts.add(1);


        chapterTags.add("03*");
        counts.add(2);
        chapterTags.add("24");
        counts.add(1);

        chapterTags.add("25*");
        counts.add(2);


        chapterTags.add("3*");
        counts.add(1);

        chapterTags.add("00*");
        counts.add(10);

        chapterTags.add("007");
        counts.add(1);

       return callRandomExam(chapterTags,counts);
    }


}
