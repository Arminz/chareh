package ir.gov.chareh.sare_ham;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

public class teachingAzmoon extends AppCompatActivity implements
//        NavigationView.OnNavigationItemSelectedListener,
        reached100 {

    private bottom_sheet_pager_adapter bottom_sheet_adapter;
    private static tasviri_adapter tasviri_adapter;
    private ViewPager bottom_sheet_viewpager;
    private static ViewPager tasviri_viewpager;
    public View bottomSheet ;
    public RelativeLayout disable ;

    private int max_nave;
    final int arc_duration = 150;
    final int arc_duration2 = 200;
    final int big_bang_duration = 400;
    final int fab_fade_duration = 150;
    final int hidden_scale_duration = 300;
    boolean is_fullscr_dlg_open = false;
    boolean bottom_dlg_open = false;
    boolean during_animation = false;
    int rel_layout_height = 160;
    private ChapterHandler chapterHandler;
    private static ImageCaller imageCaller;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MyDatabase";
    private static final String QUESTION_TABLE_NAME = "Questions";
    private static final String IMAGES_TABLE_NAME = "Images";
    private static final String EXAMS_TABLE_NAME = "Exams";
    private static final String RECORDS_TABLE_NAME = "Records";
    private static final String CHAPTERS_TABLE_NAME = "Chapters";
    private static final String PARAGRAPHS_TABLE_NAME = "Paragraphs";

    public static final String TAG_TITLE = "this is title!";

    static final DisplayMetrics metrics = new DisplayMetrics();
    static ArrayList<Integer> images = new ArrayList<>();
    static ArrayList<ArrayList<Image>> thumb_images = new ArrayList<>();

    private String tag = "23";

    private void set_big_bang(ObjectAnimator animator){
        animator.setDuration(big_bang_duration);
        animator.setInterpolator(new AccelerateInterpolator());
        animator.setStartDelay(arc_duration + fab_fade_duration - 100);
    }
    private void set_hiddenScale(ObjectAnimator animator){
        animator.setDuration(hidden_scale_duration);
        animator.setInterpolator(new DecelerateInterpolator(1));
    }
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
        TextView tv = (TextView) findViewById(R.id.toolbar_text);
        tv.setText(title);
    }

    @Override
    protected void onPause() {
        super.onPause();
        thumb_images.clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teaching_azmoon);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        this.tag = getIntent().getExtras().getString("tag", "23");

        tasviri_adapter = new tasviri_adapter(getSupportFragmentManager());
        bottom_sheet_adapter = new bottom_sheet_pager_adapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        bottom_sheet_viewpager = (ViewPager) findViewById(R.id.container);
        bottom_sheet_viewpager.setAdapter(bottom_sheet_adapter);
        tasviri_viewpager = (ViewPager) findViewById(R.id.tabloos_viewpager);
        tasviri_viewpager.setAdapter(tasviri_adapter);
        disable = (RelativeLayout) findViewById(R.id.disable);
        disable.setVisibility(View.INVISIBLE);
        bottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheet.setVisibility(View.INVISIBLE);
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(bottom_sheet_viewpager);
        if(tabs.getTabCount() > 3){
            tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        //to set status bar color
        final Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        //definitions
//        final View behind_fab = findViewById(R.id.behind_fab);
        final View behind_fab2 = findViewById(R.id.behind_fab2);
//        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        final RelativeLayout bottom_rel = (RelativeLayout) findViewById(R.id.bottom_relLayout);
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        //get screen size
        int width = metrics.widthPixels;
        final int height = metrics.heightPixels;
        //define arcs
        final Path path = new Path();
        RectF rectF = new RectF((44 - 28) * metrics.density, (float) (height - (56 + 16 + 16) * metrics.density), width - (44 + 28) * metrics.density, height - (56) * metrics.density);
        path.addArc(rectF, 0, 90);
        final Path path2 = new Path();
        RectF rectF2 = new RectF((44 - 28) * metrics.density, (float) (height - (233.5) * metrics.density), width - (44 + 28) * metrics.density, height - (56) * metrics.density);
        path2.addArc(rectF2, 0, 90);
        //load from database
        AssetDatabaseHelper dbHelper = new AssetDatabaseHelper(
                getBaseContext(), DATABASE_NAME, DATABASE_VERSION);
        try {
            dbHelper.importIfNotExist();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("MTH", "Database import err:" + e.getMessage());

        }
        chapterHandler = new ChapterHandler(getBaseContext(), dbHelper, DATABASE_NAME, DATABASE_VERSION,
                CHAPTERS_TABLE_NAME, PARAGRAPHS_TABLE_NAME, IMAGES_TABLE_NAME);
        imageCaller = new ImageCaller(getBaseContext(), dbHelper, DATABASE_NAME, DATABASE_VERSION, IMAGES_TABLE_NAME);
        max_nave = chapterHandler.getCountOfTag(tag + "?");
        if (tag.equals("00"))
            max_nave --;//exception

        images.clear();
        for(int i = 0 ; i < max_nave; i++){
            images.addAll(imageCaller.getListIdByTag(tag + Integer.toString(i)));
            tasviri_adapter.notifyDataSetChanged();
        }
        for(int i = 0 ; i < max_nave; i++){
            thumb_images.add(imageCaller.getThumbnailImages(tag + Integer.toString(i)));
            bottom_sheet_adapter.notifyDataSetChanged();
        }
        final ValueAnimator pathAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
        pathAnimator.setDuration(arc_duration);
        pathAnimator.setStartDelay(fab_fade_duration);
        pathAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            float[] point = new float[2];
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float val = animation.getAnimatedFraction();
                PathMeasure pathMeasure = new PathMeasure(path, false);
                pathMeasure.getPosTan(pathMeasure.getLength() * val, point, null);
//                behind_fab.setX(point[0]);
//                behind_fab.setY(point[1]);
            }
        });

        float scale_th2 = (float) ((Math.sqrt(Math.pow(width / 2, 2) + Math.pow(72 - 28, 2))) / 56);
        final ObjectAnimator ripple_scaleX = ObjectAnimator.ofFloat(behind_fab2, "scaleX", 1, scale_th2);
        final ObjectAnimator ripple_scaleY = ObjectAnimator.ofFloat(behind_fab2, "scaleY", 1, scale_th2);
        set_hiddenScale(ripple_scaleX);
        set_hiddenScale(ripple_scaleY);
        final ValueAnimator pathAnimator2 = ValueAnimator.ofFloat(0.0f, 1.0f);
        pathAnimator2.setDuration(arc_duration2);
        pathAnimator2.setStartDelay(fab_fade_duration);
        pathAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            float[] point = new float[2];
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float val = animation.getAnimatedFraction();
                PathMeasure pathMeasure = new PathMeasure(path2, false);
                pathMeasure.getPosTan(pathMeasure.getLength() * val, point, null);
                if(behind_fab2.getY() >= (160 - 72) * metrics.density) {
                    rel_layout_height = 72;
                    CoordinatorLayout.LayoutParams newparams = (CoordinatorLayout.LayoutParams) bottom_rel.getLayoutParams();
                    newparams.height = (int) (72 * metrics.density);
                    newparams.anchorGravity = Gravity.BOTTOM | GravityCompat.END;
                    bottom_rel.setLayoutParams(newparams);
                    ripple_scaleX.start();
                    ripple_scaleY.start();
                }
                behind_fab2.setX(point[0]);
                behind_fab2.setY(point[1] - (height - metrics.density * rel_layout_height));
            }
        });

        float scale_th = (float) ((Math.sqrt(Math.pow(width / 2, 2) + Math.pow(height - 28, 2))) / 56);
//        final ObjectAnimator big_bangX = ObjectAnimator.ofFloat(behind_fab, "scaleX", 1, scale_th);
//        big_bangX.setInterpolator(new DecelerateInterpolator());
//        final ObjectAnimator big_bangY = ObjectAnimator.ofFloat(behind_fab, "scaleY", 1, scale_th);
//        big_bangY.setInterpolator(new DecelerateInterpolator());
//        set_big_bang(big_bangX);
//        set_big_bang(big_bangY);

        final AlphaAnimation fab_fade = new AlphaAnimation(1, 0);
        fab_fade.setFillAfter(true);
        fab_fade.setDuration(fab_fade_duration);

//        if (fab != null) {
//            fab.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(!during_animation) {
//                        during_animation = true;
//                        fab.setClickable(false);
//                        fab2.setClickable(false);
//                        behind_fab.setVisibility(View.VISIBLE);
//                        pathAnimator.start();
//                        big_bangX.start();
//                        big_bangY.start();
//                        fab.startAnimation(fab_fade);
//                        fab2.startAnimation(fab_fade);
//                        final Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                open_fullscr_dlg();
//                            }
//                        }, big_bangX.getStartDelay() + 200);
//                    }
//                }
//            });
//        }
        if (fab2 != null) {
            fab2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!during_animation) {
                        during_animation = true;
                        if (bottomSheet.getVisibility() == View.INVISIBLE) {
                            bottomSheet.setVisibility(View.VISIBLE);
                        }
                        if (disable.getVisibility() == View.INVISIBLE) {
//                            fab.setClickable(false);
                            fab2.setClickable(false);
//                            behind_fab2.setVisibility(View.VISIBLE);
                            pathAnimator2.start();
//                            fab.startAnimation(fab_fade);
                            fab2.startAnimation(fab_fade);
                            SlideUpBottomSheet();
                        }
                        disable.setVisibility(View.VISIBLE);
                        bottom_dlg_open = true;
                    }
                }
            });
        }
        disable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (during_animation == false) {
                    if (bottom_dlg_open) {
                        SlideDownBottomSheet();
                        reset_bottom_dlg();
                        disable.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        setTitle("");
        TextView pageTitle = (TextView) findViewById(R.id.toolbar_text);
        String title = getIntent().getExtras().getString(TAG_TITLE);
        pageTitle.setText(title);
    }
    public void open_fullscr_dlg(){

        final View included_dlg = findViewById(R.id.included_fullscrn_dlg);
        View app_bar_layout = findViewById(R.id.appbar_layout);
        View second_toolbar = findViewById(R.id.second_toolbar);
        View stable_backgrnd = findViewById(R.id.stable_background);
        stable_backgrnd.setClickable(true);

        AlphaAnimation fade_out = new AlphaAnimation(1, 0);
        fade_out.setDuration(300);
        fade_out.setFillAfter(true);
        AlphaAnimation fade_in = new AlphaAnimation(0, 1);
        fade_in.setDuration(300);
        fade_in.setFillAfter(true);
        Animation animation_bottom_sheet = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_up_dlg);
        int bottom_sheet_anim_duration = 400;
        animation_bottom_sheet.setDuration(bottom_sheet_anim_duration);
        animation_bottom_sheet.setInterpolator(new DecelerateInterpolator(1.5f));
        animation_bottom_sheet.setFillAfter(true);
        ObjectAnimator move_up_dlg = ObjectAnimator.ofFloat(included_dlg, "y", metrics.heightPixels, 0);
        move_up_dlg.setDuration(400);
        ObjectAnimator fade_backgrnd = ObjectAnimator.ofFloat(stable_backgrnd, "alpha", 0, 1);
        fade_backgrnd.setDuration(400);
        app_bar_layout.startAnimation(fade_out);
        second_toolbar.startAnimation(fade_in);
        View toolbar_back_btn = findViewById(R.id.toolbar_back_btn);
        toolbar_back_btn.setClickable(true);
        included_dlg.startAnimation(animation_bottom_sheet);
        animation_bottom_sheet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                included_dlg.setY(0);
            }
            @Override
            public void onAnimationEnd(Animation animation) {during_animation = false;set_up_fullscr_dlg();}
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
//        move_up_dlg.start();
        stable_backgrnd.setY(24 * metrics.density);
        fade_backgrnd.start();
        is_fullscr_dlg_open = true;
    }
    public void set_up_fullscr_dlg(){
        View progress1_V = findViewById(R.id.progress1);
        progress1_V.setVisibility(View.VISIBLE);
        CircleProgressBar progress1 = new CircleProgressBar(progress1_V, 87, getApplicationContext(), this);
        progress1.start();
    }
    public void reset_positions(){
        View background_view = findViewById(R.id.stable_background);
//        View fab = findViewById(R.id.fab);
        View fab2 = findViewById(R.id.fab2);
        View progress1_V = findViewById(R.id.progress1);

//        fab.setClickable(true);
        fab2.setClickable(true);

        progress1_V.setVisibility(View.INVISIBLE);
        background_view.setAlpha(0);
    }
    public void close_fullscr_dlg(){
        final int out_duration = 300;
        View background_view = findViewById(R.id.stable_background);
        background_view.setClickable(false);
        final View included_dlg = findViewById(R.id.included_fullscrn_dlg);
        View app_bar_layout = findViewById(R.id.appbar_layout);
        View second_toolbar = findViewById(R.id.second_toolbar);
//        View fab = findViewById(R.id.fab);
        View fab2 = findViewById(R.id.fab2);
        ObjectAnimator move_out_dlg = ObjectAnimator.ofFloat(included_dlg, "y", 0, metrics.heightPixels);
        move_out_dlg.setDuration(out_duration);
        ObjectAnimator move_out_background = ObjectAnimator.ofFloat(background_view, "y", background_view.getY(), metrics.heightPixels);
        Animation move_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_down_outofpage);
        move_down.setFillAfter(true);
        move_down.setDuration(out_duration);
        move_out_background.setDuration(out_duration);
        AlphaAnimation fade_out = new AlphaAnimation(1, 0);
        fade_out.setDuration(out_duration);
        fade_out.setFillAfter(true);
        AlphaAnimation fade_in = new AlphaAnimation(0, 1);
        fade_in.setDuration(out_duration);
        fade_in.setFillAfter(true);

//        View behind_fab = findViewById(R.id.behind_fab);
//        behind_fab.setX(fab.getX());
//        behind_fab.setY(fab.getY());
//        behind_fab.setScaleX(1);
//        behind_fab.setScaleY(1);
//        behind_fab.setVisibility(View.INVISIBLE);

        app_bar_layout.startAnimation(fade_in);
        second_toolbar.startAnimation(fade_out);
        View toolbar_back_btn = findViewById(R.id.toolbar_back_btn);
        toolbar_back_btn.setClickable(false);
//        fab.startAnimation(fade_in);
        fab2.startAnimation(fade_in);
        move_out_background.start();
//        move_out_dlg.start();
        included_dlg.startAnimation(move_down);
        move_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                included_dlg.setY(metrics.heightPixels);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
//        background_view.startAnimation(move_down);
        is_fullscr_dlg_open = false;
        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                reset_positions();
            }
        }, out_duration);
    }
    public void toolbar_back_clicked(View view){
        close_fullscr_dlg();
    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (during_animation == false) {
//            if ((drawer.isDrawerOpen(GravityCompat.START))) {
//                drawer.closeDrawer(GravityCompat.START);
//            } else
                if ((is_fullscr_dlg_open)) {
                close_fullscr_dlg();
            } else if ((bottom_dlg_open)) {
                SlideDownBottomSheet();
                reset_bottom_dlg();
                disable.setVisibility(View.INVISIBLE);
            } else {
            /*    Intent teachingActivity = new Intent(this, TeachingActivity.class);
                teachingActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(teachingActivity);*/
                super.onBackPressed();
            }
        }
    }

//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
//
////        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
////        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    @Override
    public void callBack(String result) {

    }

    public static class bottomsheet_padapter extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public bottomsheet_padapter() {
        }

        public static bottomsheet_padapter newInstance(int sectionNumber) {
            bottomsheet_padapter fragment = new bottomsheet_padapter();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final int n = getArguments().getInt(ARG_SECTION_NUMBER) - 1;
            View rootView = inflater.inflate(R.layout.bottomsheet_inner_fragment_main, container, false);
            final int number_in_column = 5;
            LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.bottom_sheet_inner_linear);
            LinearLayout column = new LinearLayout(getActivity().getApplicationContext());
            column.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            LinearLayout.LayoutParams imgparams = new LinearLayout.LayoutParams(0, (int) (90 * metrics.density));
            imgparams.weight = 1;

            LinearLayout.LayoutParams paddingParams = new LinearLayout.LayoutParams(0, (int) (90 * metrics.density));
            paddingParams.weight = 0.25f;

            int high_border = thumb_images.get(n).size();
            for(int i = 0; i < high_border; i++){
                ImageView img = new ImageView(getActivity().getApplicationContext());
                View padding = new View(getActivity().getApplicationContext());
                padding.setLayoutParams(paddingParams);
                final int finalI = i;
                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int curr_no = 0;
                        for(int j = 0; j < n; j++){
                            curr_no += thumb_images.get(j).size();
                        }
                        curr_no += finalI;
                        tasviri_viewpager.setCurrentItem(curr_no, true);
                    }
                });
                img.setImageBitmap(thumb_images.get(n).get(i).getBitmap());
                img.setLayoutParams(imgparams);
                column.addView(img);
                if(!(i % number_in_column == number_in_column - 1))
                    column.addView(padding);
                if(i % number_in_column == number_in_column - 1 || i == high_border - 1){
                    linearLayout.addView(column);
                    column = new LinearLayout(getActivity().getApplicationContext());
                }
            }
            return rootView;
        }
    }
    public static class tasviri_pageradapter extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public tasviri_pageradapter() {
        }

        public static tasviri_pageradapter newInstance(int sectionNumber) {
            tasviri_pageradapter fragment = new tasviri_pageradapter();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tasviri, container, false);
            ImageView img = (ImageView) rootView.findViewById(R.id.tasviri_img);
            TextView txt = (TextView) rootView.findViewById(R.id.tasviri_txt);
            Image image = imageCaller.callImageById(images.get(getArguments().getInt(ARG_SECTION_NUMBER)), "REAL_SIZE");
            img.setImageBitmap(image.getBitmap());
            txt.setText(image.getName());
            return rootView;
        }
    }

    public class bottom_sheet_pager_adapter extends FragmentPagerAdapter {

        public bottom_sheet_pager_adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return bottomsheet_padapter.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return max_nave;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return chapterHandler.getTagName(tag + Integer.toString(position));
        }
    }
    public class tasviri_adapter extends FragmentPagerAdapter {

        public tasviri_adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return tasviri_pageradapter.newInstance(position);
        }

        @Override
        public int getCount() {
            return images.size();
        }
    }

    public void SlideUpBottomSheet(){
        ObjectAnimator slide_up = new ObjectAnimator().ofFloat(bottomSheet,"translationY",0,-(bottomSheet.getHeight()));
        slide_up.setDuration(300);
        slide_up.setStartDelay(fab_fade_duration + arc_duration2);
        slide_up.setInterpolator(new DecelerateInterpolator());
        slide_up.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}
            @Override
            public void onAnimationEnd(Animator animation) {
                during_animation = false;
            }
            @Override
            public void onAnimationCancel(Animator animation) {}
            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        slide_up.start();
    }

    public void SlideDownBottomSheet(){
        ObjectAnimator slide_down = new ObjectAnimator().ofFloat(bottomSheet,"translationY",-bottomSheet.getHeight(),0);
        slide_down.setDuration(200);
        slide_down.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                during_animation = true;
            }
            @Override
            public void onAnimationEnd(Animator animation) {
                during_animation = false;
            }
            @Override
            public void onAnimationCancel(Animator animation) {}
            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        slide_down.start();
    }
    void reset_bottom_dlg(){

        AlphaAnimation fade_in = new AlphaAnimation(0, 1);
        fade_in.setDuration(200);
        fade_in.setFillAfter(true);

//        View fab = findViewById(R.id.fab);
        View fab2 = findViewById(R.id.fab2);
        View behind_fab2 = findViewById(R.id.behind_fab2);
        RelativeLayout bottom_rel = (RelativeLayout) findViewById(R.id.bottom_relLayout);
        CoordinatorLayout.LayoutParams newparams = (CoordinatorLayout.LayoutParams) bottom_rel.getLayoutParams();
        newparams.height = (int) (160 * metrics.density);
        newparams.anchorGravity = Gravity.RIGHT | Gravity.TOP;
        bottom_rel.setLayoutParams(newparams);
//        fab.setClickable(true);
        fab2.setClickable(true);

//        fab.startAnimation(fade_in);
        fab2.startAnimation(fade_in);

        behind_fab2.setScaleX(1);
        behind_fab2.setScaleY(1);
        behind_fab2.setX(fab2.getX());
        behind_fab2.setY(fab2.getY() - (metrics.heightPixels - metrics.density * 160));
        behind_fab2.setVisibility(View.INVISIBLE);
        bottom_dlg_open = false;
        rel_layout_height = 160;
    }
}
