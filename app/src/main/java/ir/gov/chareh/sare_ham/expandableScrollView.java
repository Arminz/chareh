package ir.gov.chareh.sare_ham;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by asus on 9/14/2016.
 */
public class expandableScrollView {
    private int itemscount;
    private ArrayList<Custom_checkbox> checkboxes = new ArrayList<>();
    public ArrayList<Boolean> getIsread(){
        ArrayList<Boolean> result = new ArrayList<>(itemscount);
        for(int i = 0; i < itemscount; i++){
            result.add(checkboxes.get(i).getEnabled());
        }
        return result;
    }
    public void addItems(LinearLayout mainLinearLayout, final ArrayList<View> items_child, final Context context, ArrayList<Boolean> is_read, ArrayList<String> nave_names,boolean isSobheEmtehan){
        itemscount = items_child.size();
        int headerHeight = 48;
        final int[] heights = new int[items_child.size()] ;
        final boolean[] firstTime = new boolean[items_child.size()] ;
        for(int i = 0 ; i < items_child.size() ; i++ ){
            firstTime[i] = true ;
        }
        ArrayList<LinearLayout> item  = new ArrayList<>() ;
        final DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);

        for (int i = 0 ; i < items_child.size() ; i++ ){
            final LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (headerHeight*metrics.density));
            final LinearLayout L = new LinearLayout(context);
            L.setOrientation(LinearLayout.VERTICAL);
            final int finalI = i;
            L.setLayoutParams(p);
            RelativeLayout LHeader = new RelativeLayout(context);
            LHeader.setLayoutParams(p1);
            LHeader.setBackgroundColor(ContextCompat.getColor(context, R.color.nave_background));
            TextView nave_name = new TextView(context);
            nave_name.setText(nave_names.get(i));
            RelativeLayout.LayoutParams nave_name_params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
            nave_name_params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            nave_name_params.addRule(RelativeLayout.CENTER_VERTICAL);
            nave_name_params.rightMargin = (int) (24 * metrics.density);
            nave_name.setLayoutParams(nave_name_params);
            final ImageView arrow = new ImageView(context);
            arrow.setRotation(180);
            arrow.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_keyboard_arrow_24dp));
            RelativeLayout.LayoutParams arrow_params = new RelativeLayout.LayoutParams((int) (24* metrics.density), (int) (24* metrics.density));
            arrow_params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            arrow_params.addRule(RelativeLayout.CENTER_VERTICAL);
            arrow_params.leftMargin = (int) (16 * metrics.density);
            arrow.setLayoutParams(arrow_params);
            LHeader.addView(nave_name);
            LHeader.addView(arrow);
            L.addView(LHeader);
            L.addView(items_child.get(i));
            if (!isSobheEmtehan) {
                final View chckbox_view = ((Activity) context).getLayoutInflater().inflate(R.layout.customzied_checkbox_layout, null);
                final LinearLayout.LayoutParams chckbox_params = new LinearLayout.LayoutParams((int) (120 * metrics.density), (int) (36 * metrics.density));
                chckbox_params.leftMargin = (int) (24 * metrics.density);
                chckbox_params.topMargin = (int) (20 * metrics.density);
                chckbox_params.bottomMargin = (int) (40 * metrics.density);
                chckbox_view.setLayoutParams(chckbox_params);
                L.addView(chckbox_view);
                Custom_checkbox custom_checkbox = new Custom_checkbox(chckbox_view, is_read.get(i), context);
                checkboxes.add(custom_checkbox);
            }
            LHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (firstTime[finalI] == true){
                        heights[finalI] = L.getHeight() ;
                        firstTime[finalI] = false;
                    }
                    expand(L, (int) (items_child.get(finalI).getHeight() + 96 * metrics.density),p1.height,heights[finalI], arrow, context);
                }
            });
            item.add(L) ;
            mainLinearLayout.addView(L);
        }
    }

    private void expand(final View v, final int deltaY, final int headerHeight, final int expandY, View arrow, Context context){
        final int initialHeight = v.getMeasuredHeight();
        if(v.getLayoutParams().height == headerHeight){
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    v.getLayoutParams().height = initialHeight + ((int) (((expandY-headerHeight) * interpolatedTime)));
                    v.requestLayout();
                    super.applyTransformation(interpolatedTime, t);
                }
            };
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {v.setClickable(false);}
                @Override
                public void onAnimationEnd(Animation animation) {v.setClickable(true);}
                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
            animation.setDuration(220);
            v.startAnimation(animation);
            ObjectAnimator rotateAnimation = ObjectAnimator.ofFloat(arrow, "Rotation", 0, 180);
            rotateAnimation.setDuration(220);
            rotateAnimation.start();
        }else{
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    v.getLayoutParams().height = initialHeight - ((int) ((deltaY * interpolatedTime)));
                    v.requestLayout();
                    super.applyTransformation(interpolatedTime, t);
                }
            };
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {v.setClickable(false);}
                @Override
                public void onAnimationEnd(Animation animation) {v.setClickable(true);}
                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
            animation.setDuration(220);
            v.startAnimation(animation);
            ObjectAnimator rotateAnimation = ObjectAnimator.ofFloat(arrow, "Rotation", 180, 0);
            rotateAnimation.setDuration(220);
            rotateAnimation.start();
        }
    }
}
