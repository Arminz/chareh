package ir.gov.chareh.sare_ham;

import android.database.Cursor;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class Record {
    private static String KEY_ID = "ID";
    private static String KEY_EXAM_ID = "ExamID";
    private static String KEY_USER_ANSWERS = "UserAnswers";
    private static String KEY_DATE = "Date";
    private static String KEY_RESULT = "Result";
    private static String KEY_WEAKNESS_TAGS = "WeaknessTags";

    private int id;
    private int examId;
    private ArrayList<Integer> userAnswers = new ArrayList<>();
    private String date;
    ArrayList <Integer> result = new ArrayList<>();
    Map <String,Integer>wrongSonTags = new HashMap<String,Integer>();
    public Record(Exam exam, ArrayList<Integer> userAnswers){
        //record has not any id yet.
        examId = exam.getId();
        ArrayList<Question> questions = exam.getQuestions();
        for (int i = 0 ; i < 30 ; i ++)
        {
            Question thisQuestion = questions.get(i);
            if (userAnswers.get(i) == -1) {
                updateWrongSonTags(thisQuestion);
                result.add(0);
            }
            else if (thisQuestion.getAnswerOP() == userAnswers.get(i)) {
                result.add(1);
            }
            else {
                updateWrongSonTags(thisQuestion);
                result.add(2);
            }
        }
        this.userAnswers = userAnswers;
        this.examId = exam.getId();


        SimpleDateFormat sdf = new SimpleDateFormat("HH,mm,ss");
        String convertedClock = sdf.format(new Date());

        Calendar cal = Calendar.getInstance();
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH) + 1;
        int mDay = cal.get(Calendar.DAY_OF_MONTH);

        Roozh jCal = new Roozh();
        jCal.GregorianToPersian(mYear, mMonth, mDay);
        String convertedDate = jCal.toString(); //year,month,day
        this.date=convertedDate + "," + convertedClock;


    }
    public Record (Cursor cursor)
    {
        int iId = cursor.getColumnIndex(KEY_ID);
        int iExamId = cursor.getColumnIndex(KEY_EXAM_ID);
        int iDate = cursor.getColumnIndex(KEY_DATE);
        int iUserAnswers = cursor.getColumnIndex(KEY_USER_ANSWERS);
        int iResult = cursor.getColumnIndex(KEY_RESULT);
        int iWrongSonTags = cursor.getColumnIndex(KEY_WEAKNESS_TAGS);

        id = cursor.getInt(iId);
        examId = cursor.getInt(iExamId);

        String sUserAnswers = cursor.getString(iUserAnswers);
        String thisAnswer="";
        for (int i = 0 ; i < sUserAnswers.length() ; i ++)
        {
            if (sUserAnswers.charAt(i) == '|')
            {
                userAnswers.add(Integer.parseInt(thisAnswer));
                thisAnswer="";
            }
            else
                thisAnswer += sUserAnswers.charAt(i);
        }

        String sResult = cursor.getString(iResult);
        String thisResult = "";
        for (int i = 0 ; i < sResult.length() ; i ++)
        {
            if (sResult.charAt(i) == '|') {
                result.add(Integer.parseInt(thisResult));
                thisResult = "";
            }
            else
                thisResult += sResult.charAt(i);
        }
        this.date=cursor.getString(iDate);
        String sWeaknessTags = cursor.getString(iWrongSonTags);
        String[] keyValuePairs = sWeaknessTags.substring(1,sWeaknessTags.length()-1).split(",");
        for (String pair : keyValuePairs)
        {
            String[] thisPair = pair.split("=");
            wrongSonTags.put(thisPair[0].trim(),Integer.parseInt(thisPair[1].trim()));
        }

    }
    int getExamId(){ return examId;}
    ArrayList <Integer> getUserAnswers(){return userAnswers;}
    String getDate(){return date;}
    ArrayList <Integer> getResult(){return result;}
    boolean isAccepted(){
        int tc = 0;
        for (int i = 0 ; i < 30 ; i ++)
            if (result.get(i) == 1)
                tc ++;
        if (tc >= 27)
            return true;
        return false;

    }
    int getNumOfCorrects()
    {
        int counter = 0;
        for (int i = 0 ; i < 30 ; i ++)
            if (result.get(i) == 1)
                counter ++;
        return counter;
    }

    int getNumOfIncorrects()
    {
        int counter = 0;
        for (int i = 0 ; i < 30 ; i ++)
            if (result.get(i) == 2)
                counter ++;
        return counter;
    }
    int getNumOfEmpty()
    {
        int counter = 0;
        for (int i = 0 ; i < 30 ; i ++)
            if (result.get(i) == 0)
                counter ++;
        return counter;
    }
    int getId()
    {
        return id;
    }
    private void updateWrongSonTags(Question thisQuestion) {
        if (thisQuestion.getChapterTag() == null) {
            Log.d("TODO Warning", "null chapter tag for question : " + thisQuestion.getId());
            return;
        }
        String thisQuestionSonChapterTag = null;
        if (thisQuestion.getChapterTag().length() < 2)
            thisQuestionSonChapterTag = thisQuestion.getChapterTag().substring(0, 1);
        else
            thisQuestionSonChapterTag = thisQuestion.getChapterTag().substring(0,2);

        if (wrongSonTags.containsKey(thisQuestionSonChapterTag))
            wrongSonTags.put(thisQuestionSonChapterTag,wrongSonTags.get(thisQuestionSonChapterTag)+1);
        else
            wrongSonTags.put(thisQuestionSonChapterTag,1);
    }
    public String getStringOfWrongSonTags()
    {
        return wrongSonTags.toString();
    }
    public Map<String,Integer> getWrongSonTags()
    {
        return wrongSonTags;

    }
}
