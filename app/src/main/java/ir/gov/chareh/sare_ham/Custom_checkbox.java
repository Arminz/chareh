package ir.gov.chareh.sare_ham;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

public class Custom_checkbox {
    private View main_View;
    private boolean enabled;
    private boolean during_animation = false;
    private int color;
    private Context context;
    private final int anim_duration = 200;
    public boolean getEnabled(){return this.enabled;}
    public Custom_checkbox(View view, boolean enabled, Context context){
        this.main_View = view;
        this.enabled = enabled;
        this.context = context;
        color = Color.argb(255, 200, 100, 50);//main_color

        if(enabled){
            main_View.setBackground(context.getResources().getDrawable(R.drawable.check_box_background_enabled));
        }
        else{
            main_View.setBackground(context.getResources().getDrawable(R.drawable.check_box_background));
        }
        wait_for_click();
    }
    private void wait_for_click(){
        final View ripple = main_View.findViewById(R.id.ripple);
        main_View.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN && !during_animation){
                    during_animation = true;
                    if(enabled){
                        ripple.setBackground(context.getResources().getDrawable(R.drawable.check_box_ripple));
                    }
                    else{
                        ripple.setBackground(context.getResources().getDrawable(R.drawable.check_box_ripple_enabled));
                    }
                    enabled = !enabled;
                    ripple.setAlpha(1);
                    ripple.setX(motionEvent.getX() - ripple.getHeight() / 2);
                    ripple.setY(motionEvent.getY() - ripple.getWidth() / 2);
                    float centerX = ripple.getX() + ripple.getWidth() / 2;
                    float centerY = ripple.getY() + ripple.getHeight() / 2;
                    float[] distanceTocorner = new float[2];
                    if (centerX < view.getWidth() / 2) {
                        distanceTocorner[1] = view.getWidth() - centerX;
                    } else {
                        distanceTocorner[1] = centerX;
                    }
                    if (centerY < view.getHeight() / 2) {
                        distanceTocorner[0] = view.getHeight() - centerY;
                    } else {
                        distanceTocorner[0] = centerY;
                    }
                    float dist_to_corner = (float) Math.sqrt(Math.pow(distanceTocorner[0], 2) + Math.pow(distanceTocorner[1], 2));
                    float scale_threshold = (float) (Math.sqrt(4) * dist_to_corner / ripple.getHeight());
                    ObjectAnimator fade_in = ObjectAnimator.ofFloat(ripple, "alpha", 0.5f, 1);
                    fade_in.setDuration(anim_duration);
                    ScaleAnimation scaleAnimation = new ScaleAnimation(0, scale_threshold, 0, scale_threshold, centerX, centerY);
                    scaleAnimation.setDuration(anim_duration);
                    scaleAnimation.setFillAfter(true);
                    scaleAnimation.setInterpolator(new AccelerateInterpolator());
                    scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {}
                        @Override
                        public void onAnimationEnd(Animation animation) {
                            if(enabled){
                                main_View.setBackground(context.getResources().getDrawable(R.drawable.check_box_background_enabled));
                            }
                            else{
                                main_View.setBackground(context.getResources().getDrawable(R.drawable.check_box_background));
                            }
                            ripple.setAlpha(0);
                            during_animation = false;
                        }
                        @Override
                        public void onAnimationRepeat(Animation animation) {}
                    });
                    ripple.startAnimation(scaleAnimation);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ObjectAnimator bounce_z;
                        if(enabled){
                            bounce_z = ObjectAnimator.ofFloat(main_View, "TranslationZ", main_View.getZ(), 2);
                        }
                        else{
                            bounce_z = ObjectAnimator.ofFloat(main_View, "TranslationZ", main_View.getZ(), 8);
                        }
                        bounce_z.setDuration(anim_duration);
                        bounce_z.start();
                    }
                    fade_in.start();
                }
                return false;
            }
        });
    }
}
