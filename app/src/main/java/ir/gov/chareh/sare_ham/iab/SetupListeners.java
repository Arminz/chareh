package ir.gov.chareh.sare_ham.iab;

/**
 * Created by armin on 3/21/17.
 */
public interface SetupListeners {
    void onSuccessful();
    void onError(String error);
}
