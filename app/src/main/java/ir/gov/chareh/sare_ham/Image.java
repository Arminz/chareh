package ir.gov.chareh.sare_ham;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Base64;

/**
 * Created by user on 7/29/16.
 */
public class    Image {
    private static String KEY_ID = "ID";
    private static String KEY_IMAGE_DATA = "ImageData";
    private static String KEY_NAME = "Name";
    private static String KEY_CHAPTER_TAG = "ChapterTag";

    private int id;
    private Bitmap bitmap;
    private String name;
    private String chapterTag;

    public Image(Cursor cursor,int width , int height)//-1 -1 for free size
    {
        int iId = cursor.getColumnIndex(KEY_ID);
        int iImageData = cursor.getColumnIndex(KEY_IMAGE_DATA);
        int iName = cursor.getColumnIndex(KEY_NAME);
        int iChapterTag = cursor.getColumnIndex(KEY_CHAPTER_TAG);

        id = cursor.getInt(iId);

        byte[] encodeByte = Base64.decode(cursor.getBlob(iImageData), Base64.DEFAULT);//first param is blob format (cursor.getString(iImageData).getBytes() == cursor.getBlob(2))
        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        if (width != -1 && height != -1)
            bitmap = Bitmap.createScaledBitmap(bitmap,width,height,false);
        encodeByte=null;
        name = cursor.getString(iName);
        chapterTag = cursor.getString(iChapterTag);

    }
    public int getId(){return id;}
    public Bitmap getBitmap(){return bitmap;}
    public String getName(){return name;}
    public String getChapterTag(){return chapterTag;}
}
