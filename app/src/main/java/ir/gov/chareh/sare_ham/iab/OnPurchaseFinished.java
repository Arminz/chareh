package ir.gov.chareh.sare_ham.iab;

/**
 * Created by armin on 3/21/17.
 */
public interface OnPurchaseFinished {
    void onErrorOtherProgressIsRunning();
    void onErrorNotFullySetup();
    void onError(String error);
    void onSuccessful();
}
