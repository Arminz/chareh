package ir.gov.chareh.sare_ham;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;


public class First_page extends AppCompatActivity
//        implements NavigationView.OnNavigationItemSelectedListener
{
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MyDatabase";
    private static final String QUESTION_TABLE_NAME = "Questions";
    private static final String IMAGES_TABLE_NAME = "Images";
    private static final String EXAMS_TABLE_NAME = "Exams";
    private static final String RECORDS_TABLE_NAME = "Records";
    private static final String CHAPTERS_TABLE_NAME = "Chapters";
    private static final String PARAGRAPHS_TABLE_NAME = "Paragraphs";


    final long animation_in_duration = 300;
    final long animation_out_duration = 200;
    final long circle_anim_time = 320;
    final long rect_anim_time = 250;
    boolean is_drawer_open = false;
    boolean opening_new_page = false;

    private AssetDatabaseHelper dbHelper;

//    private InAppBillingHandler inAppBillingHandler;

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
        TextView tv = (TextView) findViewById(R.id.toolbar_text);
        tv.setText(title);
    }

    private void fade_out_shadow(View app_bar_layout){
        if (app_bar_layout == null)
            return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ObjectAnimator z_changer = null;
            z_changer = ObjectAnimator.ofFloat(app_bar_layout, app_bar_layout.Z, app_bar_layout.getZ(), 0);
            z_changer.setDuration(2 * animation_out_duration);
            z_changer.start();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.mainpage_toolbar);
            setSupportActionBar(toolbar);
        TextView title = (TextView) findViewById(R.id.toolbar_text);
        if (title != null)
            title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANSans(FaNum).ttf"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupStatusBarColor();
        }
//        IabInitial();

//        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.mainPage_drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();

        setTitle(getResources().getString(R.string.app_name));

        dbHelper = new AssetDatabaseHelper(
                getBaseContext(),DATABASE_NAME , DATABASE_VERSION);
        try {
            dbHelper.importIfNotExist();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getBaseContext(),"Database import err:"+e.getMessage(), Toast.LENGTH_LONG).show();

        }

        final Window window = getWindow();

        final View main_back_view = findViewById(R.id.main_back_view);

        final AlphaAnimation texts_in = new AlphaAnimation(0, 1);
        texts_in.setDuration(animation_in_duration);
        final View main_texts = findViewById(R.id.mainpage_cats);
        final AlphaAnimation toolbar_in = new AlphaAnimation(0, 1);
        toolbar_in.setDuration(animation_in_duration);
        toolbar_in.setFillAfter(true);

//        main_back_view.setBackgroundColor(getResources().getColor(R.color.mainPage_background));
        AlphaAnimation fade_in = new AlphaAnimation(0, 1);
        fade_in.setDuration(300);
        fade_in.setFillAfter(true);
//        background.startAnimation(fade_in);

//        final ImageView background = (ImageView) findViewById(R.id.background_mainpage);
//        background.post(new Runnable() {
//            @Override
//            public void run() {
//                DisplayMetrics metrics = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(metrics);
//                float initialRatio = (float) background.getMeasuredWidth() / background.getMeasuredHeight();
//                float newWidth = metrics.heightPixels * initialRatio;
//                ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) background.getLayoutParams();
//                params.width = (int) newWidth;
////                params.width = metrics.widthPixels + 10000;
//                params.height = metrics.heightPixels;
//                background.setLayoutParams(params);
////                Log.d("MTH", background.getMeasuredHeight() + " " + background.getMeasuredWidth());
//                Log.d("MTH", "newWidth " + newWidth + " newHeight " + metrics.heightPixels + " ratio " + initialRatio);
//            }
//        });

        new threaded_prep().execute("");
    }

    private void setupStatusBarColor() {
        Window window = this.getWindow();

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.my_statusbar_color));
        }

    }

    private class threaded_prep extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            prepare_activity();
            return null;
        }
    }
    public void prepare_activity(){
        final RelativeLayout relLayout = (RelativeLayout) findViewById(R.id.middle_layout);
        final RelativeLayout relLayout2 = (RelativeLayout) findViewById(R.id.middle_layout2);
        final RelativeLayout relLayout3 = (RelativeLayout) findViewById(R.id.middle_layout3);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.mainpage_toolbar);
        final View page_title = findViewById(R.id.toolbar_text);
        final View app_bar_layout = findViewById(R.id.app_bar_layout);

        relLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!opening_new_page) {
                    opening_new_page = true;
                    final View mycircle = findViewById(R.id.myimg1);
                    final ImageView mybtn = (ImageView) findViewById(R.id.mybtn);
                    mycircle.setVisibility(View.VISIBLE);
//                    mybtn.bringToFront();
                    final ImageView imageView = (ImageView) findViewById(R.id.main_1_iv);
                    mycircle.setX(imageView.getX() + imageView.getWidth() / 2 - mycircle.getWidth() / 2);
                    mycircle.setY(imageView.getY() + imageView.getHeight() / 2 - mycircle.getHeight() / 2);
                    float centerX = mycircle.getX() + mycircle.getWidth() / 2;
                    float centerY = mycircle.getY() + mycircle.getHeight() / 2;
                    float[] distanceTocorner = new float[2];
                    if (centerX < relLayout.getWidth() / 2) {
                        distanceTocorner[1] = relLayout.getWidth() - centerX;
                    } else {
                        distanceTocorner[1] = centerX;
                    }
                    if (centerY < relLayout.getHeight() / 2) {
                        distanceTocorner[0] = relLayout.getHeight() - centerY;
                    } else {
                        distanceTocorner[0] = centerY;
                    }
                    float dist_to_corner = (float) Math.sqrt(Math.pow(distanceTocorner[0], 2) + Math.pow(distanceTocorner[1], 2));
                    float scale_threshold = (float) (Math.sqrt(4) * dist_to_corner / mycircle.getHeight());
                    DisplayMetrics metrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    float screen_height = ((View) view.getParent()).getHeight();
                    Animation toolbar_out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.appbar_out);
                    toolbar_out.setDuration(animation_out_duration);
                    long toolbar_out_offset = (long) ((circle_anim_time + 0.18 * rect_anim_time) * 1.1);
                    toolbar_out.setStartOffset(toolbar_out_offset);
                    toolbar_out.setFillAfter(true);
                    ScaleAnimation scaleAnimation = new ScaleAnimation(0, scale_threshold, 0, scale_threshold, centerX, centerY);
                    ScaleAnimation scaleAnimation1 = new ScaleAnimation(1f, 1f,
                            1f, 2 * screen_height / relLayout.getHeight(),
                            Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 2/(screen_height / relLayout.getHeight()));
                    scaleAnimation1.setDuration(rect_anim_time);
                    scaleAnimation1.setStartOffset(circle_anim_time);
                    scaleAnimation1.setFillAfter(true);
                    scaleAnimation1.setInterpolator(new FastOutSlowInInterpolator());
                    scaleAnimation.setDuration(circle_anim_time);
                    scaleAnimation.setFillAfter(true);
                    scaleAnimation.setInterpolator(new AccelerateInterpolator());
                    mycircle.startAnimation(scaleAnimation);
                    relLayout.startAnimation(scaleAnimation1);
                    toolbar.startAnimation(toolbar_out);
                    if (page_title != null)
                        page_title.startAnimation(toolbar_out);
                    fade_out_shadow(app_bar_layout);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mybtn.setBackgroundColor(getResources().getColor(R.color.mainPage_first_item_color));
                            imageView.setBackgroundColor(getResources().getColor(R.color.mainPage_first_item_color));
                            relLayout.bringToFront();
                        }
                    }, circle_anim_time);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        final Handler handler2 = new Handler();
                        handler2.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    mybtn.setBackgroundColor(getResources().getColor(R.color.mainPage_first_item_color));
                                    imageView.setBackgroundColor(getResources().getColor(R.color.mainPage_first_item_color));
                                }
                                Intent next_page = new Intent(First_page.this, TeachingActivity.class);
                                startActivity(next_page.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                            }
                        }, animation_out_duration + toolbar_out_offset);
                    }
                }
            }
        });

        relLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!opening_new_page) {
                    opening_new_page = true;
                    final View mycircle = findViewById(R.id.myimg2);
                    final ImageView mybtn = (ImageView) findViewById(R.id.mybtn2);
                    mycircle.setVisibility(View.VISIBLE);
//                    mybtn.bringToFront();
                    final ImageView imageView = (ImageView) findViewById(R.id.main_2_iv);
                    mycircle.setX(imageView.getX() + imageView.getWidth() / 2 - mycircle.getWidth() / 2);
                    mycircle.setY(imageView.getY() + imageView.getHeight() / 2 - mycircle.getHeight() / 2);
                    float centerX = mycircle.getX() + mycircle.getWidth() / 2;
                    float centerY = mycircle.getY() + mycircle.getHeight() / 2;
                    float[] distanceTocorner = new float[2];
                    if (centerX < relLayout2.getWidth() / 2) {
                        distanceTocorner[1] = relLayout2.getWidth() - centerX;
                    } else {
                        distanceTocorner[1] = centerX;
                    }
                    if (centerY < relLayout2.getHeight() / 2) {
                        distanceTocorner[0] = relLayout2.getHeight() - centerY;
                    } else {
                        distanceTocorner[0] = centerY;
                    }
                    float dist_to_corner = (float) Math.sqrt(Math.pow(distanceTocorner[0], 2) + Math.pow(distanceTocorner[1], 2));
                    float scale_threshold = (float) (Math.sqrt(4) * dist_to_corner / mycircle.getHeight());
                    DisplayMetrics metrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    float screen_height = ((View) view.getParent()).getHeight();
                    Animation toolbar_out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.appbar_out);
                    toolbar_out.setDuration(animation_out_duration);
                    long toolbar_out_offset = (long) ((circle_anim_time + 0.27 * rect_anim_time) * 1.1);
                    toolbar_out.setStartOffset(toolbar_out_offset);
                    toolbar_out.setFillAfter(true);
                    ScaleAnimation scaleAnimation = new ScaleAnimation(0, scale_threshold, 0, scale_threshold, centerX, centerY);
                    ScaleAnimation scaleAnimation1 = new ScaleAnimation(1f, 1f,
                            1f, 2 * screen_height / relLayout2.getHeight(),
                            Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 2/(screen_height / relLayout2.getHeight()));
                    scaleAnimation1.setDuration(rect_anim_time);
                    scaleAnimation1.setStartOffset(circle_anim_time);
                    scaleAnimation1.setFillAfter(true);
                    scaleAnimation1.setInterpolator(new FastOutSlowInInterpolator());
                    scaleAnimation.setDuration(circle_anim_time);
                    scaleAnimation.setFillAfter(true);
                    scaleAnimation.setInterpolator(new AccelerateInterpolator());
                    mycircle.startAnimation(scaleAnimation);
                    relLayout2.startAnimation(scaleAnimation1);
                    toolbar.startAnimation(toolbar_out);
                    if (page_title != null)
                        page_title.startAnimation(toolbar_out);
                    fade_out_shadow(app_bar_layout);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mybtn.setBackgroundColor(getResources().getColor(R.color.mainPage_second_item_color));
                            imageView.setBackgroundColor(getResources().getColor(R.color.mainPage_second_item_color));
                            relLayout2.bringToFront();
                        }
                    }, circle_anim_time);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        final Handler handler2 = new Handler();
                        handler2.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    mybtn.setBackgroundColor(getResources().getColor(R.color.mainPage_second_item_color));
                                    imageView.setBackgroundColor(getResources().getColor(R.color.mainPage_second_item_color));
                                }
                                Intent next_page = new Intent(First_page.this, exam_page.class);
                                startActivity(next_page.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                            }
                        }, animation_out_duration + toolbar_out_offset);
                    }
                }
            }
        });

        relLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    SharedPreferences sharedpreferences = getSharedPreferences("comment", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    int count = sharedpreferences.getInt("count", 0);
                    if (count == 3 || count == 10 || count == 30 || count == 60) {
                        String PACKAGE_NAME = "ir.gov.chareh.sare_ham";
                        Intent intent = new Intent(Intent.ACTION_EDIT);
                        intent.setData(Uri.parse("bazaar://details?id=" + PACKAGE_NAME));
                        intent.setPackage("com.farsitel.bazaar");
                        startActivity(intent);
                        count ++;
                        editor.putInt("count", count);
                        editor.apply();
                        return;
                    }
                    else {
                        count++;
                        editor.putInt("count", count);
                        editor.apply();
                    }
                }catch (Exception ignored)
                {

                }

                if(!opening_new_page) {
                    opening_new_page = true;
                    final View mycircle = findViewById(R.id.myimg3);
                    final ImageView mybtn = (ImageView) findViewById(R.id.mybtn3);
                    mycircle.setVisibility(View.VISIBLE);
//                    mybtn.bringToFront();
                    final ImageView imageView = (ImageView) findViewById(R.id.main_3_iv);
                    mycircle.setX(imageView.getX() + imageView.getWidth() / 2 - mycircle.getWidth() / 2);
                    mycircle.setY(imageView.getY() + imageView.getHeight() / 2 - mycircle.getHeight() / 2);
                    float centerX = mycircle.getX() + mycircle.getWidth() / 2;
                    float centerY = mycircle.getY() + mycircle.getHeight() / 2;
                    float[] distanceTocorner = new float[2];
                    if (centerX < relLayout3.getWidth() / 2) {
                        distanceTocorner[1] = relLayout3.getWidth() - centerX;
                    } else {
                        distanceTocorner[1] = centerX;
                    }
                    if (centerY < relLayout3.getHeight() / 2) {
                        distanceTocorner[0] = relLayout3.getHeight() - centerY;
                    } else {
                        distanceTocorner[0] = centerY;
                    }
                    float dist_to_corner = (float) Math.sqrt(Math.pow(distanceTocorner[0], 2) + Math.pow(distanceTocorner[1], 2));
                    float scale_threshold = (float) (Math.sqrt(4) * dist_to_corner / mycircle.getHeight());
                    DisplayMetrics metrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    float screen_height = ((View) view.getParent()).getHeight();
                    Animation toolbar_out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.appbar_out);
                    toolbar_out.setDuration(animation_out_duration);
                    long toolbar_out_offset = (long) ((circle_anim_time + 0.30 * rect_anim_time) * 1.1);
                    toolbar_out.setStartOffset(toolbar_out_offset);
                    toolbar_out.setFillAfter(true);
                    ScaleAnimation scaleAnimation = new ScaleAnimation(0, scale_threshold, 0, scale_threshold, centerX, centerY);
                    ScaleAnimation scaleAnimation1 = new ScaleAnimation(1f, 1f,
                            1f, 2 * screen_height / relLayout2.getHeight(),
                            Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 2/(screen_height / relLayout2.getHeight()));
                    scaleAnimation1.setDuration(rect_anim_time);
                    scaleAnimation1.setStartOffset(circle_anim_time);
                    scaleAnimation1.setFillAfter(true);
                    scaleAnimation1.setInterpolator(new FastOutSlowInInterpolator());
                    scaleAnimation.setDuration(circle_anim_time);
                    scaleAnimation.setFillAfter(true);
                    scaleAnimation.setInterpolator(new AccelerateInterpolator());
                    mycircle.startAnimation(scaleAnimation);
                    relLayout3.startAnimation(scaleAnimation1);
                    toolbar.startAnimation(toolbar_out);
                    if (page_title != null) {
                        page_title.startAnimation(toolbar_out);
                    }
                    fade_out_shadow(app_bar_layout);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mybtn.setBackgroundColor(getResources().getColor(R.color.mainPage_third_item_color));
                            imageView.setBackgroundColor(getResources().getColor(R.color.mainPage_third_item_color));
                            relLayout3.bringToFront();
                        }
                    }, circle_anim_time);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        final Handler handler2 = new Handler();
                        handler2.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    getWindow().setStatusBarColor(getResources().getColor(R.color.mainPage_notification_color3));
                                    Intent next_page = new Intent(First_page.this, teachingOne.class);
                                    next_page.putExtra("tag","50");
                                    startActivity(next_page.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                                }
                            }
                        }, animation_out_duration + toolbar_out_offset);
                    }
                }
            }
        });
    }
//    public void IabInitial()
//    {
//        inAppBillingHandler = new InAppBillingHandler(this, new SetupListeners() {
//            @Override
//            public void onSuccessful() {
//                Log.d("tag", "1");
//            }
//
//            @Override
//            public void onError(String error) {
//                //TODO : make better ux for purchase button and refresh
//                Log.d("tag", "2 : " + error);
//
//            }
//        }, new GetInventoryListeners() {
//            @Override
//            public void onSuccessful(Boolean isPremium) {
//                Log.d("tag", "5 : " + isPremium);
//            }
//
//            @Override
//            public void onError(String error) {
//                Log.d("tag", "6 : " + error);
//
//            }
//
//            @Override
//            public void onFinish() {
//                //Todo: enable purchase button
//            }
//        } );
//    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
////        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.mainPage_drawer_layout);
////        if (drawer.isDrawerOpen(GravityCompat.START)) {
////            drawer.closeDrawer(GravityCompat.START);
//            /////
//
////            inAppBillingHandler.purchase(new OnPurchaseFinished() {
////                @Override
////                public void onErrorOtherProgressIsRunning() {
////                    Toast.makeText(getApplicationContext(), "عملیات دیگری در حال انجام است. لطفا بعد از چند ثانیه دوباره اقدام کنید.",Toast.LENGTH_LONG).show();
////
////                    //Todo : make better ux
////
////
////                }
////
////                @Override
////                public void onErrorNotFullySetup() {
////                    AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
////                    builder.setTitle(R.string.in_app_billing_purchase_error_title);
////                    builder.setMessage(R.string.in_app_billing_purchase_error_message);
////                    builder.setPositiveButton(R.string.okButton_alertDialog, new DialogInterface.OnClickListener() {
////                        @Override
////                        public void onClick(DialogInterface dialogInterface, int i) {
////                            startActivity(new Intent(First_page.this,First_page.class));
////                            finish();
////                        }
////                    });
////                    builder.setNegativeButton(R.string.cancelButton_alertDialog,null);
////                    builder.create().show();
////
////                }
////
////
////                @Override
////                public void onError(String error) {
////                    Log.d("tag","3 : " + error);
////                }
////
////                @Override
////                public void onSuccessful() {
////                    Log.d("tag","4");
////                }
////            });
////
////            ////
////        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
////        }
    }


//    @SuppressWarnings("StatementWithEmptyBody")

//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.nav_camera) {
////            PurchaseHandler.commitPurchase(getApplicationContext());
////            Toast.makeText(getApplicationContext(),"خریداری شد",Toast.LENGTH_LONG).show();
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
//
////        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.mainPage_drawer_layout);
////        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    @Override
    protected void onDestroy() {
        dbHelper.close();
////        inAppBillingHandler.dispose();
////        inAppBillingHandler = null;
        super.onDestroy();
    }
}
