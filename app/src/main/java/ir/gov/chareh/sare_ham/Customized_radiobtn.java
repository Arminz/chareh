package ir.gov.chareh.sare_ham;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;

/**
 * Created by root on 9/25/16.
 * edited by armin
 */
public class Customized_radiobtn {
    private float[] coeffs = new float[4];
    private final int duration = 250;
    private View options;
    private int chosen_option = 0;
    public Customized_radiobtn(View options){
        coeffs[0] = -1;
        this.options = options;
        initialize_chosen_option();
    }
    public int get_chosen_option(){
        return chosen_option - 1;

    }
    public Customized_radiobtn(View options, int chosen_option){
        coeffs[0] = -1;
        this.options = options;
        if(chosen_option < 0 || chosen_option > 4)
            this.chosen_option = 0;
        else
            this.chosen_option = chosen_option;
        initialize_chosen_option();
    }
    public void set_option(int option){
        if(option < 0 || option > 4)
            return;
        if(chosen_option == option || option == 0){
            unexpand_circle(option);
            chosen_option = 0;
        }
        else if(chosen_option != 0){
            unexpand_circle(chosen_option);
            expand_circle(option);
            chosen_option = option;
        }
        else {
            expand_circle(option);
            chosen_option = option;
        }
    }
    private void initialize_chosen_option(){
        if(chosen_option == 0)
            return;
        expand_circle(chosen_option);
    }
    private View find_circle_by_no(int no){
        View circle = null;
        if(no == 1)
            circle = options.findViewById(R.id.option1).findViewById(R.id.colored_circle1);
        else if(no == 2)
            circle = options.findViewById(R.id.option2).findViewById(R.id.colored_circle2);
        else if(no == 3)
            circle = options.findViewById(R.id.option3).findViewById(R.id.colored_circle3);
        else if(no == 4)
            circle = options.findViewById(R.id.option4).findViewById(R.id.colored_circle4);
        return circle;
    }
    private View find_background_by_no(int no){
        View circle = null;
        if(no == 1)
            circle = options.findViewById(R.id.option1).findViewById(R.id.circle_background1);
        else if(no == 2)
            circle = options.findViewById(R.id.option2).findViewById(R.id.circle_background2);
        else if(no == 3)
            circle = options.findViewById(R.id.option3).findViewById(R.id.circle_background3);
        else if(no == 4)
            circle = options.findViewById(R.id.option4).findViewById(R.id.circle_background4);
        return circle;
    }
    private View find_background_white_by_no(int no){
        View circle = null;
        if(no == 1)
            circle = options.findViewById(R.id.option1).findViewById(R.id.circle_background_white1);
        else if(no == 2)
            circle = options.findViewById(R.id.option2).findViewById(R.id.circle_background_white2);
        else if(no == 3)
            circle = options.findViewById(R.id.option3).findViewById(R.id.circle_background_white3);
        else if(no == 4)
            circle = options.findViewById(R.id.option4).findViewById(R.id.circle_background_white4);
        return circle;
    }
    private View find_background_grey_by_no(int no){
        View circle = null;
        if(no == 1)
            circle = options.findViewById(R.id.option1).findViewById(R.id.circle_background_grey1);
        else if(no == 2)
            circle = options.findViewById(R.id.option2).findViewById(R.id.circle_background_grey2);
        else if(no == 3)
            circle = options.findViewById(R.id.option3).findViewById(R.id.circle_background_grey3);
        else if(no == 4)
            circle = options.findViewById(R.id.option4).findViewById(R.id.circle_background_grey4);
        return circle;
    }
    private void expand_circle(int no){
        if(coeffs[0] == -1){
            for(int i = 0; i < 4; i++){
                View bigOne = null;
                View circle;
                if(i == 0){
                    bigOne = options.findViewById(R.id.option1);
                    circle = bigOne.findViewById(R.id.colored_circle1);
                }
                else if(i == 1){
                    bigOne = options.findViewById(R.id.option2);
                    circle = bigOne.findViewById(R.id.colored_circle2);
                }
                else if(i == 2){
                    bigOne = options.findViewById(R.id.option3);
                    circle = bigOne.findViewById(R.id.colored_circle3);
                }
                else if(i == 3){
                    bigOne = options.findViewById(R.id.option4);
                    circle = bigOne.findViewById(R.id.colored_circle4);
                }
                else{
                    throw new RuntimeException("number problem");
                }
                float a = bigOne.getHeight() / 2;
                float b = bigOne.getWidth() - circle.getX() - circle.getWidth() / 2;
                float c = (float) Math.sqrt(a * a + b * b);
                coeffs[i] = c / circle.getWidth() * 2;
            }
        }
        View circle = find_circle_by_no(no);
        View background_white = find_background_white_by_no(no);
        View background_grey = find_background_grey_by_no(no);
        AlphaAnimation fade_out = new AlphaAnimation(1, 0);
        fade_out.setFillAfter(true);
        fade_out.setDuration(duration / 4);
        float centerX = circle.getWidth() / 2;
        float centerY = circle.getHeight() / 2;
        ScaleAnimation scaleAnimation = new ScaleAnimation(1, coeffs[no - 1], 1, coeffs[no - 1], centerX, centerY);
        scaleAnimation.setDuration(duration);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new DecelerateInterpolator());
        circle.startAnimation(scaleAnimation);
        background_white.startAnimation(fade_out);
        background_grey.startAnimation(fade_out);
    }
    private void unexpand_circle(int no){
        if(coeffs[0] == -1){
            for(int i = 0; i < 4; i++){
                View bigOne = null;
                View circle;
                if(i == 0){
                    bigOne = options.findViewById(R.id.option1);
                    circle = bigOne.findViewById(R.id.colored_circle1);
                }
                else if(i == 1){
                    bigOne = options.findViewById(R.id.option2);
                    circle = bigOne.findViewById(R.id.colored_circle2);
                }
                else if(i == 2){
                    bigOne = options.findViewById(R.id.option3);
                    circle = bigOne.findViewById(R.id.colored_circle3);
                }
                else if(i == 3){
                    bigOne = options.findViewById(R.id.option4);
                    circle = bigOne.findViewById(R.id.colored_circle4);
                }
                else{
                    throw new IllegalArgumentException("chert number");
                }
                float a = bigOne.getHeight() / 2;
                float b = bigOne.getWidth() - circle.getX() - circle.getWidth() / 2;
                float c = (float) Math.sqrt(a * a + b * b);
                coeffs[i] = c / circle.getWidth() * 2;
            }
        }
        View background_white = find_background_white_by_no(no);
        View background_grey = find_background_grey_by_no(no);
        AlphaAnimation fade_in = new AlphaAnimation(0, 1);
        fade_in.setFillAfter(true);
        fade_in.setDuration(duration / 4);
        fade_in.setStartOffset(duration);
        View circle = find_circle_by_no(no);
        float centerX = circle.getWidth() / 2;
        float centerY = circle.getHeight() / 2;
        ScaleAnimation scaleAnimation = new ScaleAnimation(coeffs[no - 1], 1, coeffs[no - 1], 1, centerX, centerY);
        scaleAnimation.setDuration(duration);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new DecelerateInterpolator());
        circle.startAnimation(scaleAnimation);
        background_white.startAnimation(fade_in);
        background_grey.startAnimation(fade_in);
    }
}
