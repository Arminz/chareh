package ir.gov.chareh.sare_ham;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


/**
 * Created by user on 10/5/16.
 */

public class PurchaseHandler {

    static private String KEY_SHARED = "MYPREFS";
    static private String TAG = "BillingTag";
    static public  void commitPurchase(Context context)
    {
        changePurchaseSituation(context , true);
    }

    static public void removePurchase(Context context)
    {
        changePurchaseSituation(context , false);
    }

    static private void changePurchaseSituation(Context context,Boolean b)
    {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_SHARED,b);
        editor.apply();
        Log.d(TAG,"Purchase situation is going to change to " + b);
    }
    public static boolean getPurchaseSituation(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(KEY_SHARED,false);

    }
}
