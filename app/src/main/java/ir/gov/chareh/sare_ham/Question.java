package ir.gov.chareh.sare_ham;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by user on 7/22/16.
 */
public class Question {

    //SQLite Keys
    private static String KEY_ID = "ID";
    private static String KEY_QUESTION_TEXT = "QuestionText";
    private static String KEY_OPS = "OP's";
    private static String KEY_ANSWER_OP = "AnswerOP";
    private static String KEY_LESSON_ID = "LessonID";

    private static String KEY_IMAGE_ID = "ImageID";
    private static String KEY_CHAPTER_TAG = "ChapterTag";
    private static String KEY_IMAGE_USE = "ImageUse";
    private static String KEY_DIFFICULTY_TAG = "DifficultyTag";
    private static String KEY_SELECTED_OP = "SelectedOP";



    int id;
    String questionText;
    ArrayList<String> OPs = new ArrayList<>();
    int answerOP;
    int imageID;
    String chapterTag;
    String imageUse;
    String difficultyTag;
    Image headImage;
    int selectedOP;
    ArrayList<Image> opImages;

    public Question(Cursor cursor)
    {
        /*Column Indexes*/
        int iID = cursor.getColumnIndex(KEY_ID);
        int iQuestionText = cursor.getColumnIndex(KEY_QUESTION_TEXT);
        int iOPs = cursor.getColumnIndex(KEY_OPS);
        int iAnswerOP = cursor.getColumnIndex(KEY_ANSWER_OP);
        int iImageID = cursor.getColumnIndex(KEY_IMAGE_ID);
        int iChapterTag = cursor.getColumnIndex(KEY_CHAPTER_TAG);
        int iImageUse = cursor.getColumnIndex(KEY_IMAGE_USE);
        int iDifficultyTag = cursor.getColumnIndex(KEY_DIFFICULTY_TAG);
        int iSelectedOP = cursor.getColumnIndex(KEY_SELECTED_OP);
        id = cursor.getInt(iID);
        questionText = cursor.getString(iQuestionText);

        String _ops = cursor.getString(iOPs);
        String this_op = "";
        for (int i = 0 ; i < _ops.length() ; i++)
        {
            if (_ops.charAt(i) != '|')
                this_op += _ops.charAt(i);
            else {
                OPs.add(this_op);
                this_op = "";
            }
        }

        answerOP = cursor.getInt(iAnswerOP) - 1;

        imageID = cursor.getInt(iImageID);
        chapterTag = cursor.getString(iChapterTag);
        imageUse = cursor.getString(iImageUse);
        difficultyTag = cursor.getString(iDifficultyTag);
        if (HasOpImage()) {
            opImages = new ArrayList<>();
            for (int i = 0; i < 4; i++)
                opImages.add(null);
        }

        selectedOP=cursor.getInt(iSelectedOP);




    }
    public void setHeadImage(Image headImage){
        this.headImage = headImage;
    }
    public void setIthOpImage(Image opIthImage , int ith){
        opImages.set(ith,opIthImage);
    }
    public int getId(){return id;}
    public String getQuestionText(){return questionText;}
    public String getIthOP(int ith){return OPs.get(ith);}
    public int getAnswerOP(){return answerOP;}
    public int getSelectedOP(){return selectedOP;}


    public int getImageID(){return imageID;}
    public String getChapterTag(){return chapterTag;}
    public boolean HasHeadImage(){return imageUse.charAt(0)=='h';}
    public boolean HasOpImage(){return imageUse.charAt(1)=='o';}
    public String getDifficultyTag(){return difficultyTag;}
    public Image getHeadImage(){return headImage;}
    public Image getIthOpImage(int ith){return opImages.get(ith);}
}
