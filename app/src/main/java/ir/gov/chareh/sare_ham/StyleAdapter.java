package ir.gov.chareh.sare_ham;

import android.content.Context;
import android.graphics.Typeface;
import android.renderscript.Type;
import android.view.View;
import android.widget.TextView;

/**
 * Created by root on 9/14/16.
 */
public class StyleAdapter {
    private String tag;
    private Context context;
    public static final String TAG_default = "default";
    public static final String TAG_bullet = "bullet";
    int id;
    public StyleAdapter(Context context){
        this.context = context;
        tag = new String();
    }
    private void setId(){
        if(tag == TAG_bullet)
            id = R.layout.bullet_text;
        else
            id = R.layout.default_text;
    }
    public View get_content(String text, String tag ){
        this.tag = tag;
        setId();
        View mother = View.inflate(context, id,null);
        Typeface noto = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansArabic-Regular.ttf");
        TextView tv = (TextView) mother.findViewById(R.id.text_content);
        tv.setText(text);
        tv.setTypeface(noto);
        return mother;
    }
}
