package ir.gov.chareh.sare_ham;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class Result {
    public static void create_result(RelativeLayout base, int disLeftRight, ArrayList<Integer> answer , Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        RelativeLayout.LayoutParams flparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        flparams.width = ((int) ((metrics.widthPixels - disLeftRight * (metrics.density))/30))*30;
        RelativeLayout[] item = new RelativeLayout[30] ;
        ArrayList<Integer> mark = answer ;

        LinearLayout result_container = new LinearLayout(context);
        result_container.setLayoutParams(flparams);
        result_container.setGravity(Gravity.CENTER);

        for (int i = 0 ; i < 30 ; i++ ) {
            item[i] = new RelativeLayout(context);
            item[i].setMinimumHeight((int) (metrics.density * 16));
            item[i].setMinimumWidth((int) ((metrics.widthPixels - disLeftRight * (metrics.density))/30));
            if (mark.get(i) == 1) {
                item[i].setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            }
            else if (mark.get(i) == 2) {
                item[i].setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            }
            result_container.addView(item[i]);
        }
        base.addView(result_container);

        move_all_green(mark,item,disLeftRight,context);
        move_all_red(mark,item,disLeftRight,context);
    }

    public static void move_all_green(ArrayList<Integer> mark,RelativeLayout[] item,int disLeftRight, Context context){
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int TrueCounter = 0 ;
        for(int i = 0 ; i < mark.size() ; i++){
            if(mark.get(i) == 1){
                ObjectAnimator translateX = new ObjectAnimator().ofFloat(item[i],"translationX",0
                        ,-((i - TrueCounter) * (metrics.widthPixels - disLeftRight * (metrics.density))/30));
                ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(item[i], View.ALPHA, 1f,0.5f);
                ObjectAnimator alphaAnimation2 = ObjectAnimator.ofFloat(item[i], View.ALPHA, 0.5f,1f);
                alphaAnimation2.setDuration(100);
                alphaAnimation2.setStartDelay(3200);
                alphaAnimation.setDuration(200);
                alphaAnimation.setStartDelay(3000);
                translateX.setDuration(100) ;
                translateX.setStartDelay(3000);
                alphaAnimation.start();
                alphaAnimation2.start();
                translateX.start();
                TrueCounter++ ;
            }
        }
    }

    public static void move_all_red(ArrayList<Integer> mark,RelativeLayout[] item,int disLeftRight, Context context){
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int FalseCounter = 0 ;
        for(int i = 29 ; i >= 0 ; i--){
            if(mark.get(i) == 2){
                ObjectAnimator translateX = new ObjectAnimator().ofFloat(item[i],"translationX",0
                        ,((29 - i - FalseCounter) * (metrics.widthPixels - disLeftRight * (metrics.density))/30));
                translateX.setDuration(600) ;
                translateX.setStartDelay(3000);
                ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(item[i], View.ALPHA, 1f,0.5f);
                ObjectAnimator alphaAnimation2 = ObjectAnimator.ofFloat(item[i], View.ALPHA, 0.5f,1f);
                alphaAnimation2.setDuration(100);
                alphaAnimation2.setStartDelay(3200);
                alphaAnimation.setDuration(200);
                alphaAnimation.setStartDelay(3000);
                translateX.setDuration(100) ;
                translateX.setStartDelay(3000);
                alphaAnimation.start();
                alphaAnimation2.start();
                translateX.start();
                FalseCounter++ ;
            }
        }
    }
}
