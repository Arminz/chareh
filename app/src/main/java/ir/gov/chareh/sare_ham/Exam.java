package ir.gov.chareh.sare_ham;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by user on 8/8/16.
 */
public class Exam {

    //SQLite Keys

    private static String KEY_ID = "ID";
    private static String KEY_QUESTIONS_ID = "QuestionsID";
    private static String KEY_RECORDS_ID = "RecordsID";

    private int id;
    private ArrayList <Integer> questionsID = new ArrayList<>();
    private ArrayList <Integer> recordsID = new ArrayList<>();
    private ArrayList <Question> questions = new ArrayList<>();
    public Exam(Cursor cursor)
    {
        if (cursor == null) {
            id = -1;
            return ;
        }

        int iID = cursor.getColumnIndex(KEY_ID);
        int iQuestionsID = cursor.getColumnIndex(KEY_QUESTIONS_ID);
        int iRecordsID = cursor.getColumnIndex(KEY_RECORDS_ID);

        id = cursor.getInt(iID);
        String _questionsID = cursor.getString(iQuestionsID);

        String this_questionID = "";
        for (int i = 0 ; i < _questionsID.length(); i ++)
        {
            if (_questionsID.charAt(i) == '|')
            {
                questionsID.add(Integer.parseInt(this_questionID));
                this_questionID ="";
            }
            else
                this_questionID += _questionsID.charAt(i);
        }

        String _recordsID = cursor.getString(iRecordsID);
        String this_recordID = "";
        if (_recordsID != null)
            for (int i = 0 ; i < _recordsID.length(); i ++)
            {
                if (_recordsID.charAt(i) == '|'){
                    recordsID.add(Integer.getInteger(this_recordID));
                    this_recordID = "";
                }
                else
                    this_recordID += _recordsID.charAt(i);
            }

    }
    public void setIthQuestion(Question ithQuestion , int ith){
        questions.add(ith,ithQuestion);
    }
    public void addQuestions(ArrayList<Question> questions){
        for (int i = 0 ; i < questions.size();i++)
            this.questions.add(questions.get(i));
    }
    public Question getIthQuestion(int ith){return questions.get(ith);}
    public int getId(){return id;}
    public int getIthQuestionID(int ith){
        return questionsID.get(ith);
    }
    public int getIthRecordID(int ith){
        return recordsID.get(ith);
    }
    public ArrayList <Question> getQuestions(){return questions;}

}
