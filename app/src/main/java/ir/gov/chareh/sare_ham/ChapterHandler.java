package ir.gov.chareh.sare_ham;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by user on 9/8/16.
 */
public class ChapterHandler {

    private String KEY_ID = "ID";
    private String KEY_TEXT = "Text";
    private String KEY_TAG = "Tag";
    private String KEY_IS_READ = "IsRead";
    private String KEY_CHAPTER_TAG = "ChapterTag";
    private String KEY_HTML_TAG = "HtmlTag";
    private String KEY_NAME = "Name";


    private static String DATABASE_NAME;
    private static int DATABASE_VERSION;
    private static String CHAPTERS_TABLE_NAME;
    private static String PARAGRAPHS_TABLE_NAME;
    private static String IMAGES_TABLE_NAME;



    private Context context;
    private AssetDatabaseHelper dbHelper;
    private SQLiteDatabase rdb;
    private SQLiteDatabase wdb;

    private static int IMAGES_WIDTH = 100 , IMAGES_HEIGHT = 100;
    public ChapterHandler(Context baseContext, AssetDatabaseHelper _dbHelper , String DATABASE_NAME, int DATABASE_VERSION,
                          String CHAPTERS_TABLE_NAME , String PARAGRAPHS_TABLE_NAME,String IMAGES_TABLE_NAME){
        this.DATABASE_NAME = DATABASE_NAME;
        this.DATABASE_VERSION = DATABASE_VERSION;
        this.PARAGRAPHS_TABLE_NAME = PARAGRAPHS_TABLE_NAME;
        this.CHAPTERS_TABLE_NAME = CHAPTERS_TABLE_NAME;
        this.IMAGES_TABLE_NAME = IMAGES_TABLE_NAME;
        this.context = baseContext;

        dbHelper = _dbHelper;
//        dbHelper = new AssetDatabaseHelper(context,DATABASE_NAME,DATABASE_VERSION);

        rdb = dbHelper.getReadableDatabase();
        wdb = dbHelper.getWritableDatabase();
    }
    ArrayList <Paragraph> CallParagraphsByChapterTag(String tag){

        String SQLiteCommand = "SELECT * FROM " + PARAGRAPHS_TABLE_NAME + " WHERE " + KEY_CHAPTER_TAG + " = \"" + tag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = rdb.rawQuery(SQLiteCommand,null);

        ArrayList <Paragraph> paragraphs = new ArrayList<>();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
        {
            Paragraph thisParagraph = new Paragraph(cursor);
            paragraphs.add(thisParagraph);
        }

        return paragraphs;
    }

    ArrayList <Image> CallImagesByChapterTag(String tag)
    {
        String SQLiteCommand = "SELECT * FROM " + IMAGES_TABLE_NAME + " WHERE " + KEY_CHAPTER_TAG + " = \"" + tag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = rdb.rawQuery(SQLiteCommand,null);

        ArrayList <Image> images = new ArrayList<>();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
        {
            Image thisImage = new Image(cursor,IMAGES_WIDTH,IMAGES_HEIGHT);
            images.add(thisImage);
        }

        return images;

    }
//    public Paragraph callParagraphById(int id)
//    {
//        String SQLiteCommand = "SELECT * FROM " + PARAGRAPHS_TABLE_NAME + " WHERE " + KEY_ID + " = " + id;
//        Log.d("SQLite Command" , SQLiteCommand);
//        Cursor cursor = rdb.rawQuery(SQLiteCommand,null);
//        cursor.moveToFirst();
//        Paragraph thisParagraph = new Paragraph(cursor);
//        return thisParagraph;
//
//    }

    public void setChapterIsRead(String tag , int isRead)
    {
        String SQLiteCommand = "UPDATE  " + CHAPTERS_TABLE_NAME +
                " SET " + KEY_IS_READ + " = " + isRead +
                " WHERE " + KEY_TAG + " = \"" + tag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        wdb.execSQL(SQLiteCommand);
    }
    public int getChapterIsRead(String tag)
    {
        String SQLiteCommand = "SELECT * FROM " + CHAPTERS_TABLE_NAME + " WHERE " + KEY_TAG + " = \"" + tag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = rdb.rawQuery(SQLiteCommand,null);
        cursor.moveToFirst();
        int iIsRead = cursor.getColumnIndex(KEY_IS_READ);
        int isRead = cursor.getInt(iIsRead);
        return isRead;

    }
    public int getCountOfChaptersByIsRead(int isRead){
        String SQLiteCommand = "SELECT COUNT(*) FROM " + CHAPTERS_TABLE_NAME + " WHERE " + KEY_IS_READ + " = " + isRead;
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = wdb.rawQuery(SQLiteCommand , null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        return count;
    }
    int getCountOfTag(String tag)
    {
        String SQLiteCommand = "SELECT COUNT(*) FROM " + CHAPTERS_TABLE_NAME + " WHERE " + KEY_TAG + " GLOB \"" + tag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = wdb.rawQuery(SQLiteCommand , null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        return count;

    }
    int getCountOfParagraphs(String chapterTag)
    {
        String SQLiteCommand = "SELECT COUNT(*) FROM " + PARAGRAPHS_TABLE_NAME + " WHERE " + KEY_CHAPTER_TAG + " GLOB \"" + chapterTag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = wdb.rawQuery(SQLiteCommand,null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        return count;
    }
    String getTagName(String chapterTag)
    {
        String SQLiteCommand = "SELECT * FROM " + CHAPTERS_TABLE_NAME + " WHERE " + KEY_TAG + " = \"" + chapterTag + "\"";
        Log.d("SQLiteCommand" , SQLiteCommand);
        Cursor cursor = wdb.rawQuery(SQLiteCommand, null);
        cursor.moveToFirst();
        int iName = cursor.getColumnIndex(KEY_NAME);
        String name = cursor.getString(iName);
        return name;

    }
}
