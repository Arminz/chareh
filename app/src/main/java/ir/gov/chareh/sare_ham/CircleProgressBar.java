package ir.gov.chareh.sare_ham;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by root on 8/22/16.
 */
public class CircleProgressBar {
    private int target_percentage;
    private final int max_anim_duration = 500;
    private final int initial_rotation = -180;
    private final TextView percentage_text;
    private final ProgressBar progressBar;
    private Context context;
    final View golden_back;
    private View mother;
    private reached100 mReached100;
    private boolean forTasviri;
    public CircleProgressBar(View mother_view, int percentage, Context context, reached100 listener){
        this(mother_view, percentage, context, listener, false);
    }

    public CircleProgressBar(View mother_view, int percentage, Context context, reached100 listener, Boolean forTasviri){
        this.context = context;
        this.mReached100 = listener;
        this.forTasviri = forTasviri;
        Typeface noto_arabic = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans(FaNum).ttf");
        if(percentage > 100)
            this.target_percentage = 100;
        else if(percentage < 0)
            this.target_percentage = 0;
        else
            this.target_percentage = percentage;
        percentage_text = (TextView) mother_view.findViewById(R.id.percentage);
        progressBar = (ProgressBar) mother_view.findViewById(R.id.progressBar);
        percentage_text.setTypeface(noto_arabic);
        golden_back = mother_view.findViewById(R.id.golden_back);
        mother = mother_view.findViewById(R.id.inside_progress_mother);

        //reset things
        golden_back.setAlpha(0);
        percentage_text.setTypeface(noto_arabic);
        percentage_text.setText("");
        percentage_text.setTextColor(context.getResources().getColor(R.color.black87));
        progressBar.setProgress(0);
    }
    public void start(){
        long animation_duration;
        if(target_percentage > 50) {
            animation_duration = (long) ((target_percentage / 100.0) * max_anim_duration);
        }
        else{
            animation_duration = (long) (0.5 * max_anim_duration);
        }
        percentage_text.setText(Integer.toString(progressBar.getProgress()));
        ObjectAnimator prog_anim = ObjectAnimator.ofInt (progressBar, "progress", 0, target_percentage);
        ObjectAnimator prog_rotate = ObjectAnimator.ofFloat(progressBar, "rotation", initial_rotation, -90);
        prog_rotate.setDuration(200);
        final ValueAnimator percentage_animator = new ValueAnimator();
        percentage_animator.setInterpolator(new DecelerateInterpolator());
        percentage_animator.setObjectValues(0, target_percentage);
        percentage_animator.setDuration(animation_duration);
        percentage_animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                String number = number_to_persian(Integer.toString((int) animation.getAnimatedValue() / 2));
                if (forTasviri)
                    number = number_to_persian(Integer.toString((int) animation.getAnimatedValue()));
                percentage_text.setText(number + "%");
            }
        });
        prog_anim.setDuration(animation_duration);
        ObjectAnimator fade_out = ObjectAnimator.ofFloat(progressBar, "alpha", 0, 1);
        fade_out.setDuration((long) (500));
        prog_anim.start();
        prog_rotate.start();
        percentage_animator.start();
        fade_out.start();
        if(target_percentage == 100) {
            ObjectAnimator golden_fade = ObjectAnimator.ofFloat(golden_back, "alpha", 0, 1);
            golden_fade.setStartDelay(animation_duration);
            golden_fade.setDuration(200);
            ObjectAnimator golden_bigX = ObjectAnimator.ofFloat(mother, "ScaleX", 1, (float) 1.2);
            golden_bigX.setStartDelay(animation_duration);
            golden_bigX.setDuration(200);
            ObjectAnimator golden_bigY = ObjectAnimator.ofFloat(mother, "ScaleY", 1, (float) 1.2);
            golden_bigY.setStartDelay(animation_duration);
            golden_bigY.setDuration(200);
            ObjectAnimator golden_smallX = ObjectAnimator.ofFloat(mother, "ScaleX", (float) 1.2, 1);
            golden_smallX.setStartDelay(animation_duration + 200);
            golden_smallX.setDuration(200);
            ObjectAnimator golden_smallY = ObjectAnimator.ofFloat(mother, "ScaleY", (float) 1.2, 1);
            golden_smallY.setStartDelay(animation_duration + 200);
            golden_smallY.setDuration(200);
            golden_fade.start();
            golden_bigX.start();
            golden_bigY.start();
            golden_smallX.start();
            golden_smallY.start();
            golden_smallX.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {}
                @Override
                public void onAnimationEnd(Animator animator) {
                    mReached100.callBack("reached100");
                }
                @Override
                public void onAnimationCancel(Animator animator) {}
                @Override
                public void onAnimationRepeat(Animator animator) {}
            });
            golden_back.bringToFront();
            percentage_text.bringToFront();
            golden_bigX.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {}
                @Override
                public void onAnimationEnd(Animator animator) {
                    Typeface noto_bold = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans(FaNum)_Bold.ttf");
                    percentage_text.setTypeface(noto_bold);
                    percentage_text.setTextColor(Color.WHITE);
                }
                @Override
                public void onAnimationCancel(Animator animator) {}
                @Override
                public void onAnimationRepeat(Animator animator) {}
            });
        }
    }
    private String number_to_persian(String number){
        StringBuilder result = new StringBuilder(number.length());
        for(int i = 0; i < number.length(); i++){
            switch (number.charAt(i)){
                case '0':
                    result.append('۰');
                    break;
                case '1':
                    result.append('۱');
                    break;
                case '2':
                    result.append('۲');
                    break;
                case '3':
                    result.append('۳');
                    break;
                case '4':
                    result.append('۴');
                    break;
                case '5':
                    result.append('۵');
                    break;
                case '6':
                    result.append('۶');
                    break;
                case '7':
                    result.append('۷');
                    break;
                case '8':
                    result.append('۸');
                    break;
                case '9':
                    result.append('۹');
                    break;
                default:
                    result.append(number.charAt(i));
            }
        }
        return new String(result);
    }
}
