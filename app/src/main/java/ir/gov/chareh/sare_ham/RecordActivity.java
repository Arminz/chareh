package ir.gov.chareh.sare_ham;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;

import java.util.ArrayList;
import java.util.List;

public class RecordActivity extends AppCompatActivity
//        implements NavigationView.OnNavigationItemSelectedListener
{

    private static String EXAM_ID = "EXAM_ID";
    private String PAGE_TYPE = "PAGE_TYPE";
    private String REVIEW_MODE = "REVIEW_MODE";
    private static final String RECORD_ID = "RECORD_ID";

    private int examId ;
    AssetDatabaseHelper dbHelper;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MyDatabase";
    private static final String QUESTION_TABLE_NAME = "Questions";
    private static final String IMAGES_TABLE_NAME = "Images";
    private static final String EXAMS_TABLE_NAME = "Exams";
    private static final String RECORDS_TABLE_NAME = "Records";
    private static final String CHAPTERS_TABLE_NAME = "Chapters";
    private static final String PARAGRAPHS_TABLE_NAME = "Paragraphs";
    private static final String THIS_RECORD_ID = "THIS_RECORD_ID";
    RecordHandler recordHandler;
    private Record thisRecord;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHelper = new AssetDatabaseHelper(getApplicationContext(),DATABASE_NAME,DATABASE_VERSION);
        recordHandler = new RecordHandler(getApplicationContext(),dbHelper,DATABASE_NAME,DATABASE_VERSION,RECORDS_TABLE_NAME);
        examId = getIntent().getExtras().getInt(EXAM_ID);
        ArrayList<Record> records = new ArrayList<>();

        records = recordHandler.makeListByExamId(examId);

        int thisRecordId = getIntent().getExtras().getInt(THIS_RECORD_ID);
        if (thisRecordId != -1)
            thisRecord = recordHandler.getRecordById(thisRecordId);
        else
            thisRecord = records.get(records.size() - 1);

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        RelativeLayout r = (RelativeLayout) findViewById(R.id.sdsf);
        ArrayList<Integer> answer = thisRecord.getResult();
        Result.create_result(r,120,answer,this);

        final ChipView chipDefault = (ChipView) findViewById(R.id.chipview);
        chipDefault.setChipBackgroundColor(getResources().getColor(R.color.green));
        callWeakPoints(chipDefault);
        ArrayList<Integer> recordIds = new ArrayList<>();
        for (int i = 0 ; i < records.size() ; i ++)
            recordIds.add(records.get(i).getId());

        //تاریخچه فعلا حذفه
//        // TODO: 3/18/17  وقتی که هیچ کارنامه ای از گذشته نیست مراقب باشیم چی نشون می دیم//ت
//        ExpandableView.addItems((RelativeLayout)findViewById(R.id.r1),this,records, (ScrollView) findViewById(R.id.main_s),examId,recordIds,recordHandler);

        RelativeLayout resultColorRelativeLayout = (RelativeLayout) findViewById(R.id.result_color);
        if (!thisRecord.isAccepted()) {
            resultColorRelativeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.red));
        }
        else
            resultColorRelativeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
        TextView correctTextView = (TextView) findViewById(R.id.correct);
        TextView incorrectTextView = (TextView) findViewById(R.id.incorrect);
        TextView emptyTextView = (TextView) findViewById(R.id.empty_ans);
        correctTextView.setText(thisRecord.getNumOfCorrects() + " درست");
        incorrectTextView.setText(thisRecord.getNumOfIncorrects() + " نادرست" );
        emptyTextView.setText(thisRecord.getNumOfEmpty() + " نزده");

        Button reviewButton = (Button) findViewById(R.id.button_goto_ans);
        if (examId == -1)
            reviewButton.setEnabled(false);
        reviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToReviewActivity(thisRecord.getId());
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle(R.string.title_alertDialog_removeAllRecords);
        builder.setMessage(R.string.message_alertDialog_removeAllRecords);
        builder.setPositiveButton(R.string.okButton_alertDialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                recordHandler.deleteRecordsByExamId(examId);
                onBackPressed();
            }
        });
        builder.setNegativeButton(R.string.cancelButton_alertDialog,null);



        final AlertDialog dialog = builder.create();

//        Button deleteAll = (Button) findViewById(R.id.deleteAll);
//        assert deleteAll != null;
//        deleteAll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.show();
//            }
//        });

        final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
//        builder1.setTitle(R.string.title_alertDialog_removeThisRecord);
        builder1.setMessage(R.string.message_alertDialog_removeThisRecord);
        builder1.setNegativeButton(R.string.cancelButton_alertDialog,null);
        final ArrayList<Record> finalRecords = records;
        builder1.setPositiveButton(R.string.okButton_alertDialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                recordHandler.deleteRecordById(finalRecords.get(finalRecords.size() - 1).getId());
                startActivity(new Intent(RecordActivity.this,exam_page.class));


            }
        });
//        Button deleteThis = (Button) findViewById(R.id.deleteThis);
//        assert deleteThis != null;
//        deleteThis.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                builder1.create().show();
//
//            }
//        });
//

    }

    public void callWeakPoints(ChipView chipDefault) {
        List<Chip> chipList = new ArrayList<>();
        ArrayList<String> weaknessSonTags = RecordHandler.getWeaknessSonTags(dbHelper,thisRecord,getApplicationContext());
        ChapterHandler ch = new ChapterHandler(getBaseContext(),dbHelper,DATABASE_NAME,DATABASE_VERSION,CHAPTERS_TABLE_NAME,PARAGRAPHS_TABLE_NAME,IMAGES_TABLE_NAME);
        for (int i = 0 ; i < weaknessSonTags.size(); i ++)
            chipList.add(new Tag(ch.getTagName(weaknessSonTags.get(i))));
        chipDefault.setChipList(chipList);
    }


        @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
            Intent intent = new Intent(RecordActivity.this,exam_page.class);
            startActivity(intent);
//        }
    }




//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    public void backToReviewActivity(int recordId) {
        Intent i = new Intent(getApplicationContext(),MyExam.class);
        i.putExtra(PAGE_TYPE,REVIEW_MODE);
        i.putExtra(EXAM_ID,examId);
        i.putExtra(RECORD_ID,recordId);
        startActivity(i);
    }

}
