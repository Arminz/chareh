package ir.gov.chareh.sare_ham;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by asus on 9/14/2016.
 */
public class ExpandableView {
    private static String EXAM_ID = "EXAM_ID";
    private static String PAGE_TYPE = "PAGE_TYPE";
    private static String REVIEW_MODE = "REVIEW_MODE";
    private static final String RECORD_ID = "RECORD_ID";
    private static int numberOfRemovedItems = 0 ;

    private static final ArrayList<View> childList = new ArrayList<>();

    public static void addItems(RelativeLayout mainLinearLayout, final Context context, final ArrayList<Record> history, final ScrollView sv, final int examId, final ArrayList<Integer> recordIds, final RecordHandler recordHandler){
        childList.clear();
        numberOfRemovedItems = 0 ;
        final int headerHeight = 48 ;
        final int childHeight = 48 ;
        final DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        final LinearLayout historyView = new LinearLayout(context);
        historyView.setOrientation(LinearLayout.VERTICAL);
        final RelativeLayout Header = new RelativeLayout(context);
        final LinearLayout.LayoutParams paramsHistory =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (headerHeight*metrics.density));
        final RelativeLayout.LayoutParams paramsHeader =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (headerHeight*metrics.density));
        historyView.setLayoutParams(paramsHistory);
        Header.setLayoutParams(paramsHeader);
        historyView.addView(Header);
        historyView.setBackgroundColor(context.getResources().getColor(R.color.green));
        mainLinearLayout.addView(historyView);

        for (int i =history.size() - 2 ;i >= 0 ;i --){
            LinearLayout historyChild = new LinearLayout(context);
            childList.add(historyChild) ;
            historyChild.setOrientation(LinearLayout.HORIZONTAL);
            final LinearLayout.LayoutParams paramsHisChild =
                    new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (headerHeight*metrics.density));

            final LinearLayout.LayoutParams paramsHisChildRemoved =
                    new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);

            TextView tv = new TextView(context) ;
            tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
            String date = getDate(history.get(i).getDate(),context);
            tv.setText("تاریخ : " + date);
            historyChild.addView(tv);

            historyChild.setLayoutParams(paramsHisChild);
            historyChild.setBackgroundColor(context.getResources().getColor(R.color.red));

            if (i != history.size() - 1) {
                ImageView deleteButton = new ImageView(context);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    deleteButton.setImageDrawable(context.getDrawable(R.drawable.trash));
                }
                else{
                    deleteButton.setImageDrawable(context.getResources().getDrawable(R.drawable.trash));
                }
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                final int finalI = i;

                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.title_alertDialog_removeOneRecord);
                builder.setMessage(R.string.message_alertDialog_removeOneRecord);
                builder.setPositiveButton(R.string.okButton_alertDialog, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        recordHandler.deleteRecordById(history.get(finalI).getId());
//                        ViewGroup.LayoutParams layoutParams = historyView.getLayoutParams() ;
//                        layoutParams.height = layoutParams.height - childList.get(finalI).getLayoutParams().height ;
//                        childList.get(finalI).setLayoutParams(paramsHisChildRemoved);
//                        historyView.setLayoutParams(layoutParams);
                        removeItem(childList.get(history.size() - 2 - finalI),historyView,metrics,childHeight);
                        numberOfRemovedItems++ ;
                    }
                });
                builder.setNegativeButton(R.string.cancelButton_alertDialog,null);



                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        builder.create().show();
                    }
                });

                historyChild.addView(deleteButton,params);
            }
            else
                throw new RuntimeException("wtf??? last record???");



            final int finalI1 = i;
            historyChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, RecordActivity.class);
                    intent.putExtra("THIS_RECORD_ID",history.get(finalI1).getId());
                    intent.putExtra(EXAM_ID,examId);
                    context.startActivity(intent);

//                    Intent intent = new Intent(context,MyExam.class);
//                    intent.putExtra(PAGE_TYPE,REVIEW_MODE);
//                    intent.putExtra(EXAM_ID,examId);
//                    intent.putExtra(RECORD_ID,recordIds.get(finalI));
//                    context.startActivity(intent);
                }
            });
            historyView.addView(historyChild);

        }
//        Button deleteAll = new Button(context);
//        deleteAll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                recordHandler.deleteRecordsByExamId(examId);
//            }
//        });
//        deleteAll.setText("حذف همه");
//        historyView.addView(deleteAll);

        Header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numOfChild = history.size()-numberOfRemovedItems-1  ;
                if (numOfChild <= 0)
                    numOfChild = 0 ;
                expand(historyView,Header,metrics,numOfChild,headerHeight,childHeight,sv);
            }
        });
    }

    private static void expand(final View v, final View head, final DisplayMetrics metrics, final int childNum, final int headerHeight, final int childHeight, final ScrollView sv){
        final int initialHeight = v.getMeasuredHeight();
        if(v.getLayoutParams().height == headerHeight*metrics.density) {
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    v.getLayoutParams().height = initialHeight + ((int) (((childNum*childHeight*metrics.density) * interpolatedTime)));
                    v.requestLayout();
                    super.applyTransformation(interpolatedTime, t);
                }
            };
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    head.setClickable(false);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    head.setClickable(true);
                    sv.smoothScrollTo(0, (int) (90000*metrics.density));
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            animation.setDuration(220);
            v.startAnimation(animation);
        }else{
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    v.getLayoutParams().height = initialHeight - ((int) ((childNum*childHeight*metrics.density * interpolatedTime)));
                    v.requestLayout();
                    super.applyTransformation(interpolatedTime, t);
                }
            };
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {head.setClickable(false);}
                @Override
                public void onAnimationEnd(Animation animation) {head.setClickable(true);}
                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
            animation.setDuration(220);
            v.startAnimation(animation);
        }
    }

    private static void removeItem(final View v, final View v2, final DisplayMetrics metrics, final int childHeight) {
        final int initialHeight = v.getMeasuredHeight();
        final int initialHeight2 = v2.getMeasuredHeight();
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    v.getLayoutParams().height = initialHeight - ((int) (((childHeight * metrics.density) * interpolatedTime)));
                    v2.getLayoutParams().height = initialHeight2 - ((int) (((childHeight * metrics.density) * interpolatedTime)));
                    v.requestLayout();
                    super.applyTransformation(interpolatedTime, t);
                }
            };
            animation.setDuration(110);
            v.startAnimation(animation);
    }



    private static String getDate(String pureDate, Context context){
        String[] dates = pureDate.split(",");
        Log.d("dateLog", pureDate) ;
        for(String date:dates){
            Log.d("dateLog", date) ;
        }
        String[] months = context.getResources().getStringArray(R.array.months);


        SimpleDateFormat sdf = new SimpleDateFormat("HH,mm,ss");
        String convertedClock = sdf.format(new Date());

        Calendar cal = Calendar.getInstance();
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH) + 1;
        int mDay = cal.get(Calendar.DAY_OF_MONTH);
        Roozh jCal = new Roozh();
        jCal.GregorianToPersian(mYear, mMonth, mDay);
        String convertedDate = jCal.toString(); //year,month,day
        mYear = jCal.getYear() ;
        mMonth = jCal.getMonth() ;
        mDay = jCal.getDay() ;
        Log.d("today", mYear+","+mMonth+","+mDay) ;

        if ((mYear == Integer.parseInt(dates[0]))&&(mMonth == Integer.parseInt(dates[1]))
                && (mDay == Integer.parseInt(dates[2]))){
            return "امروز " +dates[3] + ":" + dates[4] ;
        }
        if ((mYear == Integer.parseInt(dates[0]))&&(mMonth == Integer.parseInt(dates[1]))
                && (mDay - 1 == Integer.parseInt(dates[2]))){
            return "دیروز " +dates[3] + ":" + dates[4] ;
        }

        if (mYear - Integer.parseInt(dates[0]) >= 2)
            return mYear - Integer.parseInt(dates[0]) + "سال پیش " ;
        else if (mYear - Integer.parseInt(dates[0]) == 1)
            return "پارسال " ;

        return months[Integer.parseInt(dates[1])-1] + dates[2] ;
    }
}
