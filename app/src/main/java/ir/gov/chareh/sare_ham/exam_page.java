package ir.gov.chareh.sare_ham;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class exam_page extends AppCompatActivity
//        implements NavigationView.OnNavigationItemSelectedListener
{


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MyDatabase";
    private static final String QUESTION_TABLE_NAME = "Questions";
    private static final String IMAGES_TABLE_NAME = "Images";
    private static final String EXAMS_TABLE_NAME = "Exams";
    private static final String RECORDS_TABLE_NAME = "Records";
    private static final String CHAPTERS_TABLE_NAME = "Chapters";
    private static final String PARAGRAPHS_TABLE_NAME = "Paragraphs";

    private static final String PAGE_TYPE = "PAGE_TYPE";
    private static final String EXAM_QUESTIONS = "EXAM_QUESTIONS";
    private static final String TAG_QUESTIONS = "TAG_QUESTIONS";
    private static final String RANDOM_EXAM_QUESTIONS = "RANDOM_EXAM_QUESTIONS";
    private static final String CHAPTER_TAG = "CHAPTER_TAG";
    private static final String EXAM_ID = "EXAM_ID";
    private static final int NUMBER_OF_EXAMS = 7;//13

    public static int RANDOM_EXAM_INDEX = 7;


    private SectionsPagerAdapter sectionsPagerAdapter1;
    private SectionsPagerAdapter2 sectionsPagerAdapter2;
    private ViewPager viewPager1;
    private ViewPager viewPager2;
    static TextView dad_textview;
    static ArrayList<String> bacheTag = new ArrayList<>();

    public AssetDatabaseHelper dbHelper;
    public ChapterHandler chapterHandler;





    public void setup_steppers(int n, final ViewPager viewPager, ArrayList<ImageView> arrayList, LinearLayout layout){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) (20 * metrics.density),(int) (20 * metrics.density));
        if (n != 7)
            params = new LinearLayout.LayoutParams((int) (18 * metrics.density),(int) (18 * metrics.density));
        for(int i = 0; i < n; i++){
            final int finalI = i;
            RelativeLayout relativeLayout = new RelativeLayout(getApplicationContext());
            RelativeLayout.LayoutParams relParams = new RelativeLayout.LayoutParams((int) (8 * metrics.density),(int) (8 * metrics.density));
            relativeLayout.setGravity(Gravity.CENTER);
            if(i == 0){
                RelativeLayout endsLayout = new RelativeLayout(getApplicationContext());
                endsLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewPager.setCurrentItem(finalI, true);
                    }
                });
                layout.addView(endsLayout, params);
            }
            ImageView imageView = new ImageView(getApplicationContext());
            imageView.setImageResource(R.drawable.back_circle);
            if (n == 7) {
                imageView.setScaleX(2.0f / 3);
                imageView.setScaleY(2.0f / 3);
                imageView.setAlpha(0.38f);
            }
            else {
                imageView.setScaleX(1.8f / 3);
                imageView.setScaleY(1.8f / 3);
                imageView.setAlpha(0.38f);
            }
            if(i == 0){
                imageView.setScaleX(1f);
                imageView.setScaleY(1f);
                imageView.setAlpha(1f);
            }
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPager.setCurrentItem(finalI, true);
                }
            });
            relativeLayout.addView(imageView, relParams);
            layout.addView(relativeLayout, params);
            if(i == n - 1){
                RelativeLayout endsLayout = new RelativeLayout(getApplicationContext());
                endsLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewPager.setCurrentItem(finalI, true);
                    }
                });
                layout.addView(endsLayout, params);
            }
            arrayList.add(imageView);
        }
    }
    public void set_stepper_listener(final ArrayList<ImageView> arrayList, final ViewPager viewPager, final Boolean for_lower){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                float coeff = (float) (1.0 / 3);
                float Offset = (float) (positionOffset * coeff);
                if(positionOffset != 0){
                    arrayList.get(position).setScaleX((float) (1.0 - Offset));
                    arrayList.get(position).setScaleY((float) (1.0 - Offset));
                    arrayList.get(position).setAlpha(1 - positionOffset * 0.62f);
                    arrayList.get(position + 1).setScaleX((float) (1 - coeff + Offset));
                    arrayList.get(position + 1).setScaleY((float) (1 - coeff + Offset));
                    arrayList.get(position + 1).setAlpha(0.38f + positionOffset * 0.62f);
                }
                for(int i = 0; i < arrayList.size(); i++){
                    if(i == position || i == position + 1)continue;
                    ImageView iv = arrayList.get(i);
                    if(iv.getAlpha() != 0.38f){
                        iv.setAlpha(0.38f);
                        iv.setScaleX(2.0f / 3);
                        iv.setScaleY(2.0f / 3);
                    }
                }
                if(for_lower) {
                    if(position != 15) {
                        String thisBacheTag = bacheTag.get(position);
                        String nextBacheTag = bacheTag.get(position + 1);
                        String thisDadTag = thisBacheTag.substring(0, thisBacheTag.length() - 1);
                        String nextDadTag = nextBacheTag.substring(0, nextBacheTag.length() - 1);
                        if (!thisDadTag.equals(nextDadTag)) {
                            if(positionOffset >= 0.3 || positionOffset <= 0.7)
                                dad_textview.setAlpha(5 * Math.abs(0.5f - positionOffset));
                            else
                                dad_textview.setAlpha(1);
                            if(positionOffset > 0.5)
                                dad_textview.setText(chapterHandler.getTagName(nextDadTag));
                            else
                                dad_textview.setText(chapterHandler.getTagName(thisDadTag));
                        }
                    }
                }
            }
            @Override
            public void onPageSelected(final int position) {}
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView toolbarTV = (TextView) findViewById(R.id.toolbar_text);
        if (toolbarTV != null) {
            toolbarTV.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANSans(FaNum).ttf"));
        }

        setupFonts();


        dad_textview = (TextView) findViewById(R.id.section_title);
        dad_textview.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANSans(FaNum).ttf"));
        if (bacheTag.size() == 0)
        {
            bacheTag.add("00");
            bacheTag.add("01");
            bacheTag.add("02");
            bacheTag.add("03");
            bacheTag.add("10");
            bacheTag.add("11");
            bacheTag.add("12");
            bacheTag.add("20");
            bacheTag.add("21");
            bacheTag.add("22");
            bacheTag.add("23");
            bacheTag.add("24");
            bacheTag.add("25");
            bacheTag.add("30");
            bacheTag.add("31");
            bacheTag.add("32");
            bacheTag.add("32");
            bacheTag.add("32");

        }
        dbHelper = new AssetDatabaseHelper(getApplicationContext(),DATABASE_NAME,DATABASE_VERSION);
        chapterHandler = new ChapterHandler(getApplicationContext(),dbHelper,DATABASE_NAME,DATABASE_VERSION,CHAPTERS_TABLE_NAME,PARAGRAPHS_TABLE_NAME,IMAGES_TABLE_NAME);

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
        {
            View v = findViewById(R.id.included_layout);
            AlphaAnimation fade_in = new AlphaAnimation(0, 1);
            fade_in.setDuration(200);
            v.startAnimation(fade_in);
        }
        sectionsPagerAdapter1 = new SectionsPagerAdapter(getSupportFragmentManager(), NUMBER_OF_EXAMS);
        sectionsPagerAdapter2 = new SectionsPagerAdapter2(getSupportFragmentManager(), chapterHandler.getCountOfTag("??"));

        // Set up the ViewPager with the sections adapter.
        viewPager1 = (ViewPager) findViewById(R.id.container);
        viewPager2 = (ViewPager) findViewById(R.id.container2);
        viewPager1.setAdapter(sectionsPagerAdapter1);
        viewPager2.setAdapter(sectionsPagerAdapter2);
        ArrayList<ImageView> list = new ArrayList<ImageView>();
        ArrayList<ImageView> list2 = new ArrayList<ImageView> ();//jadid emad 5 shanbe
        //Setting notification bar color
        final Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.mainPage_second_item_color));
        }

        setup_steppers(NUMBER_OF_EXAMS ,viewPager1, list, (LinearLayout) findViewById(R.id.stepper1));
        setup_steppers(chapterHandler.getCountOfTag("??"),viewPager2, list2, (LinearLayout) findViewById(R.id.stepper2));//jadid emad 5 shanbe
        set_stepper_listener(list, viewPager1, false);
        set_stepper_listener(list2, viewPager2, true);//jadid e emad 5 shanbe

        /*{

            Animation toolbar_in = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.appbar_in);
            toolbar_in.setDuration(getResources().getInteger(R.integer.animation_in));
            toolbar_in.setFillAfter(true);
            View appbar = findViewById(R.id.appbar_layout);
            appbar.startAnimation(toolbar_in);
            View main_content = findViewById(R.id.main_content);
            AlphaAnimation fade_in = new AlphaAnimation(0, 1);
            fade_in.setDuration(getResources().getInteger(R.integer.animation_in));
            main_content.startAnimation(fade_in);
        }*/

    }

    private void setupFonts() {
        TextView tozih1 = (TextView) findViewById(R.id.tozih1);
        tozih1.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/NotoSansArabic-Regular.ttf"));

        TextView tozih2 = (TextView) findViewById(R.id.tozih2);
        tozih2.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/NotoSansArabic-Regular.ttf"));

        TextView title = (TextView) findViewById(R.id.section_title);
        title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/NotoSansArabic-Regular.ttf"));
    }


    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
            Intent prev_page = new Intent(this, First_page.class);
            startActivity(prev_page);
        finish();
//        }
    }

//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }






    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";
        ChapterHandler chapterHandler;

        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {
            super.setUserVisibleHint(isVisibleToUser);
            if (getActivity() != null) {
                if (isVisibleToUser) {
                    Button b1 = (Button) getActivity().findViewById(R.id.accept_button_list1);
                    b1.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/NotoSansArabic-Regular.ttf"));
                    b1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int position = getArguments().getInt(ARG_SECTION_NUMBER);
                            Intent i = new Intent(getContext(), MyExam.class);
                            if (position > RANDOM_EXAM_INDEX) {
                                i.putExtra(PAGE_TYPE, EXAM_QUESTIONS);
                                i.putExtra(EXAM_ID, position - 1);
                            } else if (position < RANDOM_EXAM_INDEX) {
                                i.putExtra(PAGE_TYPE, EXAM_QUESTIONS);
                                i.putExtra(EXAM_ID, position);
                            } else
                                i.putExtra(PAGE_TYPE, RANDOM_EXAM_QUESTIONS);
                            startActivity(i);
                        }
                    });


                    //inja bayad benvisam
                }
            }
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            setUserVisibleHint(true);
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView ;
            if (chapterHandler == null)
            {

                AssetDatabaseHelper dbHelper = new AssetDatabaseHelper(getContext(),DATABASE_NAME,DATABASE_VERSION);
                chapterHandler = new ChapterHandler(getContext(),dbHelper,DATABASE_NAME,DATABASE_VERSION,CHAPTERS_TABLE_NAME,PARAGRAPHS_TABLE_NAME,IMAGES_TABLE_NAME);
            }
                rootView = inflater.inflate(R.layout.exam1_list_fragment, container, false);
                TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/NotoSansArabic-Regular.ttf"));

            if (getArguments().getInt(ARG_SECTION_NUMBER) == RANDOM_EXAM_INDEX)
                    textView.setText("آزمون رندوم");
                else if (getArguments().getInt(ARG_SECTION_NUMBER) > RANDOM_EXAM_INDEX)
                    textView.setText("آزمون " + numToString((getArguments().getInt(ARG_SECTION_NUMBER) - 1)));
                else
                    textView.setText("آزمون " + numToString(getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }

        private String numToString(int t)
        {
            switch (t)
            {
                case 1: return "اول";
                case 2: return "دوم";
                case 3: return "سوم";
                case 4: return "چهارم";
                case 5: return "پنجم";
                case 6: return "ششم";
                case 7: return "هفتم";
                case 8: return "هشتم";
                case 9: return "نهم";
                case 10: return "دهم";
                case 11: return "یازدهم";
                case 12: return "دوازدهم";
                default: return String.valueOf(t);
            }
        }
    }












    public static class PlaceholderFragment2 extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";
        ChapterHandler chapterHandler;

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            setUserVisibleHint(true);
        }

        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {
            super.setUserVisibleHint(isVisibleToUser);
            if (isVisibleToUser)
            {
                if (bacheTag == null) {
                    bacheTag = new ArrayList<>();
                    bacheTag.add("00");
                    bacheTag.add("01");
                    bacheTag.add("02");
                    bacheTag.add("03");
                    bacheTag.add("10");
                    bacheTag.add("11");
                    bacheTag.add("12");
                    bacheTag.add("20");
                    bacheTag.add("21");
                    bacheTag.add("22");
                    bacheTag.add("23");
                    bacheTag.add("24");
                    bacheTag.add("25");
                    bacheTag.add("30");
                    bacheTag.add("31");
                    bacheTag.add("32");
                    bacheTag.add("32");
                    bacheTag.add("32");

                }

                if (getActivity() != null) {
                    Button b2 = (Button) getActivity().findViewById(R.id.accept_button_list2);
                    b2.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/NotoSansArabic-Regular.ttf"));
                    b2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int position = getArguments().getInt(ARG_SECTION_NUMBER) - 1;
                            Intent i = new Intent(getContext(), MyExam.class);
                            i.putExtra(PAGE_TYPE, TAG_QUESTIONS);
                            i.putExtra(CHAPTER_TAG, bacheTag.get(position) + "*");


                            startActivity(i);
                        }
                    });
                }


                //inja bayad benvisam
            }
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment2 newInstance(int sectionNumber) {
            PlaceholderFragment2 fragment = new PlaceholderFragment2();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView ;
            if (chapterHandler == null)
            {

                AssetDatabaseHelper dbHelper = new AssetDatabaseHelper(getContext(),DATABASE_NAME,DATABASE_VERSION);
                chapterHandler = new ChapterHandler(getContext(),dbHelper,DATABASE_NAME,DATABASE_VERSION,CHAPTERS_TABLE_NAME,PARAGRAPHS_TABLE_NAME,IMAGES_TABLE_NAME);
            }
            if (bacheTag.size() == 0)
            {
                bacheTag.add("00");
                bacheTag.add("01");
                bacheTag.add("02");
                bacheTag.add("03");
                bacheTag.add("10");
                bacheTag.add("11");
                bacheTag.add("12");
                bacheTag.add("20");
                bacheTag.add("21");
                bacheTag.add("22");
                bacheTag.add("23");
                bacheTag.add("24");
                bacheTag.add("25");
                bacheTag.add("30");
                bacheTag.add("31");
                bacheTag.add("32");
                bacheTag.add("32");
                bacheTag.add("32");

            }

                rootView = inflater.inflate(R.layout.exam2_list_fragment, container, false);
                TextView bache_textview = (TextView) rootView.findViewById(R.id.section_label);
                bache_textview.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/NotoSansArabic-Regular.ttf"));

                String thisBacheTag = bacheTag.get(getArguments().getInt(ARG_SECTION_NUMBER) - 1);

            bache_textview.setText(chapterHandler.getTagName(thisBacheTag));
            bache_textview.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/NotoSansArabic-Regular.ttf"));

            return rootView;
        }

    }



















    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private int count;
        public SectionsPagerAdapter(FragmentManager fm, int count) {
            super(fm);
            this.count = count;
        }


        @Override
        public Fragment getItem(final int position) {
            PlaceholderFragment placeholderFragment = PlaceholderFragment.newInstance(position + 1);
            return  placeholderFragment;
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }




    public class SectionsPagerAdapter2 extends FragmentPagerAdapter {
        private int count;
        public SectionsPagerAdapter2(FragmentManager fm, int count) {
            super(fm);
            this.count = count;
        }


        @Override
        public Fragment getItem(final int position) {
            PlaceholderFragment2 placeholderFragment = PlaceholderFragment2.newInstance(position + 1);
            return  placeholderFragment;
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}

